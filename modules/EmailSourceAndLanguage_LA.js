/****************************************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Email selection, Source selection and Language selection
*****************************************************************************************/

/** Email selection popup - show **/
function showEmailSelectionPopup(){
  	printMessage("Inside - showEmailSelectionPopup");
  	PopupChangeEmail.btnCancelEmail.text	= getI18nString("common.label.cancel");
  	PopupChangeEmail.btnConfirmEmail.text	= getI18nString("common.label.confirm");
  	PopupChangeEmail.lblMessage.text		= getI18nString("common.message.1003");
  	PopupChangeEmail.btnOk.text				= getI18nString("common.label.ok");
  	PopupChangeEmail.lblEmailListTitle.text	= getI18nString("common.message.deleteaccount");
    
  /*if(PopupChangeEmail.hbxSegEmailErrListContentContainer.isVisible ===true){
 		PopupChangeEmail.lblEmailListTitle.text	= getI18nString("common.message.deleteaccount");
    }else{
      	//PopupChangeEmail.lblEmailListTitle.text	= getI18nString("common.label.chooseAccount");
    }*/
  
  	PopupChangeEmail.show();
    printMessage("Inside - showEmailSelectionPopup ends");
}

/** Settings popup - show **/
function showSettingsPopup(){
    PopupSettings.lblSettingsTitle.text		= getI18nString("common.label.settings");
  	PopupSettings.lblSource.text			= getI18nString("common.label.source");
  	PopupSettings.lblLanguage.text			= getI18nString("common.label.language");
  	PopupSettings.btnCancelSettings.text	= getI18nString("common.label.cancel");
  	PopupSettings.btnConfirmSettings.text	= getI18nString("common.label.confirm"); 
  	PopupSettings.show();
}

/** Validate configured emails in device **/
function getDeviceConfiguredEmails(){
  	printMessage("Inside - getDeviceConfiguredEmails");
  	var frmObj								= kony.application.getCurrentForm();
    configuredEmails 						= Email.getEmail();
  	printMessage("Inside - getDeviceConfiguredEmails - configuredEmails" + JSON.stringify(configuredEmails));
  	printMessage("Inside - getDeviceConfiguredEmails - typeof(configuredEmails)" + typeof(configuredEmails));
  	printMessage("Inside - getDeviceConfiguredEmails - Object.keys(configuredEmails).length" + Object.keys(configuredEmails).length);
  	if (typeof(configuredEmails) === "object" && Object.keys(configuredEmails).length === 0) {
      	configuredEmails					= [];
    }
  
   	//configuredEmails.push("service.trak1@rentokil-initial.com");
   //configuredEmails.push("chris.britten@rentokil-initial.com");
  
  	var emailsCount							= configuredEmails.length;
  	var segMasterData						= [];  	
  	var selectedEmailId						= (!isEmpty(configuredEmails) && !isEmpty(configuredEmails.email)) ? configuredEmails.email : null;
 
  	// Checking Email validation
  printMessage("Inside - getDeviceConfiguredEmails -emailsCount "+emailsCount);
  //if(configuredEmails.length ===1){
  if(configuredEmails.length >=1){
    gblConfiguredEmails = configuredEmails.join();
    startSyncICabInstances();
    //	}else if(configuredEmails.length ===0){
  }else {
    dismissLoading();
    showMessagePopup("common.message.1006", ActionDoExitApplication);
  }
  /*else{
      //More than one Email configured
      // Checking uniq Email
    var uniqueEmails 						= uniqueCriteriaVA(configuredEmails);
    var uniqueEmailsLength 					= uniqueEmails.length;
    printMessage("uniqueEmails length: " + uniqueEmailsLength);

      if (uniqueEmailsLength > 1) {
        //if more than one unique email, show popup with message
        var emailSegData = getEmailSegData(uniqueEmails); 
        printMessage("emailSegData:: "+JSON.stringify(emailSegData));
        PopupChangeEmail.segEmailErrList.setData(emailSegData);  
        //Enable Email Error popup
        PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(false);
        PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(false);
        PopupChangeEmail.hbxFooterContainer.setVisibility(false);

        PopupChangeEmail.hbxEmailErrContentContainer.setVisibility(true);
        PopupChangeEmail.hbxSegEmailErrListContentContainer.setVisibility(true);
        PopupChangeEmail.hbxEmailFooterContainer.setVisibility(true);
        showEmailSelectionPopup();
      }
    }*/
  
  	
  	/*if (emailsCount > 0){
      	var aRecord							= null;
       	var anItem							= null;
      	if (emailsCount > 1 && emailsCount < 4){          	
          	for (var i = 0; i < emailsCount; i++){
              	aRecord						= configuredEmails[i];
              	anItem						= {};
               	anItem.lblItem				= aRecord;
              	if (!isEmpty(selectedEmailId)) {
                  	if (selectedEmailId == aRecord) {
                      	anItem.imgRadioBtnIcon = LAConstants.RADIO_BTN_ON;
                    } else {
                  		anItem.imgRadioBtnIcon = LAConstants.RADIO_BTN_OFF;
                	}
                } else {
                  	anItem.imgRadioBtnIcon 		= (i === 0) ? LAConstants.RADIO_BTN_ON : LAConstants.RADIO_BTN_OFF;
                }
              	segMasterData.push(anItem);           
            }
          	gblSegEmailLastSelIndex				= 0;
          	showOverlayPopupOnLoginScreen(false, true, segMasterData, null);
        } else if (emailsCount > 3){   
          	var defSelectedEmail				= "";
          	for (var j = 0; j < emailsCount; j++){
              	aRecord							= configuredEmails[j];
              	segMasterData.push([aRecord, aRecord]);   
              	if (!isEmpty(selectedEmailId) && selectedEmailId == aRecord) {
            		defSelectedEmail 			= aRecord;
            	}
            }
          	gblSegEmailLastSelIndex				= 0;
          	var selEmail						= configuredEmails[0];
          	if (!isEmpty(defSelectedEmail) ) {
            	selEmail 						= defSelectedEmail;
            } 
          	showOverlayPopupOnLoginScreen(false, false, segMasterData, null, selEmail);
        } else {
           	printMessage("Inside - getDeviceConfiguredEmails -emailsCount insde else ");
           	printMessage("Inside - getDeviceConfiguredEmails -emailsCount insde else "+frmObj.id );
          	if (frmObj.id === LAConstants.FORM_LOGIN) {
              	if (isNetworkAvailable()){
                    gblBasicInfo.email	= configuredEmails[0];
                    ///resetSync(onSyncResetSuccessCallback);
                  	startSyncICabInstances();
                } else {
                    showMessagePopup("common.message.wifierror", ActionOnMessagePopupOkBtn);
                    dismissLoading();
                }
            } else if (gblBasicInfo.email	!= configuredEmails[0]) {
              	printMessage(" Only one email configured and it is a new one ");
            } else {
              	printMessage(" No change in email ");
            }
  		} 
    } else {
        printMessage("Inside - getDeviceConfiguredEmails -emailsCount "+emailsCount);
      	if (frmObj.id === LAConstants.FORM_LOGIN) {
      		showMessagePopup("common.message.noRentokilEmailConfigured", ActionOnMessagePopupOkBtn);
        } else {
          	showMessagePopup("common.message.noRentokilEmailConfigured", ActionPopupMessageOk);
        }
      	dismissLoading();
    }*/
}

/** To display settings / Email selections popup on login screen **/
function showOverlayPopupOnLoginScreen(isSettings, isRadioBtnGrp, firstSetData, secondSetData, firstSelKey, secondSelKey){
  //alert(1);
    printMessage(" Inside - showOverlayPopupOnLoginScreen firstSelKey:: "+firstSelKey);
   printMessage(" Inside - showOverlayPopupOnLoginScreen secondSelKey:: "+secondSelKey);
   printMessage(" Inside - showOverlayPopupOnLoginScreen ");
    printMessage(" Inside - showOverlayPopupOnLoginScreen "+isSettings);
  	if (isSettings === true) {
      printMessage(" Inside - showOverlayPopupOnLoginScreen "+isSettings);
      	var firstSetDataCount				= firstSetData.length;
       	var secondSetDataCount				= secondSetData.length;
      	var aItemObj						= null;
      	if (isRadioBtnGrp === true) {
          	var segDataSet					= [];
          	var segSectionSet				= []; /** Includes Header object and row data sets **/
			var segRowSet					= []; /** Includes row data sets **/
          	var sectionObj					= null;
          	var aRowObj						= null;
          
          	if (firstSetDataCount > 0) {
              	sectionObj					= {};
              	sectionObj.lblTitle			= getI18nString("common.label.source");
              	segSectionSet.push(sectionObj);
                for (var i = 0; i < firstSetDataCount; i++) {
                    aItemObj				= firstSetData[i];
                    aRowObj					= {};
                    if ((isEmpty(firstSelKey) && i === 0) || (!isEmpty(firstSelKey) && firstSelKey == aItemObj.iCABSServer)) {
                      	aRowObj.imgRadioBtnIcon	= LAConstants.RADIO_BTN_ON;
                    } else {
                      	aRowObj.imgRadioBtnIcon	= LAConstants.RADIO_BTN_OFF;
                    }
					printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.iCABSServer);
                  printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.iCABSServer);
                      printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.countryCode);
                    printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.businessCode);
                    aRowObj.lblItem			= aItemObj.iCABSServer;
                    aRowObj.itemCode		= aItemObj.iCABSServer;
                    segRowSet.push(aRowObj);
                }
              	segSectionSet.push(segRowSet);
              	segDataSet.push(segSectionSet);
            }
          	
          	if (secondSetDataCount > 0) {
              	segSectionSet				= [];
              	segRowSet					= [];
              	sectionObj					= {};
              	sectionObj.lblTitle			= getI18nString("common.label.language");
              	segSectionSet.push(sectionObj);
                for (var j = 0; j < secondSetDataCount; j++) {
                    aItemObj				= secondSetData[j];
                    aRowObj					= {};
                    if ((isEmpty(secondSelKey) && j === 0) || (!isEmpty(secondSelKey) && secondSelKey == aItemObj.code)) {
                      	aRowObj.imgRadioBtnIcon	= LAConstants.RADIO_BTN_ON;
                    } else {
                      	aRowObj.imgRadioBtnIcon	= LAConstants.RADIO_BTN_OFF;
                    }
                    aRowObj.lblItem			= aItemObj.name;
                    aRowObj.itemCode		= aItemObj.code;
                    segRowSet.push(aRowObj);
                }
              	segSectionSet.push(segRowSet);
              	segDataSet.push(segSectionSet);
            }
          	PopupSettings.segSettingsList.setData(segDataSet);
          	PopupSettings.hbxSettingsContentContainer.setVisibility(true); 
          	PopupSettings.hbxSettingsContentContainerAlt.setVisibility(false);
          	PopupSettings.lbxSourceList.masterData	= [];
          	PopupSettings.lbxLangDropdownList.masterData= [];
    	} else {
          	var sourceDataSet					= [];
          	var languageDataSet					= [];
          	if (firstSetDataCount > 0) {
              	var selKeyFirst					= firstSelKey;
              	for (var k = 0; k < firstSetDataCount; k++) {
					aItemObj					= firstSetData[k];
                   printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.iCABSServer);
                  //LAA 40
                  printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.countryCode);
                  printMessage(" Inside - showOverlayPopupOnLoginScreen "+aItemObj.businessCode);
                    sourceDataSet.push([aItemObj.iCABSServer, aItemObj.countryCode+" "+aItemObj.businessCode]);
                  //sourceDataSet.push([aItemObj.iCABSServer, aItemObj.iCABSServer]);
                    printMessage(" Inside - showOverlayPopupOnLoginScreen firstSelKey "+firstSelKey);
                  	if (k === 0 && isEmpty(firstSelKey)) {
                      	selKeyFirst				= aItemObj.iCABSServer;
                    }
				}
              	PopupSettings.lbxSourceList.masterData	= sourceDataSet;
          		PopupSettings.lbxSourceList.selectedKey	= selKeyFirst;
            }
          	
          	if (secondSetDataCount > 0) {
              	var selKeySecond				= secondSelKey;
              	for (var l = 0; l < secondSetDataCount; l++) {
					aItemObj					= secondSetData[l];
                  	languageDataSet.push([aItemObj.code, aItemObj.name]);
                  	if (l === 0 && isEmpty(secondSelKey)) {
                      	selKeySecond			= aItemObj.code;
                    }
				}
              	PopupSettings.lbxLangDropdownList.masterData	= languageDataSet;
          		PopupSettings.lbxLangDropdownList.selectedKey 	= selKeySecond;
            }
          	PopupSettings.hbxSettingsContentContainerAlt.setVisibility(true);
            PopupSettings.hbxSettingsContentContainer.setVisibility(false);
          	PopupSettings.segSettingsList.setData([]);
    	}

      //Hiding Source Combo box if User in CreateLeads or CreateAlerts Page
      printMessage("kony.application.getCurrentForm().id::: "+kony.application.getCurrentForm().id);
      if (kony.application.getCurrentForm().id === LAConstants.FORM_LEADS || kony.application.getCurrentForm().id ===LAConstants.FORM_ALERTS) {
        printMessage("kony.application.getCurrentForm().id::: if");
        PopupSettings.hbxSettingsContentContainer.setVisibility(false);
        PopupSettings.lblSource.setVisibility(false);
        PopupSettings.lbxSourceList.setVisibility(false);
      }else{
         printMessage("kony.application.getCurrentForm().id::: else");
         PopupSettings.lblSource.setVisibility(true);
         PopupSettings.lbxSourceList.setVisibility(true);
      }
      	showSettingsPopup();
    } else {
      	if (isRadioBtnGrp === true) {
      		PopupChangeEmail.segEmailList.setData(firstSetData);  
          	PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(true);
          	PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(false);
          	PopupChangeEmail.hbxFooterContainer.setVisibility(true);
          
          	PopupChangeEmail.hbxEmailErrContentContainer.setVisibility(false);
			PopupChangeEmail.hbxSegEmailErrListContentContainer.setVisibility(false);
			PopupChangeEmail.hbxEmailFooterContainer.setVisibility(false);
          
    	} else {
      		PopupChangeEmail.lbxEmailList.masterData	= firstSetData;
          	PopupChangeEmail.lbxEmailList.selectedKey	= firstSelKey;
          	PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(true);
          	PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(false);
          	PopupChangeEmail.hbxFooterContainer.setVisibility(true);
          	
          	PopupChangeEmail.hbxEmailErrContentContainer.setVisibility(false);
			PopupChangeEmail.hbxSegEmailErrListContentContainer.setVisibility(false);
			PopupChangeEmail.hbxEmailFooterContainer.setVisibility(false);
    	}
      	showEmailSelectionPopup();
    }
  	dismissLoading();
}

function displayLanguageSelectionPopup(){
  
  /*gblBasicInfo.languages			= [];
  gblBasicInfo.languages.push({"code":"en_GB", "name":"English"});
  //gblBasicInfo.languages.push({"code":"es_ES", "name":"Spanish"});
  gblBasicInfo.languages.push({"code":"en_US", "name":"US"});

  if (isEmpty(gblBasicInfo.selLanguage)){
    gblBasicInfo.selLanguage		= "en_GB";
  }
  if (isEmpty(gblBasicInfo.selLanguage)){
    gblBasicInfo.selLanguage		= "en_GB";
  } */
  setInternationalizationCode();
  var emptyArray = [];
	var selectediCabServerCode = ""; //rowItem.countryCode+'_'+rowItem.businessCode;
  	//selectediCabServerCode = gblBasicInfo.countryCode+'_'+gblBasicInfo.businessCode;
  
  if(gblIsLaunchedAsStandAloneApp){
    selectediCabServerCode = getSelectedSourceValue();
  }else{
    selectediCabServerCode = gblBasicInfo.countryCode+'_'+gblBasicInfo.businessCode;
    //setSelectedSourceValue(selectediCabServerCode);
  }
	gblBasicInfo.selLanguage = kony.store.getItem(LAConstants.SEL_LAN_CODE);
  	printMessage("Inside displayLanguageSelectionPopup selectediCabServerCode:: "+selectediCabServerCode);
  	printMessage("Inside displayLanguageSelectionPopup gblBasicInfo.selLanguage:: "+gblBasicInfo.selLanguage);
  	showOverlayPopupOnLoginScreen(true, false, emptyArray, gblBasicInfo.languages, selectediCabServerCode, gblBasicInfo.selLanguage);

    /*PopupSettings.hbxSettingsContentContainer.setVisibility(false);
    PopupSettings.lblSource.setVisibility(false);
    PopupSettings.lbxSourceList.setVisibility(false);
  	showSettingsPopup();*/
}

/** Selecting a row from email segment **/
function onEmailListRowClick(){
  	var selectedRowIndex				= PopupChangeEmail.segEmailList.selectedRowIndex[1];
  	var selectedSectionIndex			= PopupChangeEmail.segEmailList.selectedRowIndex[0];
  	if (selectedRowIndex !== gblSegEmailLastSelIndex) {
      	var preSelectedRow				= PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
      	var curSelectedRow				= PopupChangeEmail.segEmailList.data[selectedRowIndex];
      
      	preSelectedRow.imgRadioBtnIcon 	= LAConstants.RADIO_BTN_OFF;
      	curSelectedRow.imgRadioBtnIcon 	= LAConstants.RADIO_BTN_ON;
      
      	PopupChangeEmail.segEmailList.setDataAt(preSelectedRow, gblSegEmailLastSelIndex, selectedSectionIndex);
      	PopupChangeEmail.segEmailList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
      
      	gblSegEmailLastSelIndex			= selectedRowIndex;
    }
}

/** On click Emails popup Confirm button action **/
function onClickEmailConfirmButton(){
  	showLoading("common.message.1001");  	
  	var selectedEmail					= null;
  	if (PopupChangeEmail.hbxSegEmailListContentContainer.isVisible === true) {
      	var selectedItem				= PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
      	selectedEmail					= selectedItem.lblItem;
    } else {
      	selectedEmail					= PopupChangeEmail.lbxEmailList.selectedKey;
    }
  	printMessage(" selectedEmail is  " + selectedEmail);
  	var curEmailAddress					= (!isEmpty(gblEmployeeObject)) ? gblEmployeeObject.email : null;
  	var isEmpAlreayLoggedIn				= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
  	PopupChangeEmail.dismiss();
  	if (!isEmpty(curEmailAddress) && curEmailAddress == selectedEmail) {
      	showMessagePopup("common.message.noChangeInEmail", ActionPopupMessageOk);
      	dismissLoading();
    } else if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true){
      //	showConfirmPopup("common.message.changeEmailWarning", ActionConfirmChangeEmail, ActionDeclineChangeEmail);
      	dismissLoading();
    } else {
        resetOnChangeSourceOrEmail();
       	printMessage(" selectedEmail is from onClickEmailConfirmButton " + selectedEmail);
      	gblBasicInfo.email				= selectedEmail;
      	startSyncICabInstances();
    }
}

/** User confirmed change email **/
function confirmChangeEmail(){
  
  //alert(123);
  	if (isNetworkAvailable()){
      printMessage(" confirmChangeEmail-111-->> ");
      	//resetOnChangeSourceOrEmail();
      	var selectedEmail					= null;
        if (PopupChangeEmail.hbxSegEmailListContentContainer.isVisible === true) {
            var selectedItem				= PopupChangeEmail.segEmailList.data[gblSegEmailLastSelIndex];
            selectedEmail					= selectedItem.lblItem;
        } else {
            selectedEmail					= PopupChangeEmail.lbxEmailList.selectedKey;
        }
      printMessage(" confirmChangeEmail-2222-->> ");
      	gblBasicInfo.email					= selectedEmail;
      	startSyncICabInstances();
      	dismissConfirmPopup();
    } else {
      	var isEmpAlreayLoggedIn				= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
      	if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true){
          	printMessage(" Network not available to sync data ");
          	dismissLoading();
        } else {
          	showMessagePopup("common.message.wifierror", ActionDoExitApplication);
          	dismissLoading();
        }
      	dismissConfirmPopup();
    }	
}

/** User declined changing email **/
function declineChangeEmail(){
  	dismissLoading();
  	dismissConfirmPopup();
}

/** On click Emails popup Cancel button action **/
function onClickEmailCancelButton(){
	PopupChangeEmail.dismiss();
  	if (isEmpty(gblEmployeeObject)) {
      	exitApplication();
    }
}

/** Cancel Email account selection **/
function onCancelEmailSelection(){
  	doExitApplication();
}

/** Selecting a row from settings segment **/
function onSettingsListRowClick(){
  	var selectedRowIndex						= PopupSettings.segSettingsList.selectedRowIndex[1];
  	var selectedSectionIndex					= PopupSettings.segSettingsList.selectedRowIndex[0];
  	var preSelectedRow							= null;	
  	var curSelectedRow							= null;
  	var sectionData								= null;
  	var sectionRows								= null;	
  	
  	if (selectedSectionIndex === LAConstants.SOURCE_SECTION) {
      	if (selectedRowIndex !== gblSegSourceLastSelIndex) {
          	sectionData							= PopupSettings.segSettingsList.data[selectedSectionIndex]; 
          	sectionRows							= sectionData[1];
          	preSelectedRow						= sectionRows[gblSegSourceLastSelIndex];
      		curSelectedRow						= sectionRows[selectedRowIndex];
          
          	preSelectedRow.imgRadioBtnIcon 		= LAConstants.RADIO_BTN_OFF;
            curSelectedRow.imgRadioBtnIcon 		= LAConstants.RADIO_BTN_ON;
          	PopupSettings.segSettingsList.setDataAt(preSelectedRow, gblSegSourceLastSelIndex, selectedSectionIndex);
            PopupSettings.segSettingsList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
          	gblSegSourceLastSelIndex			= selectedRowIndex;
        }
    } else {
      	if (selectedRowIndex !== gblSegLanguageLastSelIndex) {
          	sectionData							= PopupSettings.segSettingsList.data[selectedSectionIndex]; 
          	sectionRows							= sectionData[1];
          	preSelectedRow						= sectionRows[gblSegLanguageLastSelIndex];
      		curSelectedRow						= sectionRows[selectedRowIndex];
          	preSelectedRow.imgRadioBtnIcon 		= LAConstants.RADIO_BTN_OFF;
            curSelectedRow.imgRadioBtnIcon 		= LAConstants.RADIO_BTN_ON;
          	PopupSettings.segSettingsList.setDataAt(preSelectedRow, gblSegLanguageLastSelIndex, selectedSectionIndex);
            PopupSettings.segSettingsList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
          	gblSegLanguageLastSelIndex			= selectedRowIndex;
        }
    }
}

/** Confirm Source and Language selection **/
function onConfirmSourceLanguageSelection(){
  printMessage(" Inside - onConfirmSourceLanguageSelection ");
  showLoading("common.message.1001");
  // Updating Server code 
  printMessage("onConfirmSourceLanguageSelection--------- gblBasicInfo.selICabServerCode "+gblBasicInfo.selICabServerCode);
  printMessage("onConfirmSourceLanguageSelection--------- gblBasicInfo.selICabServerCode "+gblBasicInfo.selLanguage);
  var isEmpAlreayLoggedIn						= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
  var selLanCode								= "";
  if (PopupSettings.hbxSettingsContentContainer.isVisible === true){
    var selSourceObjSection					= PopupSettings.segSettingsList.data[LAConstants.SOURCE_SECTION];
    var selLanguageObjSection				= PopupSettings.segSettingsList.data[LAConstants.LANGUAGE_SECTION];
    var sourceObjRows						= selSourceObjSection[1];
    var languageObjRows						= selLanguageObjSection[1];
    var selSourceObj						= sourceObjRows[gblSegSourceLastSelIndex];
    var selLanguageObj						= languageObjRows[gblSegLanguageLastSelIndex];
    if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true ){

      printMessage(" startSyncEmpService------------isEmpAlreayLoggedIn-"+isEmpAlreayLoggedIn);  
      printMessage(" startSyncEmpService------------gblBasicInfo.selICabServerCode-"+gblBasicInfo.selICabServerCode); 
      printMessage(" startSyncEmpService------------gblBasicInfo.selICabServerCode-"+selSourceObj.itemCode); 
      printMessage(" startSyncEmpService------------gblBasicInfo.selICabServerCode-"+selLanguageObj.itemCode); 
      if (gblBasicInfo.selICabServerCode === selSourceObj.itemCode && gblBasicInfo.selLanguage === selLanguageObj.itemCode) {
        printMessage(" No change in source or language ");
        dismissLoading();
      } else if (gblBasicInfo.selLanguage === selLanguageObj.itemCode) {

        gblBasicInfo.selICabServerCode= selSourceObj.itemCode;
        gblBasicInfo.selLanguage		= selLanguageObj.itemCode;

        // calling the method for showing the warning
        //startSyncEmpService();
      } else if (gblBasicInfo.selICabServerCode === selSourceObj.itemCode) {
        gblBasicInfo.selICabServerCode= selSourceObj.itemCode;
        gblBasicInfo.selLanguage		= selLanguageObj.itemCode;
        changeDeviceLocale(gblBasicInfo.selLanguage, onChangeLanguageSuccessCallback, onChangeLanguageFailureCallback);
      } else {
        gblBasicInfo.selICabServerCode= selSourceObj.itemCode;
        gblBasicInfo.selLanguage		= selLanguageObj.itemCode;
        changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
      }
    } else {
      printMessage("startSyncEmpService--------1-");
      selLanCode							= kony.store.getItem(LAConstants.SEL_LAN_CODE);
      gblBasicInfo.selICabServerCode		= selSourceObj.itemCode;
      gblBasicInfo.selLanguage			= selLanguageObj.itemCode;
      if (isEmpty(selLanCode) || selLanCode != selLanguageObj.itemCode) {
        changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
      } else {
        printMessage(" startSyncEmpService------------2-");  
        startSyncEmpService();
      }
    }
  } else {
    printMessage("startSyncEmpService--------3-");
    dismissLoading();
    var selSourceKey							= "";
    var selLanguageKey							= "";
    if(!isEmpty(PopupSettings.lbxSourceList.selectedKey)){
      selSourceKey							= PopupSettings.lbxSourceList.selectedKey;
    }else{
      selSourceKey 							= kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE);
    }
    printMessage("startSyncEmpService--------3- PopupSettings.lbxSourceList.selectedKey "+PopupSettings.lbxSourceList.selectedKey);
    printMessage("startSyncEmpService--------3- PopupSettings.lbxLangDropdownList.selectedKey "+PopupSettings.lbxLangDropdownList.selectedKey);
    printMessage("startSyncEmpService--------3-kony.store.getItem(LAConstants.SEL_LAN_CODE) "+kony.store.getItem(LAConstants.SEL_LAN_CODE));
    printMessage("startSyncEmpService--------3- PopupSettings.lbxLangDropdownList.selectedKey "+!isEmpty(PopupSettings.lbxLangDropdownList.selectedKey));
    printMessage("startSyncEmpService--------3- PopupSettings.lbxLangDropdownList.selectedKey is null check "+(PopupSettings.lbxLangDropdownList.selectedKey !== null));

    if(!isEmpty(PopupSettings.lbxLangDropdownList.selectedKey && PopupSettings.lbxSourceList.selectedKey !== null)){
      selLanguageKey							= PopupSettings.lbxLangDropdownList.selectedKey;
    }else{
       printMessage("startSyncEmpService--------3-"+isEmpty(kony.store.getItem(LAConstants.SEL_LAN_CODE)));
      if(isEmpty(kony.store.getItem(LAConstants.SEL_LAN_CODE))){
        selLanguageKey							= "en_GB";
      }else{
         selLanguageKey 						= kony.store.getItem(LAConstants.SEL_LAN_CODE);
      }
    }

    printMessage("startSyncEmpService--------3-"+selSourceKey);
    printMessage("startSyncEmpService--------3-"+selLanguageKey);
    var isDifferentSourceSelected = false;
    // printMessage("startSyncEmpService--------3.7-"+gblSharedInfo.selLanguage);
    //printMessage("startSyncEmpService--------3.7-"+gblSharedInfo.selICabServerCode);
    printMessage("startSyncEmpService--------3.7-gblBasicInfo.selLanguage "+gblBasicInfo.selLanguage);
    printMessage("startSyncEmpService--------3.7-"+gblBasicInfo.selICabServerCode);
    printMessage("startSyncEmpService--------3.7-"+gblBasicInfo.selICabServerCode);
    printMessage("startSyncEmpService--------3.7-gblBasicInfo.selLanguage "+gblBasicInfo.selLanguage);
    if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true){
      if (gblBasicInfo.selICabServerCode == selSourceKey && gblBasicInfo.selLanguage	== selLanguageKey) {
        printMessage(" No change in source or language ");
        //Add Alert
        dismissLoading();
      } 
      //baic info is used while switching the icab..this will be allowed if the user is in standalone app
      else if (gblBasicInfo.selLanguage == selLanguageKey) {
        printMessage("startSyncEmpService--------3.5-");
        gblBasicInfo.selICabServerCode 	= selSourceKey;
        gblBasicInfo.selLanguage		= selLanguageKey;
        // resetSync();
        //   startSyncEmpService();
        //calling the popup
        gblCheckSelectedIcab=selSourceKey;
        isDifferentSourceSelected = true;
        displayChangingSourceWaring();
      } else if (gblBasicInfo.selICabServerCode == selSourceKey) {
        printMessage("startSyncEmpService--------3.6- only language changed");

        glbLanguageOnly =true;
        gblBasicInfo.selICabServerCode= selSourceKey;
        gblBasicInfo.selLanguage		= selLanguageKey;
        //changeDeviceLocale(gblBasicInfo.selLanguage, onChangeLanguageSuccessCallback, onChangeLanguageFailureCallback);
        changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageSuccessCallback);
      } else {
        printMessage("startSyncEmpService--------3.7-");
        gblBasicInfo.selICabServerCode= selSourceKey;
        gblBasicInfo.selLanguage		= selLanguageKey;
        printMessage("startSyncEmpService--------3.7-changing the vlaue hererer");
        changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
      }
    } else {
      printMessage("startSyncEmpService--------4-");
       
      //Previous Code
      /*selLanCode								= kony.store.getItem(LAConstants.SEL_LAN_CODE);
          	gblBasicInfo.selICabServerCode	= selSourceKey;
            gblBasicInfo.selLanguage			= selLanguageKey;
          	if (isEmpty(selLanCode) || selLanCode != selLanguageKey) {
              	printMessage("startSyncEmpService--------5-");
              	changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
            } else {
                    printMessage("startSyncEmpService--------6.1-"+gblBasicInfo.selICabServerCode);
					printMessage("startSyncEmpService--------6.1-"+gblBasicInfo.selICabServerCode);
                	printMessage("startSyncEmpService--------6-");
              	startSyncEmpService();
            }*/
      if(isEmpty(kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE))){
        printMessage("startSyncEmpService--------4-1");
        gblBasicInfo.selICabServerCode	= selSourceKey;
        gblBasicInfo.selLanguage		= selLanguageKey;
      }else{
        printMessage("startSyncEmpService--------4-2"+kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE));
        selSourceKey								= kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE);
      }
	  
      //Updated Code
      if(kony.application.getCurrentForm().id == LAConstants.FORM_LEAD_MANAGEMENT || kony.application.getCurrentForm().id ==LAConstants.FORM_LOGIN){
        printMessage("startSyncEmpService--------6.1-"+gblBasicInfo.selICabServerCode);
        printMessage("startSyncEmpService--------6.1-"+gblBasicInfo.selICabServerCode);
        printMessage("startSyncEmpService--------6-");

        gblBasicInfo.selICabServerCode			= selSourceKey;
        gblBasicInfo.selLanguage				= selLanguageKey; 
        printMessage("startSyncEmpService--------6- gblBasicInfo.selICabServerCode "+gblBasicInfo.selICabServerCode);
        printMessage("startSyncEmpService--------6- gblBasicInfo.selICabServerCode "+gblBasicInfo.selLanguage);
        changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
        startSyncEmpService();
      }else{
        selLanCode								= kony.store.getItem(LAConstants.SEL_LAN_CODE);
        gblBasicInfo.selICabServerCode			= selSourceKey;
        gblBasicInfo.selLanguage				= selLanguageKey;
        printMessage("Language selection confirm selLanCode --------- "+selLanCode);
        printMessage("Language selection confirm --selSourceKey $$$------- "+selSourceKey);
        printMessage("Language selection confirm --------- "+selLanguageKey);
        // if (isEmpty(selLanCode) || selLanCode != selLanguageKey) {
        if(selLanCode != selLanguageKey) {
          printMessage("startSyncEmpService--------5-");
          changeDeviceLocale(gblBasicInfo.selLanguage, startSyncOnChangeLanguageSuccessCallback, startSyncOnChangeLanguageFailureCallback);
        }else{
          printMessage("No change language, selcted language is same ");
        }
        dismissLoading();
      }
    }
 	printMessage("gblMyAppDataObject isDifferentSourceSelected" + isDifferentSourceSelected);
    if(isDifferentSourceSelected === false){
      //selSourceKey							= kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE);
      printMessage("gblMyAppDataObject isDifferentSourceSelected selSourceKey" + selSourceKey);
      setSelectedSourceValue(selSourceKey);
    }
  }
  PopupSettings.dismiss();
  printMessage("gblMyAppDataObject " + JSON.stringify(gblSharedInfo));
  printMessage("gblMyAppDataObject " + JSON.stringify(gblBasicInfo));
}

/** Cancel Source and Language selection **/
function onCancelSourceLanguageSelection(){
  
if (!isEmpty(gblEmployeeObject)){
    PopupSettings.dismiss();
  } else {
    if((kony.application.getCurrentForm().id === LAConstants.FORM_LEAD_MANAGEMENT)  ||
      (kony.application.getCurrentForm().id  === LAConstants.FORM_ALERTS	)||
      (kony.application.getCurrentForm().id === LAConstants.FORM_LEADS)){
      PopupSettings.dismiss();
    }else{
      doExitApplication();
    }
  }
}

/** Success callback - change device locale - Based on settings screen language selection **/
function onChangeLanguageSuccessCallback(oldlocalename, newlocalename){
  	printMessage(" Inside - onChangeLanguageSuccessCallback ");
  	kony.store.setItem(LAConstants.SEL_LAN_CODE, newlocalename);
  	saveLocaleUpdatedLangCode();
  	doRefreshi18n();
  	dismissLoading();
}

/** Failure callback - change device locale - Based on settings screen language selection **/
function onChangeLanguageFailureCallback(errCode, errMsg){
  	printMessage(" Inside - onChangeLanguageFailureCallback ");
  	dismissLoading();
}

/** Success callback - change device locale will initiate download data  - Based on settings screen language selection **/
function startSyncOnChangeLanguageSuccessCallback(oldlocalename, newlocalename,info){
  	printMessage(" Inside - startSyncOnChangeLanguageSuccessCallback newlocalename "+newlocalename);
  	kony.store.setItem(LAConstants.SEL_LAN_CODE, newlocalename);
  	saveLocaleUpdatedLangCode();
  	doRefreshi18n();
  	//startSyncEmpServiceAreaAndConfigData();
  
  if(kony.application.getCurrentForm().id == LAConstants.FORM_LEAD_MANAGEMENT){
     if(!glbLanguageOnly){
    	startSyncEmpService();
       }
  }
}

/** Failure callback - change device locale will initiate download data - Based on settings screen language selection **/
function startSyncOnChangeLanguageFailureCallback(errCode, errMsg,info){
  	printMessage(" Inside - startSyncOnChangeLanguageFailureCallback ");
  	//startSyncEmpServiceAreaAndConfigData();
  	startSyncEmpService();
}


