var gFlow="";

function getCurrentLocationAddress() 
{
	showLoading("common.message.1001");
    gFlow = "Leads&Alerts";
    getCurrentLocation();
}

function getCurrentLocation() 
{
  kony.print("Inside getCurrentLocation ::::");
  if (isNetworkAvailable())
  {
        var positionoptions = 
		{
            timeout: 5000,
            useBestProvider: true
        };
    kony.location.getCurrentPosition(geoLocationSuccessCallback, geoLocationErrorcallback, positionoptions);
 
   } 
   else 
   {	
			showMessagePopup("common.message.wifierror", ActionPopupMessageOk);
			dismissLoading();
   }
}

function geoLocationSuccessCallback(position) 
{
    kony.print("inside geoLocationSuccessCallback function");
    gblVars.gblLatitude = position.coords.latitude;
    gblVars.gblLongitude =  position.coords.longitude;
    kony.print("gblLatitude :::" + gblVars.gblLatitude);
    kony.print("gblLongitude :::" + gblVars.gblLongitude + "currentlocale ::::" + kony.i18n.getCurrentLocale());
  	callGetLocationNameService(gblVars.gblLatitude, gblVars.gblLongitude, kony.i18n.getCurrentLocale());
	
	if (gFlow === "Leads&Alerts") {
        dismissLoading();
    }
}

function geoLocationErrorcallback(positionerror)
{
	//alert("Please turn on device location to use Map Functionality");
	showMessagePopup("common.message.devicelocationerror", ActionPopupMessageOk);
	dismissLoading();	
}

function callGetLocationNameService(latitude, longitude, lang) {
    kony.print("inside callGetLocationNameService function");
    var httpClient = new kony.net.HttpRequest();
    httpClient.timeout = 5000;

        httpClient.onReadyStateChange = callGetLocationAddressServiceCallBack;
    
    //httpClient.open(constants.HTTP_METHOD_GET, "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=false&language=" + lang, false);
  httpClient.open(constants.HTTP_METHOD_GET, "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCQQH-IB6kVSfOrkt5ZZv2ZKLPLX9Up1dc&latlng="+ latitude + "," + longitude + "&sensor=false&language=" + lang, true);
    httpClient.send();
}

function callGetLocationAddressServiceCallBack() {
    try {
        var strReadyState = this.readyState.toString();
        kony.print("in callGetLocationAddressServiceCallBack state is:" + strReadyState);
        if (kony.string.equalsIgnoreCase(strReadyState, "4")) {
            kony.print("responseType: " + this.responseType);
            kony.print("this.status: " + this.status);
            if ((null !== this.response) && (constants.HTTP_RESPONSE_TYPE_JSON == this.responseType) && ("200" == this.status)) {
                kony.print(" ** status and json condtion passed **");
                var responseData = JSON.stringify(this.response);
                var result = JSON.parse(responseData);
                kony.print("result[results].length " + result.results.length);
                kony.print("Google result ::---------->>>", responseData);
                if (0 < result.results.length) {
                    var formattedAddress = result.results[0]["formatted_address"];
                    var addressArray = formattedAddress.split(",");
                    for (var i in addressArray) {
                        var index = parseInt(i);
                        if (index === 0) {
                          	frmLead.PHtxtAddress1.setVisibility(false);
                            frmLead.txtAddress1.text = addressArray[index];
                        } /*else if (index === 1) {
                         	frmLead.PHtxtAddress2.setVisibility(false);
                            frmLead.txtAddress2.text = addressArray[index];
                        } else if (index === 2) {
                            frmLead.PHtxtAddress3.setVisibility(false);
                            frmLead.txtAddress3.text = addressArray[index];
                        } else if (index === 3) {
                          	frmLead.PHtxtAddress4.setVisibility(false);
                            frmLead.txtAddress4.text = addressArray[index];
                        } else if (index === 4) {
                          	frmLead.PHtxtAddress5.setVisibility(false);
                            frmLead.txtAddress5.text = addressArray[index];
                        }*/
                    }
                    for (var i in result["results"][0]["address_components"]) {
                      
                           //modified
                      
                       if (result["results"][0]["address_components"][i]["types"][0] === "route") {
                          frmLead.PHtxtAddress2.setVisibility(false);
                            frmLead.txtAddress2.text = result["results"][0]["address_components"][i]["long_name"];
                        }
      
       				if (result["results"][0]["address_components"][i]["types"][0] === "postal_town") {
                          frmLead.PHtxtAddress4.setVisibility(false);
                            frmLead.txtAddress4.text = result["results"][0]["address_components"][i]["long_name"];
                        }
                    
                      
                        if (result["results"][0]["address_components"][i]["types"][0] === "postal_code") {
                          frmLead.PHtxtPostcode.setVisibility(false);
                            frmLead.txtPostcode.text = result["results"][0]["address_components"][i]["long_name"];
                        }
                      
                      	if (result["results"][0]["address_components"][i]["types"][0] === "country") {
                          frmLead.PHtxtAddress5.setVisibility(false);
                            frmLead.txtAddress5.text = result["results"][0]["address_components"][i]["long_name"];
                        }
                        //end
                    }
                } else {
                    if (result.status == "OVER_QUERY_LIMIT") 
					{
                        alert("OVER_QUERY_LIMIT");
                        //kony.timer.schedule("MapTimer", callGetLocationNameService, 200, true);
                    } else 
					{
					alert("no data found");	
                      //showMessagePopup("generic_noDataFound", ActionPopupMessageOk);
                    }

                }
            } else 
			{
			showMessagePopup("common.message.wifierror", ActionPopupMessageOk);	
            }
        } else 
		{
            dismissLoading();
        }
    } catch (Error) {
        kony.print("ERROR :", Error);
        displayError(Error);
        dismissLoading();
    }
}
