/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Login - Methods
***********************************************************************/

/** Login screen - PreShow **/
function onPreShowLoginScreen(){
  	printMessage("Inside - onPreShowLoginScreen");
  	gblEmployeeObject					= null;
  	refreshi18nLogin();
}

/** Login screen - PostShow **/
function onPostShowLoginScreen(){	
  	gblStartupFormIdentifier			= "Login";
  	printMessage("Inside - onPostShowLoginScreen");
  	showLoading("common.message.1001");  
  	initSyncModule();
}

/** Login screen - Refresh all i18n **/
function refreshi18nLogin(){
  	printMessage("Inside - refreshi18nLogin");
  	Login.lblAppName.text				= getI18nString("common.app.title");
  	Login.lblWelcome.text				= getI18nString("common.label.1002");
  	Login.lblUsername.text				= (!isEmpty(gblEmployeeObject)) ? gblEmployeeObject.getEmployeeName() : "";
  	//Login.lblAppVersion.text			= getI18nString("common.label.version") +" " + LAConstants.APP_VERSION;
  	Login.lblAppVersion.text			= getI18nString("common.label.version") +" " + appConfig.appVersion;
}



/** Sync initialization - Failure callback  **/
function failure_initSyncModule(response){
	printMessage("Inside  - failure_initSyncModule");
  	dismissLoading();
}

/** Success callback - SQL query execution - get iCabInstances  **/
function success_getiCabInstances(result){
  printMessage("Inside - success_getiCabInstances"+JSON.stringify(result));
  if(result !== null && result.length > 0) {
    var selectediCabServerCode				= "";
    gblBasicInfo.iCabInstances              = [];
    var rowItem 							= null;
    var anIcabInstance						= null;
    var emailArray 							= [];
    printMessage("Inside - success_getiCabInstances selectediCabServerCode"+gblBasicInfo.selICabServerCode);
    for (var i = 0; i < result.length; i++) {
      rowItem								= result[i];
      anIcabInstance						= {};
      anIcabInstance.businessCode			= rowItem.businessCode;
      anIcabInstance.countryCode			= rowItem.countryCode;
      anIcabInstance.email				    = rowItem.email;
     // anIcabInstance.iCABSServer			= rowItem.iCABSServer+'_'+rowItem.countryCode+'_'+rowItem.businessCode;
      anIcabInstance.iCABSServer			= rowItem.countryCode+'_'+rowItem.businessCode;
      gblBasicInfo.iCabInstances.push(anIcabInstance);
      emailArray.push(rowItem.email.toString().toLowerCase());
     //check the source change then the popup need to avoid
      if(!gblSourceChange){
        if (isEmpty(gblBasicInfo.selICabServerCode) && i === 0){
         // selectediCabServerCode		= rowItem.iCABSServer+'_'+rowItem.countryCode+'_'+rowItem.businessCode;
           selectediCabServerCode			= rowItem.countryCode+'_'+rowItem.businessCode;
        } else if (!isEmpty(gblBasicInfo.selICabServerCode) && gblBasicInfo.selICabServerCode == anIcabInstance.iCABSServer){
         // selectediCabServerCode		= rowItem.iCABSServer+'_'+rowItem.countryCode+'_'+rowItem.businessCode;
          selectediCabServerCode			= rowItem.countryCode+'_'+rowItem.businessCode;
        }
      }else{
        selectediCabServerCode =gblCheckSelectedIcab;
          printMessage("Inside - else loop selectediCabServerCode"+selectediCabServerCode);
      }
    }
    // Checking uniq Email
    var uniqueEmails 						= uniqueCriteriaVA(emailArray);
    var uniqueEmailsLength 					= uniqueEmails.length;
    kony.print("uniqueEmails length: " + uniqueEmailsLength);
    printMessage("Inside - success_getiCabInstances selectediCabServerCode"+selectediCabServerCode);
    if (uniqueEmailsLength > 1) {
      //if more than one unique email, show popup with message
      var emailSegData = getEmailSegData(uniqueEmails); 
      printMessage("emailSegData:: "+JSON.stringify(emailSegData));
      PopupChangeEmail.segEmailErrList.setData(emailSegData);  
      //Enable Email Error popup
      PopupChangeEmail.hbxSegEmailListContentContainer.setVisibility(false);
      PopupChangeEmail.hbxLbxEmailListContentContainer.setVisibility(false);
      PopupChangeEmail.hbxFooterContainer.setVisibility(false);

      PopupChangeEmail.hbxEmailErrContentContainer.setVisibility(true);
      PopupChangeEmail.hbxSegEmailErrListContentContainer.setVisibility(true);
      PopupChangeEmail.hbxEmailFooterContainer.setVisibility(true);
      showEmailSelectionPopup();
    }else{
      gblBasicInfo.email 					= uniqueEmails[0];
      printMessage("Inside - gblBasicInfo.email "+gblBasicInfo.email);
      
      setInternationalizationCode();
     
      /*gblBasicInfo.languages			= [];
      gblBasicInfo.languages.push({"code":"en_GB", "name":"English"});
      //gblBasicInfo.languages.push({"code":"es_ES", "name":"Spanish"});
      gblBasicInfo.languages.push({"code":"en_US", "name":"US"});

      if (isEmpty(gblBasicInfo.selLanguage)){
        gblBasicInfo.selLanguage		= "en_GB";
      }*/
      var isEmpAlreayLoggedIn			= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
      if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
         selectediCabServerCode   = getSelectedSourceValue();
         gblBasicInfo.selLanguage = kony.store.getItem(LAConstants.SEL_LAN_CODE);
      }
      printMessage("Selected Lang Value SEL_LAN_CODE::: "+kony.store.getItem(LAConstants.SEL_LAN_CODE));
      printMessage("Selected Lang Value SAVED_LANG_CODE:: "+kony.store.getItem(LAConstants.SAVED_LANG_CODE));
      //skipping the popupshowing if we are changing the source
      if(!gblSourceChange){
      var totalSettingsItemCount				= gblBasicInfo.iCabInstances.length + gblBasicInfo.languages.length;
      if (totalSettingsItemCount > 3) {
        printMessage("showing--->> - getIcabInstances returned empty records1");	
        printMessage("showing--->> - getIcabInstances returned empty records11");
        printMessage("showing--->> - getIcabInstances returned empty records11"+JSON.stringify(gblBasicInfo));
        showOverlayPopupOnLoginScreen(true, false, gblBasicInfo.iCabInstances, gblBasicInfo.languages, selectediCabServerCode, gblBasicInfo.selLanguage);
      } else {
        printMessage("showing--->> - getIcabInstances returned empty records222");
        printMessage("showing--->> - getIcabInstances returned empty records222"+JSON.stringify(gblBasicInfo));
        showOverlayPopupOnLoginScreen(true, true, gblBasicInfo.iCabInstances, gblBasicInfo.languages, selectediCabServerCode, gblBasicInfo.selLanguage);
      }
      
      }
      else{
         printMessage(" skipped popup showing--->> - getIcabInstances ");
        gblBasicInfo.selICabServerCode =selectediCabServerCode;
        
        startSyncEmpService();
        gblSourceChange =false;
        gblCheckSelectedIcab ="";
      }
    }
  }else{
    /* 	printMessage("Error - getIcabInstances returned empty records");
      	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      	dismissLoading();*/
    printMessage("Error - getIcabInstances returned empty records "+glbflagTocheckIcabSync);
    if(glbflagTocheckIcabSync){
      if (isNetworkAvailable()){
        startSyncICabInstances();
        glbflagTocheckIcabSync =false;
      }else {
        var isEmpAlreayLoggedIn				= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true){
          printMessage(" Network not available to sync data ");
          dismissLoading();
        } else {
          showMessagePopup("common.message.noNetwork", ActionPopupMessageOk);//ActionDoExitApplication
          dismissLoading();
        }
        dismissConfirmPopup();
      }
    }else{
      printMessage("Error - success_getiCabInstances returned empty records");
      dismissLoading();
      showMessagePopup("common.message.1004", ActionDoExitApplication);
    }
  }
}

/** Error callback - SQL query execution - get iCabInstances **/
function error_getiCabInstances(error) {
  	printMessage("Inside - error_getiCabInstances");
  	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
    dismissLoading();
}

/** Success callback - SQL query execution - get employee record **/
function success_getEmployee(result){
  	printMessage("Inside - success_getEmployee");
	if(result !== null && result.length > 0) {
      	var rowItem 					= result[0];
      	printMessage("Inside - success_getEmployee"+JSON.stringify(rowItem));
      	printMessage("Inside - success_getEmployee"+rowItem.businessCode);
      //	gblBasicInfo.email             = rowItem.employeeWorkEmail;      	       
      	gblBasicInfo.businessCode      = rowItem.businessCode;
      	gblBasicInfo.countryCode	   = rowItem.countryCode;
      	setGblUserObject(rowItem);      
    	updateWelcomeLabel();
        
      	getLeadsFromLocalTbl(success_getLeadsForDisplaying, error_getLeadsForDisplaying, true);
    } else {
      	printMessage("Error - getEmployee returned empty records");
      	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      	clearStoreData();
         kony.store.setItem(LAConstants.SELECTED_SOURCE_VALUE, "");
      	//showMessagePopup("common.message.0001", ActionOnMessagePopupOkBtn);
      	dismissProgressMessage();
      	dismissLoading();
    }
}

/** Error callback - SQL query execution - get employee record **/
function error_getEmployee(error) {
  	printMessage("Inside - error_getEmployee");
    showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
    dismissProgressMessage();
    dismissLoading();
}

/** The method called on success of initial sync  **/
function onInitialSyncDone(){
  	kony.store.setItem(LAConstants.IS_EMP_LOGGED_IN, true);
    setEmpProfielInStore(gblEmployeeObject); 
    //getWorkOrdersFromLocalTbl();
}

/** To get employee profile from store **/
function getEmpProfielFromStore(){
  	var empProfileStr						= kony.store.getItem(LAConstants.EMP_PROFILE);
  	var empProfileObj						= JSON.parse(empProfileStr);
  	return empProfileObj;
}

/** To store employee profile in store **/
function setEmpProfielInStore(empProfileObj){
  	var empProfileStr						= JSON.stringify(empProfileObj);
  	kony.store.setItem(LAConstants.EMP_PROFILE, empProfileStr);
}

/** To get appDataObject from store **/
function getAppDataObjFromStore(){
  	var appDataObjStr						= kony.store.getItem(LAConstants.APP_DATA_OBJECT);
  	var appDataObj							= null;
  	if (!isEmpty(appDataObjStr)) {
      	appDataObj							= JSON.parse(appDataObjStr);
    }
  	return appDataObj;
}

/** To store appDataObject in store **/
function setAppDataObjInStore(){
  	var appDataObjStr						= JSON.stringify(gblMyAppDataObject);
  	kony.store.setItem(LAConstants.APP_DATA_OBJECT, appDataObjStr);
}

/** To update welcome label text **/
function updateWelcomeLabel(){
  	Login.lblUsername.text				= gblEmployeeObject.getEmployeeName();
   //alert('gblEmployeeObject.getEmployeeName();'+gblBasicInfo.iCabInstances.email);
  
   if(gblIsLaunchedAsStandAloneApp){
    //frmObj.lblUserName.text = gblBasicInfo.email;
     Login.lblUsername.text = gblEmployeeObject.firstName+" " +gblEmployeeObject.lastName;
    }else{
       	if ((!isEmpty(gblEmployeeObject)) && (!isEmpty(gblEmployeeObject.firstName)) && (!isEmpty(gblEmployeeObject.lastName)) ) {
           Login.lblUsername.text = gblEmployeeObject.firstName+" " +gblEmployeeObject.lastName;
        }else{
          Login.lblUsername.text =  gblBasicInfo.email;
        }
    }
  
  
}

/** On click Emails popup Cancel button action **/
function doExitApplication(){
    exitApplication();
}


/**
* Displaying Waring for changing for Source from BirgurMenu
*/
function displayChangingSourceWaring(){
  kony.print("Inside displayChangingSourceWaring :::: "+kony.application.getCurrentForm().id);
  if (kony.application.getCurrentForm().id !== "Login") {
    showConfirmPopup("common.message.1005", ActionConfirmSourceChange, ActionDeclineChangeEmail);
  }else{
    startSyncEmpService();
  }
}

function getSelectedSourceValue(){
  	var sourceValue						= kony.store.getItem(LAConstants.SELECTED_SOURCE_VALUE);
  	return sourceValue;
}

/** To store SourceValue in store **/
function setSelectedSourceValue(sourceValue){
  	kony.store.setItem(LAConstants.SELECTED_SOURCE_VALUE, sourceValue);
}
