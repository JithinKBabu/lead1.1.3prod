//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Tue Nov 06 16:41:57 UTC 2018alerts*******************
// **********************************Start alerts's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}




/************************************************************************************
* Creates new alerts
*************************************************************************************/
alerts = function(){
	this.businessCode = null;
	this.countryCode = null;
	this.alertId = null;
	this.authorEmailAddress = null;
	this.accountCode = null;
	this.contractCode = null;
	this.propertyCode = null;
	this.propertyContactName = null;
	this.propertyContactPosition = null;
	this.propertyContactTelephone = null;
	this.propertyContactMobile = null;
	this.propertyContactEmail = null;
	this.propertyContactFax = null;
	this.ticketSummary = null;
	this.ticketNotes = null;
	this.createdDate = null;
	this.createdTime = null;
	this.updateDateTime = null;
	this.alertDeleted = null;
	this.email = null;
	this.languageCode = null;
	this.sourceApplication = null;
	this.markForUpload = true;
};

alerts.prototype = {
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get alertId(){
		return this._alertId;
	},
	set alertId(val){
		this._alertId = val;
	},
	get authorEmailAddress(){
		return this._authorEmailAddress;
	},
	set authorEmailAddress(val){
		this._authorEmailAddress = val;
	},
	get accountCode(){
		return this._accountCode;
	},
	set accountCode(val){
		this._accountCode = val;
	},
	get contractCode(){
		return this._contractCode;
	},
	set contractCode(val){
		this._contractCode = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get propertyContactName(){
		return this._propertyContactName;
	},
	set propertyContactName(val){
		this._propertyContactName = val;
	},
	get propertyContactPosition(){
		return this._propertyContactPosition;
	},
	set propertyContactPosition(val){
		this._propertyContactPosition = val;
	},
	get propertyContactTelephone(){
		return this._propertyContactTelephone;
	},
	set propertyContactTelephone(val){
		this._propertyContactTelephone = val;
	},
	get propertyContactMobile(){
		return this._propertyContactMobile;
	},
	set propertyContactMobile(val){
		this._propertyContactMobile = val;
	},
	get propertyContactEmail(){
		return this._propertyContactEmail;
	},
	set propertyContactEmail(val){
		this._propertyContactEmail = val;
	},
	get propertyContactFax(){
		return this._propertyContactFax;
	},
	set propertyContactFax(val){
		this._propertyContactFax = val;
	},
	get ticketSummary(){
		return this._ticketSummary;
	},
	set ticketSummary(val){
		this._ticketSummary = val;
	},
	get ticketNotes(){
		return this._ticketNotes;
	},
	set ticketNotes(val){
		this._ticketNotes = val;
	},
	get createdDate(){
		return this._createdDate;
	},
	set createdDate(val){
		this._createdDate = val;
	},
	get createdTime(){
		return this._createdTime;
	},
	set createdTime(val){
		this._createdTime = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
	get alertDeleted(){
		return kony.sync.getBoolean(this._alertDeleted)+"";
	},
	set alertDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute alertDeleted in alerts.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._alertDeleted = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get languageCode(){
		return this._languageCode;
	},
	set languageCode(val){
		this._languageCode = val;
	},
	get sourceApplication(){
		return this._sourceApplication;
	},
	set sourceApplication(val){
		this._sourceApplication = val;
	},
};

/************************************************************************************
* Retrieves all instances of alerts SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "businessCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "countryCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* alerts.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
alerts.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering alerts.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	orderByMap = kony.sync.formOrderByClause("alerts",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering alerts.getAll->successcallback");
		successcallback(alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of alerts present in local database.
*************************************************************************************/
alerts.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering alerts.getAllCount function");
	alerts.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of alerts using where clause in the local Database
*************************************************************************************/
alerts.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering alerts.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering alerts.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of alerts in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  alerts.prototype.create function");
	var valuestable = this.getValuesTable(true);
	alerts.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
alerts.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  alerts.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "alerts.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"alerts",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  alerts.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = alerts.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "alertId=" + valuestable.alertId;
		pks["alertId"] = {key:"alertId",value:valuestable.alertId};
		alerts.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of alerts in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].alertId = "alertId_0";
*		valuesArray[0].authorEmailAddress = "authorEmailAddress_0";
*		valuesArray[0].accountCode = "accountCode_0";
*		valuesArray[0].contractCode = "contractCode_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].propertyContactName = "propertyContactName_0";
*		valuesArray[0].propertyContactPosition = "propertyContactPosition_0";
*		valuesArray[0].propertyContactTelephone = "propertyContactTelephone_0";
*		valuesArray[0].propertyContactMobile = "propertyContactMobile_0";
*		valuesArray[0].propertyContactEmail = "propertyContactEmail_0";
*		valuesArray[0].propertyContactFax = "propertyContactFax_0";
*		valuesArray[0].ticketSummary = "ticketSummary_0";
*		valuesArray[0].ticketNotes = "ticketNotes_0";
*		valuesArray[0].createdDate = "createdDate_0";
*		valuesArray[0].createdTime = "createdTime_0";
*		valuesArray[0].updateDateTime = "updateDateTime_0";
*		valuesArray[0].alertDeleted = true;
*		valuesArray[0].email = "email_0";
*		valuesArray[0].languageCode = "languageCode_0";
*		valuesArray[0].sourceApplication = "sourceApplication_0";
*		valuesArray[1] = {};
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].alertId = "alertId_1";
*		valuesArray[1].authorEmailAddress = "authorEmailAddress_1";
*		valuesArray[1].accountCode = "accountCode_1";
*		valuesArray[1].contractCode = "contractCode_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].propertyContactName = "propertyContactName_1";
*		valuesArray[1].propertyContactPosition = "propertyContactPosition_1";
*		valuesArray[1].propertyContactTelephone = "propertyContactTelephone_1";
*		valuesArray[1].propertyContactMobile = "propertyContactMobile_1";
*		valuesArray[1].propertyContactEmail = "propertyContactEmail_1";
*		valuesArray[1].propertyContactFax = "propertyContactFax_1";
*		valuesArray[1].ticketSummary = "ticketSummary_1";
*		valuesArray[1].ticketNotes = "ticketNotes_1";
*		valuesArray[1].createdDate = "createdDate_1";
*		valuesArray[1].createdTime = "createdTime_1";
*		valuesArray[1].updateDateTime = "updateDateTime_1";
*		valuesArray[1].alertDeleted = true;
*		valuesArray[1].email = "email_1";
*		valuesArray[1].languageCode = "languageCode_1";
*		valuesArray[1].sourceApplication = "sourceApplication_1";
*		valuesArray[2] = {};
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].alertId = "alertId_2";
*		valuesArray[2].authorEmailAddress = "authorEmailAddress_2";
*		valuesArray[2].accountCode = "accountCode_2";
*		valuesArray[2].contractCode = "contractCode_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].propertyContactName = "propertyContactName_2";
*		valuesArray[2].propertyContactPosition = "propertyContactPosition_2";
*		valuesArray[2].propertyContactTelephone = "propertyContactTelephone_2";
*		valuesArray[2].propertyContactMobile = "propertyContactMobile_2";
*		valuesArray[2].propertyContactEmail = "propertyContactEmail_2";
*		valuesArray[2].propertyContactFax = "propertyContactFax_2";
*		valuesArray[2].ticketSummary = "ticketSummary_2";
*		valuesArray[2].ticketNotes = "ticketNotes_2";
*		valuesArray[2].createdDate = "createdDate_2";
*		valuesArray[2].createdTime = "createdTime_2";
*		valuesArray[2].updateDateTime = "updateDateTime_2";
*		valuesArray[2].alertDeleted = true;
*		valuesArray[2].email = "email_2";
*		valuesArray[2].languageCode = "languageCode_2";
*		valuesArray[2].sourceApplication = "sourceApplication_2";
*		alerts.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
alerts.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering alerts.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"alerts",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "alertId=" + valuestable.alertId;
				pks["alertId"] = {key:"alertId",value:valuestable.alertId};
				var wcs = [];
				if(alerts.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  alerts.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  alerts.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = alerts.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates alerts using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  alerts.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	alerts.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
alerts.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  alerts.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(alerts.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"alerts",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = alerts.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates alerts(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering alerts.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"alerts",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  alerts.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, alerts.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = alerts.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, alerts.getPKTable());
	}
};

/************************************************************************************
* Updates alerts(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.businessCode = "businessCode_updated0";
*		inputArray[0].changeSet.countryCode = "countryCode_updated0";
*		inputArray[0].changeSet.authorEmailAddress = "authorEmailAddress_updated0";
*		inputArray[0].changeSet.accountCode = "accountCode_updated0";
*		inputArray[0].whereClause = "where alertId = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.businessCode = "businessCode_updated1";
*		inputArray[1].changeSet.countryCode = "countryCode_updated1";
*		inputArray[1].changeSet.authorEmailAddress = "authorEmailAddress_updated1";
*		inputArray[1].changeSet.accountCode = "accountCode_updated1";
*		inputArray[1].whereClause = "where alertId = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.businessCode = "businessCode_updated2";
*		inputArray[2].changeSet.countryCode = "countryCode_updated2";
*		inputArray[2].changeSet.authorEmailAddress = "authorEmailAddress_updated2";
*		inputArray[2].changeSet.accountCode = "accountCode_updated2";
*		inputArray[2].whereClause = "where alertId = '2'";
*		alerts.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
alerts.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering alerts.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898cf65f1fb";
	var tbname = "alerts";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"alerts",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, alerts.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  alerts.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, alerts.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  alerts.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = alerts.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes alerts using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering alerts.prototype.deleteByPK function");
	var pks = this.getPKTable();
	alerts.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
alerts.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering alerts.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(alerts.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function alertsTransactionCallback(tx){
		sync.log.trace("Entering alerts.deleteByPK->alerts_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function alertsErrorCallback(){
		sync.log.error("Entering alerts.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function alertsSuccessCallback(){
		sync.log.trace("Entering alerts.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering alerts.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, alertsTransactionCallback, alertsSuccessCallback, alertsErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes alerts(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. alerts.remove("where businessCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
alerts.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering alerts.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function alerts_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function alerts_removeSuccess(){
		sync.log.trace("Entering alerts.remove->alerts_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering alerts.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering alerts.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, alerts_removeTransactioncallback, alerts_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes alerts using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
alerts.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering alerts.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	alerts.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
alerts.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering alerts.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(alerts.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function alertsTransactionCallback(tx){
		sync.log.trace("Entering alerts.removeDeviceInstanceByPK -> alertsTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function alertsErrorCallback(){
		sync.log.error("Entering alerts.removeDeviceInstanceByPK -> alertsErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function alertsSuccessCallback(){
		sync.log.trace("Entering alerts.removeDeviceInstanceByPK -> alertsSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering alerts.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, alertsTransactionCallback, alertsSuccessCallback, alertsErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes alerts(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
alerts.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering alerts.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function alerts_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function alerts_removeSuccess(){
		sync.log.trace("Entering alerts.remove->alerts_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering alerts.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering alerts.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, alerts_removeTransactioncallback, alerts_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves alerts using primary key from the local Database. 
*************************************************************************************/
alerts.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering alerts.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	alerts.getAllDetailsByPK(pks,successcallback,errorcallback);
};
alerts.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering alerts.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var wcs = [];
	if(alerts.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering alerts.getAllDetailsByPK-> success callback function");
		successcallback(alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves alerts(s) using where clause from the local Database. 
* e.g. alerts.find("where businessCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
alerts.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering alerts.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of alerts with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering alerts.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	alerts.markForUploadbyPK(pks, successcallback, errorcallback);
};
alerts.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering alerts.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(alerts.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of alerts matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
alerts.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering alerts.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering alerts.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering alerts.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering alerts.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of alerts pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
alerts.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering alerts.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of alerts pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
alerts.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering alerts.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of alerts deferred for upload.
*************************************************************************************/
alerts.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering alerts.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, alerts.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to alerts in local database to last synced state
*************************************************************************************/
alerts.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering alerts.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to alerts's record with given primary key in local 
* database to last synced state
*************************************************************************************/
alerts.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering alerts.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	alerts.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
alerts.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering alerts.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var wcs = [];
	if(alerts.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering alerts.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether alerts's record  
* with given primary key got deferred in last sync
*************************************************************************************/
alerts.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  alerts.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	alerts.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
alerts.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering alerts.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var wcs = [] ;
	var flag;
	if(alerts.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether alerts's record  
* with given primary key is pending for upload
*************************************************************************************/
alerts.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  alerts.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	alerts.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
alerts.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering alerts.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "alerts.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = alerts.getTableName();
	var wcs = [] ;
	var flag;
	if(alerts.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering alerts.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
alerts.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering alerts.removeCascade function");
	var tbname = alerts.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


alerts.convertTableToObject = function(res){
	sync.log.trace("Entering alerts.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new alerts();
			obj.businessCode = res[i].businessCode;
			obj.countryCode = res[i].countryCode;
			obj.alertId = res[i].alertId;
			obj.authorEmailAddress = res[i].authorEmailAddress;
			obj.accountCode = res[i].accountCode;
			obj.contractCode = res[i].contractCode;
			obj.propertyCode = res[i].propertyCode;
			obj.propertyContactName = res[i].propertyContactName;
			obj.propertyContactPosition = res[i].propertyContactPosition;
			obj.propertyContactTelephone = res[i].propertyContactTelephone;
			obj.propertyContactMobile = res[i].propertyContactMobile;
			obj.propertyContactEmail = res[i].propertyContactEmail;
			obj.propertyContactFax = res[i].propertyContactFax;
			obj.ticketSummary = res[i].ticketSummary;
			obj.ticketNotes = res[i].ticketNotes;
			obj.createdDate = res[i].createdDate;
			obj.createdTime = res[i].createdTime;
			obj.updateDateTime = res[i].updateDateTime;
			obj.alertDeleted = res[i].alertDeleted;
			obj.email = res[i].email;
			obj.languageCode = res[i].languageCode;
			obj.sourceApplication = res[i].sourceApplication;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

alerts.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering alerts.filterAttributes function");
	var attributeTable = {};
	attributeTable.businessCode = "businessCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.alertId = "alertId";
	attributeTable.authorEmailAddress = "authorEmailAddress";
	attributeTable.accountCode = "accountCode";
	attributeTable.contractCode = "contractCode";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.propertyContactName = "propertyContactName";
	attributeTable.propertyContactPosition = "propertyContactPosition";
	attributeTable.propertyContactTelephone = "propertyContactTelephone";
	attributeTable.propertyContactMobile = "propertyContactMobile";
	attributeTable.propertyContactEmail = "propertyContactEmail";
	attributeTable.propertyContactFax = "propertyContactFax";
	attributeTable.ticketSummary = "ticketSummary";
	attributeTable.ticketNotes = "ticketNotes";
	attributeTable.createdDate = "createdDate";
	attributeTable.createdTime = "createdTime";
	attributeTable.updateDateTime = "updateDateTime";
	attributeTable.alertDeleted = "alertDeleted";
	attributeTable.email = "email";
	attributeTable.languageCode = "languageCode";
	attributeTable.sourceApplication = "sourceApplication";

	var PKTable = {};
	PKTable.alertId = {}
	PKTable.alertId.name = "alertId";
	PKTable.alertId.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject alerts. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject alerts. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject alerts. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

alerts.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering alerts.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = alerts.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

alerts.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering alerts.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.businessCode = this.businessCode;
	valuesTable.countryCode = this.countryCode;
	if(isInsert===true){
		valuesTable.alertId = this.alertId;
	}
	valuesTable.authorEmailAddress = this.authorEmailAddress;
	valuesTable.accountCode = this.accountCode;
	valuesTable.contractCode = this.contractCode;
	valuesTable.propertyCode = this.propertyCode;
	valuesTable.propertyContactName = this.propertyContactName;
	valuesTable.propertyContactPosition = this.propertyContactPosition;
	valuesTable.propertyContactTelephone = this.propertyContactTelephone;
	valuesTable.propertyContactMobile = this.propertyContactMobile;
	valuesTable.propertyContactEmail = this.propertyContactEmail;
	valuesTable.propertyContactFax = this.propertyContactFax;
	valuesTable.ticketSummary = this.ticketSummary;
	valuesTable.ticketNotes = this.ticketNotes;
	valuesTable.createdDate = this.createdDate;
	valuesTable.createdTime = this.createdTime;
	valuesTable.updateDateTime = this.updateDateTime;
	valuesTable.alertDeleted = this.alertDeleted;
	valuesTable.email = this.email;
	valuesTable.languageCode = this.languageCode;
	valuesTable.sourceApplication = this.sourceApplication;
	return valuesTable;
};

alerts.prototype.getPKTable = function(){
	sync.log.trace("Entering alerts.prototype.getPKTable function");
	var pkTable = {};
	pkTable.alertId = {key:"alertId",value:this.alertId};
	return pkTable;
};

alerts.getPKTable = function(){
	sync.log.trace("Entering alerts.getPKTable function");
	var pkTable = [];
	pkTable.push("alertId");
	return pkTable;
};

alerts.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering alerts.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key alertId not specified in  " + opName + "  an item in alerts");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("alertId",opName,"alerts")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.alertId)){
			if(!kony.sync.isNull(pks.alertId.value)){
				wc.key = "alertId";
				wc.value = pks.alertId.value;
			}
			else{
				wc.key = "alertId";
				wc.value = pks.alertId;
			}
		}else{
			sync.log.error("Primary Key alertId not specified in  " + opName + "  an item in alerts");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("alertId",opName,"alerts")));
			return false;
		}
	}
	else{
		wc.key = "alertId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

alerts.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering alerts.validateNull function");
	return true;
};

alerts.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering alerts.validateNullInsert function");
	if(kony.sync.isNull(valuestable.alertId) || kony.sync.isEmptyString(valuestable.alertId)){
		sync.log.error("Mandatory attribute alertId is missing for the SyncObject alerts.");
		errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "alerts", "alertId")));
		return false;
	}
	return true;
};

alerts.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering alerts.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


alerts.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

alerts.getTableName = function(){
	return "alerts";
};




// **********************************End alerts's helper methods************************