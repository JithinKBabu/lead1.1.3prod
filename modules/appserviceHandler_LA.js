

/*	Capture information passed from parent application and save in gblSharedInfo      
  		gblSharedInfo 					=> This global JSON object should be used for storing shared information from parent app. Format given below
  	 
     	gblSharedInfo.email				=> Email address from parent application which is currently used for login (Technician)
     	gblSharedInfo.countryCode		=> Country code from parent application which is currently used for login (Technician)
        gblSharedInfo.businessCode		=> Business code from parent application      
        gblSharedInfo.languageCode		=> Country code from parent application 
        
    	gblSharedInfo.mode				=> CREATE_ALERT or CREATE_LEAD or LEAD_HISTORY
        gblSharedInfo.propertyName		=> Property name
        gblSharedInfo.contractNumber	=> Contract number
        gblSharedInfo.propertyCode		=> Property code
        gblSharedInfo.address1			=> Contact address line 1
        gblSharedInfo.address2			=> Contact address line 2
        gblSharedInfo.address3			=> Contact address line 3
        gblSharedInfo.address4			=> Contact address line 4
        gblSharedInfo.address5			=> Contact address line 5
        gblSharedInfo.postcode			=> Contact address postcode
        gblSharedInfo.contactName		=> Contact name
        gblSharedInfo.contactPosition	=> Contact position
        gblSharedInfo.contactPhone		=> Contact phone
        gblSharedInfo.contactEmail		=> Contact email
*/
        
function appserviceHandler(params) {
  	var startUpFormHandler				= Login;
    if (!isEmpty(params) && !isEmpty(params.launchparams)) {
      	gblIsLaunchedAsStandAloneApp	= false;
        printMessage("params.launchparams: "+params.launchparams+" and params.launchmode: "+params.launchmode);
        printMessage("params.launchparams.form2go:  "+params.launchparams.form2go);
        //var decodedParamValue			= decodeURI(params.launchparams.ParamstoSends);
      
      //===================================================================================
      //=====================================================================================
       /* var ParamstoSends='{"email":"service.trak1@rentokil-initial.com","ccode":"SG","bcode":"K","pname":"ITE College Central - Blk H Student Block","cnum":"30024169","pcode":"300241698","add1":"2 Ang Mo Kio Drive","add2":"Test","add3":"","add4":"#Singapore","add5":"","po":"567720","cname":"Mr Wen Dong","cpos":"PIC","cph":"97425458","cemail":"","acntcode":"100002545"}';
      	var encodedParamValue 			= encodeURIComponent(ParamstoSends);
      	printMessage("encodedParamValue:  "+encodedParamValue);
      	var decodedParamValue			= decodeURIComponent(ParamstoSends);
      	printMessage("decodedParamValue:  "+decodedParamValue);
      	var decodedJsonValue			= JSON.parse(decodedParamValue);
        printMessage("decodedParamValue: " + JSON.stringify(decodedJsonValue));*/
        printMessage("before decodedParamValue: " + JSON.stringify(params.launchparams.ParamstoSends));
      	var decodedParamValue			= decodeURIComponent(params.launchparams.ParamstoSends);
        printMessage("after decodedParamValue: " + decodedParamValue);
      	var decodedJsonValue			= JSON.parse(decodedParamValue);
      	printMessage("after decodedJsonValue: " + JSON.stringify(decodedJsonValue));
       
      
      /*var decodedParamValue			= decodeURIComponent(params.launchparams.ParamstoSends);
      	var decodedJsonValue			= JSON.parse(decodedParamValue);
        printMessage("decodedParamValue: " + JSON.stringify(decodedJsonValue));*/
      
      	gblBasicInfo					= {};
      	gblBasicInfo.email				= decodedJsonValue.email;			
        gblBasicInfo.countryCode		= decodedJsonValue.ccode;		
        gblBasicInfo.businessCode		= decodedJsonValue.bcode;		
        gblBasicInfo.languageCode		= "ENG";
        gblBasicInfo.selICabServerCode  = decodedJsonValue.ccode+'_'+decodedJsonValue.bcode;
      	gblBasicInfo.selLanguage  		= "en_GB";
      
        gblSharedInfo 					= {};      	
        gblSharedInfo.email				= decodedJsonValue.email;			
        gblSharedInfo.countryCode		= decodedJsonValue.ccode;		
        gblSharedInfo.businessCode		= decodedJsonValue.bcode;		
        gblSharedInfo.languageCode		= "ENG";		
        gblSharedInfo.mode				= decodedJsonValue.form2go;			
        gblSharedInfo.propertyName		= decodedJsonValue.pname;
        gblSharedInfo.contractNumber	= decodedJsonValue.cnum;
        gblSharedInfo.propertyCode		= decodedJsonValue.pcode;
        gblSharedInfo.address1			= decodedJsonValue.add1;		
        gblSharedInfo.address2			= decodedJsonValue.add2;		
        gblSharedInfo.address3			= decodedJsonValue.add3;		
        gblSharedInfo.address4			= decodedJsonValue.add4;		
        gblSharedInfo.address5			= decodedJsonValue.add5;		
        gblSharedInfo.postcode			= decodedJsonValue.po;		
        gblSharedInfo.contactName 		= decodedJsonValue.cname;	
        gblSharedInfo.contactPosition	= decodedJsonValue.cpos;	
        gblSharedInfo.contactPhone		= decodedJsonValue.cph;	
      
       	if(!isEmpty(decodedJsonValue.cemail)){
        	gblSharedInfo.contactEmail	= decodedJsonValue.cemail.trim();
        }else{
          	gblSharedInfo.contactEmail	= ""  ;
        }
        //gblSharedInfo.contactEmail		= decodedJsonValue.cemail.trim();
        gblSharedInfo.accountCode 		= decodedJsonValue.acntcode;
      	printMessage("email---> " + gblBasicInfo.email);
      	printMessage("gblSharedInfo---> " + JSON.stringify(gblSharedInfo));
      	printMessage("gblBasicInfo---> " + JSON.stringify(gblBasicInfo));
      
        // Save selected busines and contry code
       var selectediCabServerCode = gblBasicInfo.countryCode+'_'+gblBasicInfo.businessCode;
      	printMessage("selectediCabServerCode---> " + selectediCabServerCode);
      	setSelectedSourceValue(selectediCabServerCode);
      	//Checking Adrees is empty or not
      	if(isEmpty(gblSharedInfo.address1)&& isEmpty(gblSharedInfo.address2)){
          
          gblLeadsAddressIsEmpty = true;
        }else{
          gblLeadsAddressIsEmpty = false;
        }
      
      	printMessage("Event Type---> " + params.launchparams.form2go);
      	if (params.launchparams.form2go == "alerts") {
          	startUpFormHandler			= frmAlert;
        } else if (params.launchparams.form2go == "leads") {
          	startUpFormHandler			= frmLead;
        } else {
          	startUpFormHandler			= frmLead;
        }        
      
      	printMessage("PARENT_APP_PACKAGE_NAME---> " + params.launchparams.packageName);
		LAConstants.PARENT_APP_PACKAGE_NAME = params.launchparams.packageName;
      
    } else {
        gblBasicInfo					= {};
        gblSharedInfo 					= {};     
      	gblIsLaunchedAsStandAloneApp	= true;      	
    }
  	gblCurrentPageIndex					= 0;
  	printMessage("Inside appserviceHandler - startUpFormHandler: " + startUpFormHandler);
	return startUpFormHandler; 	
}
