/*** Author  : RI - CTS - 363601 ****/


function LAConstants() {

}

/** Build changes starts: Change below settings based on environment **/
LAConstants.APP_SERVER_ON				= "CLOUD"; 	//["CLOUD" or "PREMISES"]
LAConstants.APP_MODE					= "SIT"; 	//["SIT" or "STUB"]

/** Change below settings based on environment: Build changes ends **/

//LAConstants.PARENT_APP_PACKAGE_NAME		= "com.orgname.ServicePest";
LAConstants.PARENT_APP_PACKAGE_NAME		= "com.kony.PestServiceT";

LAConstants.FORM_LEADS					= "frmLead";
LAConstants.FORM_ALERTS					= "frmAlert";
LAConstants.FORM_LEAD_MANAGEMENT		= "frmLeadManagement";
LAConstants.FORM_SUPPORTVIEW 			= "SupportView";
LAConstants.SYNC_LEAD_HISTORY			= "SYNC_LEAD_HISTORY";
LAConstants.SYNC_ICAB_INSTANCES		    = "SYNC_ICAB_INSTANCES";
LAConstants.SYNC_EMP_DATA			    = "SYNC_EMP_DATA";

//LAConstants.SYNC_LEAD_AND_ALERT_UPLOADS	= "SYNC_LEAD_AND_ALERT_UPLOADS";

LAConstants.SERVICE_HTTP_STATUS			= 200;
LAConstants.SERVICE_OPSTATUS			= 0;
LAConstants.SERVICE_MSG_SUCCESS			= "SUCCESS";
LAConstants.SYNC_BATCH_SIZE				= "1000";
LAConstants.SYNC_CHUNK_SIZE				= "512";
LAConstants.SYNC_TIMEOUT				= 3600; 
LAConstants.SYNC_RETRY_ATTEMPTS			= 3;
LAConstants.SYNC_RETRY_WAIT_TIME		= 10;
LAConstants.SYNC_SCHEDULER_TIME			= 15; /** seconds **/
LAConstants.SYNC_UPLOAD_SYNC_TIMER		= 2; //15; /** minutes **/

//LAConstants.INTEGRATION_CLIENT_ID		= "76d8be62a3cd44aaad73cb03e5efdba1"; // SIT
//LAConstants.INTEGRATION_CLIENT_SECRET	= "879d0a48ba4143d4BBCB922787C39627"; // SIT

//LAConstants.INTEGRATION_CLIENT_ID		= "7990f612e27e4c0dbc7a97251f1d9375"; // STG NA
//LAConstants.INTEGRATION_CLIENT_SECRET	= "2E1e622C5092456bB47C7ebe5489bF02"; // STG NA

LAConstants.INTEGRATION_CLIENT_ID		= "9af66f91c04549a89e41cd3111490044"; // PROD NA
LAConstants.INTEGRATION_CLIENT_SECRET	= "6e38D726dC9c4c3BA89c54A1835c71EA"; // PROD NA

//LAConstants.INTEGRATION_CLIENT_ID		= "c89acbf594ec450480d9dbf518a0f4fe"; // STG
//LAConstants.INTEGRATION_CLIENT_SECRET	= "0c3d0df4ffc74563BDF5AA7D001F5CAC"; // STG
//LAConstants.INTEGRATION_CLIENT_ID	    = "a6205c3eb6ff49c6968624c7e9642753"; // STG ASIA
//LAConstants.INTEGRATION_CLIENT_SECRET   = "1101541d10784a5185DE1435ED6A73D4"; // STG ASIA
//LAConstants.INTEGRATION_CLIENT_ID	    = "e9931c4fd8b74b5897429c5dc7d6595f"; // PROD ASIA
//LAConstants.INTEGRATION_CLIENT_SECRET   = "6e0ab43e7d8e421580517CF1BF9DB3D8"; // PROD ASIA
//LAConstants.INTEGRATION_CLIENT_ID	    = "5b9066aa2b35436099f439f92799d110"; // PROD EMEA
//LAConstants.INTEGRATION_CLIENT_SECRET   = "8ba56d39780848d6B421635519C796EC"; // PROD EMEA


LAConstants.SYNC_USER_ID				= "";
LAConstants.SYNC_PASSWORD				= "";
//LAConstants.SYNC_APP_ID					= "LeadsAndAlertsASIA";  //ASIA PROD
//LAConstants.SYNC_APP_ID	                = "LeadsAndAlertsNASTG";  //PROD EMEA
//LAConstants.SYNC_APP_ID					= "100004898e984e208"; //Dev2 Asia
//LAConstants.SYNC_APP_ID					= "LeadsAndAlertsEMEASTG"; //Dev2 EMEA
//LAConstants.SYNC_APP_ID					= "100004898bea95b1b"; //Dev
//LAConstants.SYNC_APP_ID					= "1000048984db3ef33"; //SIT
//LAConstants.SYNC_SERVER_HOST_CLOUD		= "rentokil-initial-dev.sync.konycloud.com"; //SIT
//LAConstants.SYNC_SERVER_HOST_CLOUD		= "rentokil-initial-dev2.konycloud.com"; //STG
LAConstants.SYNC_SERVER_HOST_CLOUD		= "rentokil-initial.konycloud.com";
//LAConstants.SYNC_SERVER_HOST_CLOUD		= "rentokil-initial-stg.konycloud.com";

LAConstants.SYNC_SERVER_HOST_PREMISES	= "wklxkonyappts05.uk.ri.ad";
LAConstants.SYNC_IS_SECURE_CLOUD		= true;
LAConstants.SYNC_IS_SECURE_PREMISES		= false;
LAConstants.SYNC_ERR_POLICY_CONTINUE	= "continueonerror"; 
LAConstants.SYNC_ERR_POLICY_ABORT		= "abortonerror";

LAConstants.DEF_COUNTRY_CODE			= "US";
LAConstants.DEF_BUSINESS_CODE			= "D";
LAConstants.DEF_LANGUAGE_CODE			= "ENG";
LAConstants.DEF_LOCALE					= "en_GB";
LAConstants.DEF_EMAIL_ADDRESS			= "";

LAConstants.DISPLAY_SYNC_DATE_TIME		= "DISPLAY_SYNC_DATE_TIME_LA";
LAConstants.LAST_UPLOAD_SYNC_DATE_TIME	= "LAST_UPLOAD_SYNC_DATE_TIME_LA";

LAConstants.PAGINATION_DAYS_RANGE		= 28; // 28 days => 4 weeks
LAConstants.PAGINATION_DAYS_MAX_RANGE	= 357; // 357 days => 51 weeks
LAConstants.LEAD_DATE_FORMAT			= "yyyy-MM-dd";

LAConstants.LEAD_DATE_SYNC_FORMAT		= "YYYY-MM-DDTHH:mm:ss.SSS";


LAConstants.RADIO_BTN_ON			    = "radioon.png";
LAConstants.SAVED_LANG_CODE             = "ENG";
LAConstants.RADIO_BTN_OFF               = "radiooff.png";
LAConstants.FORM_LOGIN                  = "Login";
LAConstants.IS_EMP_LOGGED_IN            = "LA_KEY_IS_EMP_LOGGED_IN";
LAConstants.EMP_PROFILE				    = "LA_KEY_EMP_PROFILE";
LAConstants.SOURCE_SECTION              = 0;
LAConstants.LANGUAGE_SECTION            = 1;
LAConstants.SEL_LAN_CODE                = "LA_KEY_SEL_LAN_CODE";
LAConstants.APP_VERSION                 ="1.1.3";

LAConstants.LAST_SYNC_DATE				= "PC_KEY_LAST_SYNC_DATE";			/** To check daily sync **/
LAConstants.LAST_SYNC_DATE_TIME			= "PC_KEY_LAST_SYNC_DATE_TIME";
LAConstants.SELECTED_SOURCE_VALUE       ="SELECTED_SOURCE_VALUE_KEY";
LAConstants.PURGE_DB_DATE_TIME			= "LA_PURGE_DB_DATE_TIME";
LAConstants.PURGE_DB_DATE_SCHEDULE_TIME	= 15; //seconds
LAConstants.PURGE_DB_TIMER      		= 5; //minutes
//LAConstants.PURGE_DB_TIMER      		= 1440; //one day (in minutes)
LAConstants.PURGE_DB_SEVENDAYS_TIMER    = 1440 ; // (86400 - Seven days (in minutes))
LAConstants.PURGE_DB_TIME_SEVENDAYS    	= 7 ;// 7 Seven days (in minutes)
LAConstants.SUPPORTvIEW_ERROR_LOG_TIMER = 1440; //one day (in minutes)

gblLeadsObject 							= {};
gblAlertObject 							= {};
LAConstants.SYNC_UPLOAD_LEAD			= "SYNC_UPLOAD_LEAD";
LAConstants.SYNC_UPLOAD_ALERT			= "SYNC_UPLOAD_ALERT";

