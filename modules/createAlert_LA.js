/*** Author  : RI - CTS - 363601 ****/


/** Show create alert form **/
function showCreateAlert() {
  	printMessage("Inside - showCreateAlert");
  	frmAlert.show();
    gblPrevFormId					= "frmLeadManagement";
}




/** Postshow - Form - frmAlert **/
function onPostShowAlert(){
  
  	printMessage("Inside - onPostShowAlert");
  	showLoading("common.message.1001");
  	refreshSettingsUI();
    frmAlert.flxBurgerMenu.flexSettingBtn4Container.setVisibility(false);
  	frmAlert.flxFormHeader.lblHeaderTitle.text		= getI18nString("common.label.createAlert");
  	frmAlert.lblPropertyName.text		= getI18nString("common.label.propertyName");
  	frmAlert.PHtxtPropertyName.text		= getI18nString("common.placeholder.propertyName");
  	//frmAlert.lblPropertyCode.text		= getI18nString("common.label.propertyCode");
  	//frmAlert.PHtxtPropertyCode.text		= getI18nString("common.placeholder.propertyCode");
  	frmAlert.PHtxtContactName.text		= getI18nString("common.placeholder.contactName");  
  	frmAlert.PHtxtContactPosition.text	= getI18nString("common.placeholder.contactPosition");
  	frmAlert.PHtxtContactPhone.text		= getI18nString("common.placeholder.contactPhone");	
  	frmAlert.PHtxtContactEmail.text		= getI18nString("common.placeholder.contactEmail");
  	frmAlert.PHtxtCustomerReference.text= getI18nString("common.placeholder.customerReference");
  	frmAlert.PHtxtSummary.text			= getI18nString("common.placeholder.summary");
  	frmAlert.PHtxtNotes.text			= getI18nString("common.placeholder.notes");
  	frmAlert.btnCancel.text				= getI18nString("common.label.cancel");
  	frmAlert.btnConfirm.text			= getI18nString("common.label.confirm");
  
  
    if (gblIsLaunchedAsStandAloneApp){
      	frmAlert.flxBurgerMenu.lblUserName.text 			= gblBasicInfo.email;
      	frmAlert.txtPropertyName.setEnabled(true);
      	frmAlert.txtPropertyCode.setEnabled(true);
      	frmAlert.txtPropertyName.skin		= "txtNormalSkin";
      	frmAlert.txtPropertyCode.skin	    = "txtNormalSkin";
    } else {
  		frmAlert.txtPropertyName.setEnabled(false);
  		frmAlert.txtPropertyName.skin		= "txtDisabledSkin";  	
  		frmAlert.txtPropertyCode.setEnabled(false);
  		frmAlert.txtPropertyCode.skin		= "txtDisabledSkin";
        frmAlert.txtContractNumber.setEnabled(false);
        frmAlert.txtContractNumber.skin		= "txtDisabledSkin";
      
      	setMasterBurgerMenuVisibity();
      	printMessage("Inside error_insertLead"+gblIsLaunchedAsStandAloneApp);  
      	if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyName)){
            	printMessage("Inside error_insertLead"+gblSharedInfo.propertyName);  
          printMessage("Inside error_insertLead"+gblSharedInfo.contractNumber);  
          printMessage("Inside error_insertLead validation gblSharedInfo "+isEmpty(gblSharedInfo));  
          printMessage("Inside error_insertLead validation gblSharedInfo contractNumber "+isEmpty(gblSharedInfo.contractNumber));  
           //if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractNumber)){
          if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractNumber)){
              printMessage("If Inside error_insertLead condition::: ");
              frmAlert.flxBurgerMenu.flxSettingBtn3Container.setVisibility(true);
              frmAlert.flxBurgerMenu.lblMenuName3.text =getI18nString("common.label.converttolead"); 
           }else{
             frmAlert.flxBurgerMenu.flxSettingBtn3Container.setVisibility(false);
           }
      }else{
       // alert(123);
        frmAlert.flxBurgerMenu.flxSettingBtn3Container.setVisibility(false);
      }
  
      //setHumburgerDeviceDatetime(frmAlert.lblAppVersion,frmAlert.lblLastTransferred);
    }
  
  	frmAlert.lblContractNumber.text		= getI18nString("common.label.contractNumber");
  //	frmAlert.PHtxtContractNumber.text	= getI18nString("common.placeholder.contractNumber");
 /* 	frmAlert.lblAccountCode.text		= getI18nString("common.label.accountCode");
  	frmAlert.PHtxtAccountCode.text		= getI18nString("common.placeholder.accountCode");*/
  	prePopulateDataInAlertForm();
  	gblStartupFormIdentifier			= LAConstants.FORM_ALERTS;
  
  	if (gblIsSyncInitialized === false) {
      	initSyncModule();
    } else {
      	dismissLoading();
    }
  	//frmLeadManagement.destroy();
  
  //LAA-88 - Hide SupportView 
  frmAlert.flxBurgerMenu.flxSettingsBtn2Container.setVisibility(false);
}

/** Create alert - Pre populate data in form **/
function prePopulateDataInAlertForm(){
  	frmAlert.txtPropertyName.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyName)) ? gblSharedInfo.propertyName : "";
  	frmAlert.txtPropertyCode.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyCode)) ? gblSharedInfo.propertyCode : "";
  	frmAlert.txtContactName.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactName)) ? gblSharedInfo.contactName : "";
  	frmAlert.txtContactPosition.text	= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactPosition)) ? gblSharedInfo.contactPosition : "";
  	frmAlert.txtContactPhone.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactPhone)) ? gblSharedInfo.contactPhone : "";
  	frmAlert.txtContactEmail.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactEmail)) ? gblSharedInfo.contactEmail : "";
  	frmAlert.txtAccountCode.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.accountCode)) ? gblSharedInfo.accountCode : "";
  	frmAlert.txtContractNumber.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractNumber)) ? gblSharedInfo.contractNumber : "";
  	frmAlert.txtCustomerReference.text	= "";
  	frmAlert.txtSummary.text			= "";
  	frmAlert.txtNotes.text				= "";
  	resetAlertFormPlaceHolders();
  
    //modified for showing the splitted value for contract number
     if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyCode)){
  		var cnumandpremnum = gblSharedInfo.propertyCode;
        var cnum = cnumandpremnum.substr(0, 8); // Gets the first part 
       	var premnum = cnumandpremnum.substr(8);  // Gets the next part
        frmAlert.txtContractNumber.text =cnum +"/"+premnum;
      }else{
        frmAlert.txtContractNumber.text ="";
      }
  
      //cheking leads is empty and navigate to lead management screen
  
     if (isEmpty(frmAlert.txtPropertyName.text)) {
       
       	printMessage("CreateAlert -prePopulateDataInAlertForm->NAvigating to lead managment screen -->>");  
      	showLeadsManagementFromLocal();
    } 
  
}

/** Create alert - To reset placeholder labels visibility **/
function resetAlertFormPlaceHolders() {
  	if (isEmpty(frmAlert.txtPropertyName.text)) {
      	frmAlert.PHtxtPropertyName.setVisibility(true);
    } else {
      	frmAlert.PHtxtPropertyName.setVisibility(false);
    }
  
  	if (isEmpty(frmAlert.txtPropertyCode.text)) {
      	frmAlert.PHtxtPropertyCode.setVisibility(false);
    } else {
      	frmAlert.PHtxtPropertyCode.setVisibility(false);
    }
  	
  	if (isEmpty(frmAlert.txtContactName.text)) {
      	frmAlert.PHtxtContactName.setVisibility(true);
    } else {
      	frmAlert.PHtxtContactName.setVisibility(false);
    }
  if (isEmpty(frmAlert.txtAccountCode.text)) {
      	frmAlert.PHtxtAccountCode.setVisibility(false);
    } else {
      	frmAlert.PHtxtAccountCode.setVisibility(false);
    }
  if (isEmpty(frmAlert.txtContractNumber.text)) {
      	frmAlert.PHtxtContractNumber.setVisibility(true);
    } else {
      	frmAlert.PHtxtContractNumber.setVisibility(false);
    }
  
  	if (isEmpty(frmAlert.txtContactPosition.text)) {
      	frmAlert.PHtxtContactPosition.setVisibility(true);
    } else {
      	frmAlert.PHtxtContactPosition.setVisibility(false);
    }
  
  	if (isEmpty(frmAlert.txtContactPhone.text)) {
      	frmAlert.PHtxtContactPhone.setVisibility(true);
    } else {
      	frmAlert.PHtxtContactPhone.setVisibility(false);
    }
  
  	if (isEmpty(frmAlert.txtContactEmail.text)) {
      	frmAlert.PHtxtContactEmail.setVisibility(true);
    } else {
      	frmAlert.PHtxtContactEmail.setVisibility(false);
    }
  	
  	frmAlert.PHtxtCustomerReference.setVisibility(true);
  	frmAlert.PHtxtSummary.setVisibility(true);
  	frmAlert.PHtxtNotes.setVisibility(true);
}

/** Create alert - button click **/
/** Create alert - button click **/
function onClickCreateAlert(){
  	var isValidFormData					= true;
  /**  if (isEmpty(frmAlert.txtPropertyName.text)) {
      	isValidFormData					= false;
        showMessagePopup("common.message.enterPropertyName");
    } else if (isEmpty(frmAlert.txtPropertyCode.text)) {
        isValidFormData					= false;
        showMessagePopup("common.message.enterPropertycode");
        }  else**/  if (isEmpty(frmAlert.txtContactName.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.enterContactName");
        gblFocusTextBox 				= frmAlert.txtContactName;
    }  else if (isEmpty(frmAlert.txtContactPosition.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.enterContactPosition");
      	gblFocusTextBox 				= frmAlert.txtContactPosition;
    } else if (isEmpty(frmAlert.txtContactPhone.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.enterContactPhone");
      	gblFocusTextBox 				= frmAlert.txtContactPhone;
    }else if (frmAlert.txtContactPhone.text.length < 3) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.noSpecialCharectersAllowed");
      	gblFocusTextBox 				= frmAlert.txtContactPhone;
    }else if (!isEmpty(frmAlert.txtContactEmail.text)) {
       //isValidFormData					= true;
      if (isValidEmail(frmAlert.txtContactEmail.text) === false) { // updated isValidEmail
        isValidFormData					= false;
      	//frmAlert.txtContactEmail.skin 	= "txtEmailRedSkin";
        showMessagePopupValidation("common.label.novalidEmailEntered");
      	gblFocusTextBox 				= frmAlert.txtContactEmail;
        frmAlert.txtContactEmail.padding = [1.4,0,0,0];	 // Update Skin
        frmAlert.txtContactEmail.skin 	="txtEmailRedSkin";
      }
      	
       else if (isEmpty(frmAlert.txtSummary.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.entertxtSummary");
      	gblFocusTextBox 				= frmAlert.txtSummary;
      }  else if (isEmpty(frmAlert.txtNotes.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.entertxtNotes");
      	gblFocusTextBox 				= frmAlert.txtNotes;
      }
  
    }else if (isEmpty(frmAlert.txtSummary.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.entertxtSummary");
      	gblFocusTextBox 				= frmAlert.txtSummary;
    }  else if (isEmpty(frmAlert.txtNotes.text)) {
        isValidFormData					= false;
        showMessagePopupValidation("common.message.entertxtNotes");
      	gblFocusTextBox 				= frmAlert.txtNotes;
    }
  	if (isValidFormData === true) {
      	showLoading("common.message.1001");
      	gblFocusTextBox 	= null;
      	insertAlertInLocalTbl();
    }
  
}
function success_insertAlert() {
  	printMessage("Inside success_insertLead");
  	//frmLeadManagement.show();
  	gblSharedInfo						= null;
  	//getAlertsESBErrorsForAutoResend(successGetAlertsESBErrorsForAutoUpload, errorGetAlertsESBErrorsForAutoUpload);  
  	prePopulateDataInAlertForm();
  	resetAlertFormPlaceHolders();
//	showMessagePopupWithCallback("common.message.alertSaved");
  	dismissLoading();
   	Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
} 

function error_insertAlert() {
  	printMessage("Inside error_insertLead");
  	dismissLoading();
}

/** Cancel alert - button click **/
function onClickCancelAlert(){
  //LAA33 
  printMessage("Inside onClickCancelAlert");
  frmAlert.txtPropertyName.text    ="";
  gblSharedInfo						= null;
  prePopulateDataInAlertForm();
  resetAlertFormPlaceHolders();
   printMessage("Inside onClickCancelAlert gblSharedInfo"+JSON.stringify(gblSharedInfo));
  Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
  //showLeadsManagementFromLocal();
  //doExitApplication();
  //Navigating to LeadsmangementPage
  //onClickCancelLead();
  //Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
}

/** To construct alerts array from local (locally created alerts) table inorder to sync with backend **/
function constructAlertsInputForSyncUpload(){
  	getAlertsFromLocalTbl(success_getAlertsForUpload, error_getAlertsForUpload);  
}

/** Success getAlert - for uploading created alerts **/

function success_getAlertsForUpload(result){
  
  
  	printMessage("Inside  - success_getAlertsForUpload:" + JSON.stringify(result));
  	if (result.length > 0) {
      	gblAlertsCollectionForScheduler		= [];
      	var aRecord							= null;
      	for (var i = 0; i < result.length; i++) {
          	aRecord							= result[i];
          	aRecord.alertDeleted			= (aRecord.alertDeleted == "false") ? false : true;
          	gblAlertsCollectionForScheduler.push(aRecord);
        }
    } else {
      	gblAlertsCollectionForScheduler	= [];
    }
  	////startSyncLeadsAndAlertsUpload();
  	uploadLeadRecords();
}	

/** Error getAlert - for uploading created alerts **/
function error_getAlertsForUpload(result){
  	printMessage("Inside  - error_getAlertsForUpload: " + JSON.stringify(result));
  	gblAlertsCollectionForScheduler		= [];
  	////startSyncLeadsAndAlertsUpload();
  	uploadLeadRecords();
}

