/*** Author  : RI - CTS - 363601 ****/


/** Show create lead form **/
function showCreateLead() {
  	printMessage("Inside - showCreateLead");
  	gblIsFilterApplied				= false;
  	//gblAppliedFilters				= [];
    gblAppliedFilters               = ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
  	frmLead.show();
  	gblPrevFormId					= "frmLeadManagement";
}

/** Postshow - Form - frmLead **/
function onPostShowLead(){
  
  
    var previousForm = kony.application.getPreviousForm();
  	//setHumburgerDeviceDatetime(frmLead.lblAppVersion,frmLead.lblLastTransferred);
  	setMasterBurgerMenuVisibity();
	//Alert the Previous form
/*	alert("previousForm is::"+previousForm);
  
    if(gblIsLaunchedAsStandAloneApp){
      
      alert('standalone app');
    }
  if(previousForm === null && gblflagtoCheckAppStatus ){
    gblIsLaunchedAsStandAloneApp =false;
    gblflagtoCheckAppStatus = false;
  }
  
  if(gblflagtoCheckAppStatus){
    
    alert('standalone app');
    onPreShowLoginScreen();
  }*/
  
    frmLead.flxBurgerMenu.lblUserName.text =gblBasicInfo.email ;
    frmLead.flxBurgerMenu.flxSettingBtn3Container.setVisibility(false);
  	printMessage("Inside onPostShowLead");
  	showLoading("common.message.1001");
  	refreshSettingsUI();
  	frmLead.flxFormHeader.lblHeaderTitle.text			= getI18nString("common.label.createLead");
  	frmLead.lblPropertyName.text		= getI18nString("common.label.propertyName");
  	//frmLead.PHtxtPropertyName.text		= getI18nString("common.placeholder.propertyName");
  	frmLead.lblContractNumber.text		= getI18nString("common.label.contractNumber");
	//frmLead.lblPropertyCode.text		= getI18nString("common.label.propertyCode");
  	//frmLead.PHtxtContractNumber.text	= getI18nString("common.placeholder.contractNumber");
	//frmLead.PHtxtPropertyCode.text		= getI18nString("common.placeholder.propertyCode");
  	frmLead.PHtxtAddress1.text			= getI18nString("common.placeholder.addressLine1");
  	frmLead.PHtxtAddress2.text			= getI18nString("common.placeholder.addressLine2");
  	//frmLead.PHtxtAddress3.text			= getI18nString("common.placeholder.addressLine3");
  	frmLead.PHtxtAddress4.text			= getI18nString("common.placeholder.addressLine4");
  	frmLead.PHtxtAddress5.text			= getI18nString("common.placeholder.addressLine5");
  	frmLead.PHtxtPostcode.text			= getI18nString("common.placeholder.postcode"); 
  	frmLead.PHtxtContactName.text		= getI18nString("common.placeholder.contactName");  
  	frmLead.PHtxtContactPosition.text  	= getI18nString("common.placeholder.contactPosition");
  	frmLead.PHtxtContactPhone.text		= getI18nString("common.placeholder.contactPhone");	
  	frmLead.PHtxtContactEmail.text		= getI18nString("common.placeholder.contactEmail");
  	frmLead.PHtxtNotes.text				= getI18nString("common.placeholder.notes");
  	frmLead.btnCancel.text				= getI18nString("common.label.cancel");
  	frmLead.btnConfirm.text				= getI18nString("common.label.confirm");
  /* 	frmLead.lblAccountCode.text			= getI18nString("common.label.accountCode");
   	frmLead.PHtxtAccountCode.text		= getI18nString("common.placeholder.accountCode");*/
  	prePopulateDataInLeadForm();
  	
  	gblStartupFormIdentifier			= "frmLead";
  	if (gblIsSyncInitialized === false) {
      	initSyncModule();
    } else {
      	dismissLoading();
    }
  	//frmLeadManagement.destroy(); // Added this line for bug fix
  
      if (!gblIsLaunchedAsStandAloneApp){
        printMessage("checking stand alone app--->>>"+gblIsLaunchedAsStandAloneApp);  
      	if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyName)){
            	printMessage("Inside error_insertLead"+gblSharedInfo.propertyName);  
          printMessage("Inside error_insertLead"+gblSharedInfo.contractNumber);  
           if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractNumber)){
              frmLead.flxBurgerMenu.flexSettingBtn4Container.setVisibility(true);
             frmLead.flxBurgerMenu.lblMenuName3.text =  getI18nString("common.label.convertoalert"); 
                //disabling contract number
            frmLead.txtPropertyName.setEnabled(false);
  		    frmLead.txtPropertyName.skin		= "txtDisabledSkin";  	
  		    frmLead.txtContractNumber.setEnabled(false);
            frmLead.txtContractNumber.skin		= "txtDisabledSkin";
             
           }else{
             frmLead.flxBurgerMenu.flexSettingBtn4Container.setVisibility(false);
             frmLead.txtPropertyName.setEnabled(true);
             frmLead.txtPropertyName.skin		= "txtNormalSkin";
             /*frmLead.txtContractNumber.skin	    = "txtNormalSkin";
             frmLead.txtContractNumber.setEnabled(true);*/
             frmLead.txtContractNumber.setEnabled(false);
             frmLead.txtContractNumber.skin		= "txtDisabledSkin";
           }
      }else{
        frmLead.flxBurgerMenu.flexSettingBtn4Container.setVisibility(false);
        frmLead.txtPropertyName.setEnabled(true);
      	frmLead.txtPropertyName.skin		= "txtNormalSkin";
      	
        /*frmLead.txtContractNumber.skin	    = "txtNormalSkin";
        frmLead.txtContractNumber.setEnabled(true);*/
        
        frmLead.txtContractNumber.setEnabled(false);
        frmLead.txtContractNumber.skin		= "txtDisabledSkin";
      }
  	}else{
        frmLead.flxBurgerMenu.flexSettingBtn4Container.setVisibility(false);
        frmLead.txtPropertyName.setEnabled(true);
      	frmLead.txtPropertyName.skin		= "txtNormalSkin";
      	
      	/*frmLead.txtContractNumber.skin	    = "txtNormalSkin";
     	frmLead.txtContractNumber.setEnabled(true);*/

      	frmLead.txtContractNumber.setEnabled(false);
      	frmLead.txtContractNumber.skin		= "txtDisabledSkin";
      
      
      }
  		//LAA-88 - Hide SupportView 
  		frmLead.flxBurgerMenu.flxSettingsBtn2Container.setVisibility(false);
}

/** Create lead - Pre populate data in form **/
function prePopulateDataInLeadForm(){
  	printMessage("Inside prePopulateDataInLeadForm");
  	frmLead.txtPropertyName.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyName)) ? gblSharedInfo.propertyName : "";
  	frmLead.txtContractNumber.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractNumber)) ? gblSharedInfo.contractNumber : "";
  	frmLead.txtAddress1.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.address1)) ? gblSharedInfo.address1 : "";
  	frmLead.txtAddress2.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.address2)) ? gblSharedInfo.address2 : "";
  	//frmLead.txtAddress3.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.address3)) ? gblSharedInfo.address3 : "";
  	frmLead.txtAddress4.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.address4)) ? gblSharedInfo.address4 : "";
  	frmLead.txtAddress5.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.address5)) ? gblSharedInfo.address5 : "";
  	frmLead.txtPostcode.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.postcode)) ? gblSharedInfo.postcode : "";
  	frmLead.txtContactName.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactName)) ? gblSharedInfo.contactName : "";
  	frmLead.txtContactPosition.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactPosition)) ? gblSharedInfo.contactPosition : "";
  	frmLead.txtContactPhone.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactPhone)) ? gblSharedInfo.contactPhone : "";
  	frmLead.txtContactEmail.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contactEmail)) ? gblSharedInfo.contactEmail : "";
	frmLead.txtPropertyCode.text		= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyCode)) ? gblSharedInfo.propertyCode : "";
  	frmLead.txtAccountCode.text			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.accountCode)) ? gblSharedInfo.accountCode : "";

  	frmLead.txtNotes.text				= "";
  	resetLeadFormPlaceHolders();
  //modified for showing the splitted value for contract number
     if(!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.propertyCode)){
  		var cnumandpremnum = gblSharedInfo.propertyCode;
        var cnum = cnumandpremnum.substr(0, 8); // Gets the first part 
       	var premnum = cnumandpremnum.substr(8);  // Gets the next part
        frmLead.txtContractNumber.text =cnum +"/"+premnum;
      }else{
        frmLead.txtContractNumber.text ="";
      }
    handleLocationIconVisibility();
 
}

/** Create lead - To reset placeholder labels visibility **/
function resetLeadFormPlaceHolders() {

      	frmLead.PHtxtPropertyName.setVisibility(false);
      	frmLead.PHtxtContractNumber.setVisibility(false);

  
  	if (isEmpty(frmLead.txtAddress1.text)) {
      	frmLead.PHtxtAddress1.setVisibility(true);
    } else {
      	frmLead.PHtxtAddress1.setVisibility(false);
    }
  
  	if (isEmpty(frmLead.txtAddress2.text)) {
      	frmLead.PHtxtAddress2.setVisibility(true);
    } else {
      	frmLead.PHtxtAddress2.setVisibility(false);
    }
  
  	/*if (isEmpty(frmLead.txtAddress3.text)) {
      	frmLead.PHtxtAddress3.setVisibility(true);
    } else {
      	frmLead.PHtxtAddress3.setVisibility(false);
    }*/
  	if (isEmpty(frmLead.txtPropertyCode.text)) {
      	frmLead.PHtxtPropertyCode.setVisibility(false);
    } else {
      	frmLead.PHtxtPropertyCode.setVisibility(false);
    }
  	if (isEmpty(frmLead.txtAccountCode.text)) {
      	frmLead.PHtxtAccountCode.setVisibility(false);
    } else {
      	frmLead.PHtxtAccountCode.setVisibility(false);
    }
  	if (isEmpty(frmLead.txtAddress4.text)) {
      	frmLead.PHtxtAddress4.setVisibility(true);
    } else {
      	frmLead.PHtxtAddress4.setVisibility(false);
    }
  
  	if (isEmpty(frmLead.txtAddress5.text)) {
      	frmLead.PHtxtAddress5.setVisibility(true);
    }else {
      	frmLead.PHtxtAddress5.setVisibility(false);
       }
  
  	if (isEmpty(frmLead.txtPostcode.text)) {
      	frmLead.PHtxtPostcode.setVisibility(true);
    } else {
      	frmLead.PHtxtPostcode.setVisibility(false);
       }
  
  	
  	if (isEmpty(frmLead.txtContactName.text)) {
      	frmLead.PHtxtContactName.setVisibility(true);
    } else {
      	frmLead.PHtxtContactName.setVisibility(false);
    }
  
  	if (isEmpty(frmLead.txtContactPosition.text)) {
      	frmLead.PHtxtContactPosition.setVisibility(true);
    } else {
      	frmLead.PHtxtContactPosition.setVisibility(false);
    }
  
  	if (isEmpty(frmLead.txtContactPhone.text)) {
      	frmLead.PHtxtContactPhone.setVisibility(true);
    } else {
      	frmLead.PHtxtContactPhone.setVisibility(false);
    }
  
  	if (isEmpty(frmLead.txtContactEmail.text)) {
      	frmLead.PHtxtContactEmail.setVisibility(true);
    } else {
      	frmLead.PHtxtContactEmail.setVisibility(false);
    }
  
  	frmLead.PHtxtNotes.setVisibility(true);
}

/** Create lead - button click **/
function onClickCreateLead(){
  	printMessage("Inside onClickCreateLead");
  	var isValidFormData					= true;
  	if (isEmpty(frmLead.txtPropertyName.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterPropertyName");
      	gblFocusTextBox 				= frmLead.txtPropertyName;
    } else if (isEmpty(frmLead.txtAddress1.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterAddressLine1");
      	gblFocusTextBox 				= frmLead.txtAddress1;
    } else if (isEmpty(frmLead.txtPostcode.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterPostcode");
      	gblFocusTextBox 				= frmLead.txtPostcode;
    } else if (isEmpty(frmLead.txtContactName.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterContactName");
      	gblFocusTextBox 				= frmLead.txtContactName;
    } else if (isEmpty(frmLead.txtContactPosition.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterContactPosition");
      	gblFocusTextBox 				= frmLead.txtContactPosition;
    } else if (isEmpty(frmLead.txtContactPhone.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.enterContactPhone");
      	gblFocusTextBox 				= frmLead.txtContactPhone;
    }else if (frmLead.txtContactPhone.text.length < 3 ) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.noSpecialCharectersAllowed");
      	gblFocusTextBox 				= frmLead.txtContactPhone;
    }else if (isNumericSpaceRegValidation(frmLead.txtContactPhone.text) === false){
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.noSpecialCharectersAllowed");
      	gblFocusTextBox 				= frmLead.txtContactPhone;
      
    } else if ((!isEmpty(frmLead.txtContactEmail.text))) {
      	//isValidFormData					= true;
      	/*showMessagePopupValidation("common.message.enterValidContactEmail");
      	gblFocusTextBox 				= frmLead.txtContactEmail;*/
     if (isValidEmail(frmLead.txtContactEmail.text) === false) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.label.novalidEmailEntered");
      	gblFocusTextBox 				= frmLead.txtContactEmail;
      	frmAlert.txtContactEmail.padding= [1.4,0,0,0];   // Update Skin
      	frmLead.txtContactEmail.skin 	="txtEmailRedSkin";
    }
    else if (isEmpty(frmLead.txtNotes.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.entertxtNotes");
      	gblFocusTextBox 				= frmLead.txtNotes;
    }
  }else if (isEmpty(frmLead.txtNotes.text)) {
      	isValidFormData					= false;
      	showMessagePopupValidation("common.message.entertxtNotes");
      	gblFocusTextBox 				= frmLead.txtNotes;
    }
  
  printMessage("txtContactPhone.text.length status "+(frmLead.txtContactPhone.text.length >= 2));
  printMessage("txtContactPhone.text.length "+frmLead.txtContactPhone.text.length);
  	if (isValidFormData === true) {
      	showLoading("common.message.1001");
      	gblFocusTextBox = null;
      	insertLeadInLocalTbl();
    }
}
/** Cancel lead - button click **/
function onClickCancelLead(){
  	gblPrevFormId						= null;
  	//showLeadsManagement();
  	gblSharedInfo						= null;
    prePopulateDataInLeadForm();
  	resetLeadFormPlaceHolders();
    showLeadsManagementFromLocal();
}

function success_insertLead() {
  	printMessage("Inside success_insertLead");
  	//getLeadsESBErrorsForAutoResend(successGetLeadESBErrorsForAutoUpload, errorGetLeadsESBErrorsForAutoUpload, false);
  	gblSharedInfo						= null;
  	prePopulateDataInLeadForm();
  	resetLeadFormPlaceHolders();
    //dismissLoading();
  //  frmLeadManagement.show();
  	gblLeadsDataInsertedInDB 			= true;
   //LA11
  //	showMessagePopupWithCallback("common.message.leadSaved");
  //  showLeadsManagement();
     showLeadsManagementFromLocal();
	dismissLoading();
  
   //calling lead save when it inserted
  //commented for 109 issue
   // uploadLeadRecords();
} 

function error_insertLead() {
  	printMessage("Inside error_insertLead");
  	dismissLoading();
}

/** To construct leads array from local (locally created leads) table inorder to sync with backend **/
function constructLeadsInputForSyncUpload(){
  	getLeadsFromLocalTbl(success_getLeadsForUpload, error_getLeadsForUpload, false);
}

/** Success getLeads - for uploading created leads **/
function success_getLeadsForUpload(result){
  	printMessage("Inside  - success_getLeadsForUpload");
  	if (result.length > 0) {
      	gblLeadsCollectionForScheduler		= [];
      	var aRecord							= null;
      	for (var i = 0; i < result.length; i++) {
          	aRecord							= result[i];
          	aRecord.leadDeleted				= (aRecord.leadDeleted == "false") ? false : true;
          	gblLeadsCollectionForScheduler.push(aRecord);
        }
    } else {
      	gblLeadsCollectionForScheduler	= [];
    }
  	constructAlertsInputForSyncUpload();
}

/** Error getLeads - for uploading created leads **/
function error_getLeadsForUpload(result){
  	printMessage("Inside  - error_getLeadsForUpload");
  	gblLeadsCollectionForScheduler		= [];
  	constructAlertsInputForSyncUpload();
}
/**
*/
function handleLocationIconVisibility(){

  if(isEmpty(frmLead.txtAddress1.text) && isEmpty(frmLead.txtAddress2.text)){
    frmLead.imgGetLocation.setVisibility(true);
  }else{
    frmLead.imgGetLocation.setVisibility(false);
  }
}


