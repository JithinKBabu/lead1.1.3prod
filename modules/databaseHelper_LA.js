/*** Author  : RI - CTS - 363601 ****/


/** To fetch lead history from local DB **/
function getLeadHistoryFromLocalTbl(){
  	printMessage(" Inside - getLeadHistoryFromLocalTbl ");	
  	var sqlCondition					= "";
  	var dateRange						= "";
  	printMessage(" Inside - getLeadHistoryFromLocalTbl - gblShowAllLeadsFromHistoryTbl: " + gblShowAllLeadsFromHistoryTbl);
  	var businessCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;	
  	var email 							= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	if (gblShowAllLeadsFromHistoryTbl === true){
       //fix for loading default 28 days
     
      //	dateRange						= getDateRangeForPreviousPage();
         dateRange                      = getDateRangeForNextPage();
      	var toDate						= getDateTimeInFormat(new Date(), LAConstants.LEAD_DATE_FORMAT);
      	gblShowAllLeadsFromHistoryTbl	= false;
      	sqlCondition					= " WHERE createdDate <= '" + toDate + "' AND createdDate > '" + dateRange.from + "' AND countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "' AND authorEmailAddress IN ('" + email + "', '') ";
        printMessage(" Inside - getLeadHistoryFromLocalTbl - sqlCondition: " + sqlCondition);
		leadhistory.find(sqlCondition, success_getLeadHistory, error_getLeadHistory);
    } else {
      	dateRange						= getDateRangeForNextPage();  
        if (!isEmpty(dateRange.from) && !isEmpty(dateRange.to)) {
            sqlCondition				= " WHERE createdDate <= '" + dateRange.to + "' AND createdDate > '" + dateRange.from + "' AND countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "' AND authorEmailAddress  COLLATE NOCASE IN ('" + email + "', '') ";
            printMessage(" Inside - getLeadHistoryFromLocalTbl - sqlCondition: " + sqlCondition);
            gblCurrentPageIndex++;
            leadhistory.find(sqlCondition, success_getLeadHistory, error_getLeadHistory);
        } else {
            showMessagePopupValidation("common.message.noMoreHistoryAvailable");
            dismissLoading();
        }
    }
}

/** To fetch alert records from local DB **/
function getAlertsFromLocalTbl(successCallbackMethod, errorCallbackMethod){
  	printMessage(" Inside - getAlertsFromLocalTbl ");
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;		
  	var sqlStatement		= "SELECT alertId, authorEmailAddress, accountCode, contractCode, propertyCode, propertyContactName, propertyContactPosition, propertyContactTelephone, propertyContactMobile, propertyContactEmail, propertyContactFax, ticketSummary, ticketNotes, createdDate, createdTime, updateDateTime, alertDeleted FROM alerts WHERE countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "'";
  	printMessage(" Inside - getAlertsFromLocalTbl - sqlStatement: " + sqlStatement);	
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

/** To fetch lead records from local DB **/
function getLeadsFromLocalTbl(successCallbackMethod, errorCallbackMethod, isFilterRequired){
  	printMessage(" Inside - getLeadsFromLocalTbl ");
  	var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	var sqlStatement		= "SELECT leadId, authorEmailAddress, accountCode, contractCode, propertyCode, propertyName, propertyAddressLine1, propertyAddressLine2, propertyAddressLine3, propertyAddressLine4, propertyAddressLine5, propertyPostcode, propertyContactName, propertyContactPosition, propertyContactTelephone, propertyContactMobile, propertyContactEmail, propertyContactFax, ticketNotes, createdDate, createdTime, leadDeleted, updateDateTime  FROM leads WHERE countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "'";
  	
  	if (isFilterRequired === true) {
      	sqlStatement		+= " AND authorEmailAddress COLLATE NOCASE IN ('" + email + "', '') ";
    }
  	printMessage(" Inside - getLeadsFromLocalTbl - isFilterRequired: " + isFilterRequired);	
  	printMessage(" Inside - getLeadsFromLocalTbl - sqlStatement: " + sqlStatement);		  
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

/** To fetch lead records from error local DB **/
function getLeadsErrorFromLocalTbl(successCallbackMethod, errorCallbackMethod, isFilterRequired){
  	printMessage(" Inside - getLeadsFromErrorLocalTbl ");
  	var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	var sqlStatement		= "SELECT leadId, authorEmailAddress, accountCode, contractCode, propertyCode, propertyName, propertyAddressLine1, propertyAddressLine2, propertyAddressLine3, propertyAddressLine4, propertyAddressLine5, propertyPostcode, propertyContactName, propertyContactPosition, propertyContactTelephone, propertyContactMobile, propertyContactEmail, propertyContactFax, ticketNotes, createdDate, createdTime, leadDeleted, updateDateTime  FROM leaderrors WHERE countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "'";
  	
  	if (isFilterRequired === true) {
      	sqlStatement		+= " AND authorEmailAddress COLLATE NOCASE IN ('" + email + "', '') ";
    }
  	printMessage(" Inside - getLeadsFromErrorLocalTbl - isFilterRequired: " + isFilterRequired);	
  	printMessage(" Inside - getLeadsFromErrorLocalTbl - sqlStatement: " + sqlStatement);		  
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}



/** To insert lead record in local DB **/
function insertLeadInLocalTbl(){
  	printMessage("--------------- Inside - insertLeadInLocalTbl ");	
  	var leadObj 						= new leads();
  	leadObj.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
	leadObj.businessCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
    /*leadObj.countryCode 				= "NO";
	leadObj.businessCode 				=  "K";*/
	
    leadObj.leadId 						= generateUUID();
	leadObj.authorEmailAddress 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
	//leadObj.accountCode 				= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.accountCode)) ? gblSharedInfo.accountCode : "";
	//leadObj.contractCode 				= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractCode)) ? gblSharedInfo.contractCode : "";
  	leadObj.accountCode 				= frmLead.txtAccountCode.text;
    
   
    // alert('frmLead.txtContractNumber.text'+frmLead.txtContractNumber.text);
     
     var cCode=frmLead.txtContractNumber.text;
     var cnumFlag = cCode.indexOf("/");
   if(cnumFlag!=-1){
     leadObj.contractCode              =  cCode.substr(0, 8);
    // alert('leadObj.contractCode '+leadObj.contractCode );
   }else{
     leadObj.contractCode 				= frmLead.txtContractNumber.text;
   }
	
	leadObj.propertyCode 				= frmLead.txtPropertyCode.text;
	leadObj.propertyName 				= frmLead.txtPropertyName.text;
	leadObj.propertyAddressLine1 		= frmLead.txtAddress1.text;
	leadObj.propertyAddressLine2 		= frmLead.txtAddress2.text;
  	leadObj.propertyAddressLine3 		= "";
  	//leadObj.propertyAddressLine3 		= frmLead.txtAddress3.text;
	//leadObj.propertyAddressLine4 		= frmLead.txtAddress3.text.concat(", "+frmLead.txtAddress4.text).trim();
  	leadObj.propertyAddressLine4 		= frmLead.txtAddress4.text.trim();
	leadObj.propertyAddressLine5 		= frmLead.txtAddress5.text;
	leadObj.propertyPostcode 			= frmLead.txtPostcode.text;
	leadObj.propertyContactName 		= frmLead.txtContactName.text;
	leadObj.propertyContactPosition 	= frmLead.txtContactPosition.text;
	leadObj.propertyContactTelephone 	= frmLead.txtContactPhone.text;
	leadObj.propertyContactMobile 		= "";
	leadObj.propertyContactEmail 		= frmLead.txtContactEmail.text;
	leadObj.propertyContactFax 			= "";
	leadObj.ticketNotes 				= frmLead.txtNotes.text;
	leadObj.createdDate 				= getTodaysDateStr();
	leadObj.createdTime 				= getCurrentTimeStr();
	leadObj.leadDeleted 				= false;
	leadObj.updateDateTime 				= getDateTimeFormat(new Date(), "YYYY-MM-DDTHH:mm:ss.SSS"); //getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");  
	leadObj.markForUpload 				= true;
  	printMessage(" Inside - leadObj " + JSON.stringify(leadObj));
  	leadObj.create(success_insertLead, error_insertLead);
}

/** To insert alert record in local DB **/
function insertAlertInLocalTbl(){  
  	printMessage(" Inside - insertAlertInLocalTbl ");
  	var alertObj 						= new alerts();
    
  	alertObj.businessCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
	alertObj.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
	/*alertObj.businessCode 				= "K";
  	alertObj.countryCode 				= "NO";*/
  	alertObj.alertId 					= generateUUID();
	alertObj.authorEmailAddress 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
	//alertObj.accountCode 				= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.accountCode)) ? gblSharedInfo.accountCode : "";
	//alertObj.contractCode 			= (!isEmpty(gblSharedInfo) && !isEmpty(gblSharedInfo.contractCode)) ? gblSharedInfo.contractCode : "";
  	alertObj.accountCode 				= frmAlert.txtAccountCode.text;
	//alertObj.contractCode 				= frmAlert.txtContractNumber.text;
     var cCode=frmAlert.txtContractNumber.text;
		var cnumFlag = cCode.indexOf("/");
 	if(cnumFlag!=-1){
      alertObj.contractCode              =  cCode.substr(0, 8);
 	  //alert('alertObj.contractCode '+alertObj.contractCode );
 	}else{
   	alertObj.contractCode = frmAlert.txtContractNumber.text;
   }
  
  	alertObj.propertyCode 				= frmAlert.txtPropertyCode.text;
	alertObj.propertyContactName 		= frmAlert.txtContactName.text;
	alertObj.propertyContactPosition 	= frmAlert.txtContactPosition.text;
	alertObj.propertyContactTelephone	= frmAlert.txtContactPhone.text;
	alertObj.propertyContactMobile 		= "";
	alertObj.propertyContactEmail 		= frmAlert.txtContactEmail.text;
	alertObj.propertyContactFax 		= "";
	alertObj.ticketSummary 				= frmAlert.txtSummary.text;
	alertObj.ticketNotes 				= frmAlert.txtNotes.text;
	alertObj.createdDate 				= getTodaysDateStr();
	alertObj.createdTime 				= getCurrentTimeStr();
	alertObj.updateDateTime 			= getDateTimeFormat(new Date(), "YYYY-MM-DDTHH:mm:ss.SSS"); //getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
	alertObj.alertDeleted 				= false;
	alertObj.markForUpload 				= true;
  	printMessage(" Inside - alertObj " + JSON.stringify(alertObj));
  	alertObj.create(success_insertAlert, error_insertAlert);
}

/** To delete locally created records (Leads) from table **/
function deleteLeadsFromLocalTbl() {
  
  printMessage("inside deleteLeadsFromLocalTbl::: since the upload success ");
  	if (!isEmpty(gblLeadsCollectionForScheduler) && gblLeadsCollectionForScheduler.length > 0) {
      	var aRecord					= null;
      	var sqlStatement			= "DELETE FROM leads WHERE leadId IN ("; 
      	for (var i = 0; i < gblLeadsCollectionForScheduler.length; i++) {
          	aRecord					= gblLeadsCollectionForScheduler[i];
          	sqlStatement			+= "'" + aRecord.leadId + "'";
          	if (i < (gblLeadsCollectionForScheduler.length - 1)) {
              	sqlStatement		+= ",";
            }
        }
      	sqlStatement				+= ")";
      	printMessage("Inside deleteLeadsFromLocalTbl - sqlStatement: " + sqlStatement);
      	executeCustomQuery(sqlStatement, null, success_commonCallback, error_commonCallback);
    }
}

/** To delete locally created records (Alerts) from table **/
function deleteAlertsFromLocalTbl() { 
  	if (!isEmpty(gblAlertsCollectionForScheduler) && gblAlertsCollectionForScheduler.length > 0) {
      	var aRecord1				= null;
      	var sqlStatement1			= "DELETE FROM alerts WHERE alertId IN ("; 
      	for (var j = 0; j < gblAlertsCollectionForScheduler.length; j++) {
          	aRecord1				= gblAlertsCollectionForScheduler[j];
          	sqlStatement1			+= "'" + aRecord1.alertId + "'";
          	if (j < (gblAlertsCollectionForScheduler.length - 1)) {
              	sqlStatement1		+= ",";
            }
        }
      	sqlStatement1				+= ")";
      	printMessage("Inside deleteAlertsFromLocalTbl - sqlStatement: " + sqlStatement1);
      	executeCustomQuery(sqlStatement1, null, success_commonCallback, error_commonCallback);
    }
}

/********************************************* Custom DB operations *************************************************/

/** To open DB handler **/
function openLocalDatabase(){
  	var dbname 			= kony.sync.getDBName();
  	var dbObjectId 		= kony.sync.getConnectionOnly(dbname, dbname);
  	return dbObjectId;
}

/** To execute custom sql queries from local db **/
function executeCustomQuery(sqlStatement, params, successCallback, errorCallback){
	var dbname 			= kony.sync.getDBName();
	printMessage("Inside - executeCustomQuery " + sqlStatement);
  printMessage("Inside - executeCustomQuery dbname" + dbname);
  printMessage("Inside - executeCustomQuery params" + JSON.stringify(params));
	kony.sync.single_select_execute(dbname, sqlStatement, params, successCallback, errorCallback);
}


/** To fetch iCabInstances from local DB **/
function getiCabInstancesFromLocalTbl(){
  	printMessage(" Inside - getiCabInstancesFromLocalTbl ");	
   	//printMessage(" Inside - getiCabInstancesFromLocalTbl gblConfiguredEmails"+JSON.stringify(gblConfiguredEmails));	
  printMessage(" Inside - getiCabInstancesFromLocalTbl  gblBasicInfo.email "+ gblBasicInfo.email);	
  //LAA41
  var emailaddress		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  
  printMessage(" Inside - getiCabInstancesFromLocalTbl "+emailaddress);	
    sqlCondition = " where email = '"+emailaddress+"' COLLATE NOCASE " ;
  //	com.pc.icabinstances.iCabinstancesPc.getAll(success_getiCabInstances, error_getiCabInstances, orderByMap);
  //LAiCabInstances.find(sqlCondition ,success_getiCabInstances, error_getiCabInstances);
  
  // Updated query 
  var sqlStatement		= "select * from LAiCabInstances";
  printMessage(" Inside - getAlertsFromLocalTbl - sqlStatement: " + sqlStatement);	
  executeCustomQuery(sqlStatement, null, success_getiCabInstances, error_getiCabInstances);  
}

/** To fetch employee record from local DB **/
function getEmployeeFromLocalTbl(){
  	printMessage(" Inside - getEmployeeFromLocalTbl ");
  	var orderByMap 			= [];
	orderByMap[0] 			= {};
	orderByMap[0].key 		= "employeeWorkEmail";
	orderByMap[0].sortType 	= "asc";
  	//	com.pc.employees.Employees.getAll(success_getEmployee, error_getEmployee, orderByMap, 1, 0);
  	EmployeesLA.getAll(success_getEmployee, error_getEmployee, orderByMap, 1, 0);
}


/**Get all errors data from alerterrors table */
function getAlertErrorsTableDataFromLocalDB(){
	sqlalertErrors 	= "select * from alertErrors";
	executeCustomQuery(sqlalertErrors, null, getAlertErrosSuccesCallback, getAlertErrosErrorCallback);  
}

function getAlertErrosSuccesCallback(result){
	kony.print("getAllErrorsErrorCallback1 : result" + JSON.stringify(result));
	
	
	if(result !== null && result.length > 0) {
	 //showMessagePopupValidation("common.message.2004");
      showAlertMessagePopupValidation("common.message.2004");
	}
}
function getAlertErrosErrorCallback(){
	kony.print("getAllErrorsErrorCallback1 : res" + JSON.stringify(res));
}

/********************************************* Transaction Callbacks *************************************************/

/** Transaction common success callback - For sql statement execution **/
function success_transCommonCallback(transactionId, result) {
	printMessage("Inside - success_transCommonCallback");
}

/** Transaction common error callback - For sql statement execution **/
function error_transCommonCallback(error) {
	printMessage("Inside - error_transCommonCallback");
}

/** Common success callback - For sql statement execution **/
function success_commonCallback() {
	printMessage("Inside - success_commonCallback");
}

/** Common error callback - For sql statement execution **/
function error_commonCallback(error) {
	printMessage("Inside - error_commonCallback");
}

//==================================================

function getLeadsErrorFromLocalDB(){
  printMessage(" Inside - getLeadsFromErrorLocalTbl ");
  var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  var businessCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  var countryCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  var curentTimeDate		=  getPurgeCurrentDateTimeFormat();
  var validateTime 			= (60 * 60 * 24); // one day
  var sqlStatement 			= "SELECT 'Yes' AS 'isLead', leaderrors.leadId as resultId, CAST (( (strftime('%s','"+curentTimeDate+"') - strftime('%s',(leaderrors.createdDate || ' ' || leaderrors.createdTime) ) )/ '"+validateTime+"'"+") AS TEXT) as leadTimeDif  from leaderrors where leaderrors.leadDeleted = '"+1+"' AND leadTimeDif >= '"+LAConstants.PURGE_DB_TIME_SEVENDAYS+"'";

  printMessage(" Inside - getLeadsFromErrorLocalTbl - sqlStatement: " + sqlStatement);	
  executeCustomQuery(sqlStatement, null, successLeadErrorsForPurgeDB, errorLeadErrorForPurgeDB);  	
}

function successLeadErrorsForPurgeDB(result){
  printMessage(" Inside - successLeadErrorsForPurgeDB " + JSON.stringify(result));
  if (result.length > 0) {
    for(var i = 0; i < result.length; i++){
       printMessage(" Inside - successLeadErrorsForPurgeDB " + JSON.stringify(result));
      if(result[i].isLead === "Yes"){

        deletePurgeErrorRecordFromDB("Lead",result[i].resultId);
      }else{
        deletePurgeErrorRecordFromDB("Alert",result[i].resultId);
      }
    }
  }
}
function errorLeadErrorForPurgeDB(err){
  
  printMessage(" Inside - errorLeadErrorForPurgeDB "+JSON.stringify(err));		
}

function getErrosSuccesCallback(result){
  printMessage(" Inside - getErrosSuccesCallback " + JSON.stringify(result));
  if (result.length > 0) {
    
  	gblPurgeDBRecords.push(result);
  }
}

function getErrorsErrorCallback(err){
  
  printMessage(" Inside - getErrorsErrorCallback "+JSON.stringify(err));		
}


function deletePurgeErrorRecordFromDB(tabelName,errorId) {
 
	var sqlLeadAlertsError ="";
  if (tabelName == "Lead") {
    sqlLeadAlertsError 	= "delete from leaderrors"+" where leadId = '" + errorId+"'";

  }else if(tabelName == "Alert"){
    sqlLeadAlertsError 	= "delete from alertErrors"+" where alertId = '" + errorId+"'";
  }
  executeCustomQuery(sqlLeadAlertsError, null, deletePurgeRecordSuccessCallback, deletePurgeRecordErrorCallback);

}

function deletePurgeRecordSuccessCallback(res) {
    printMessage("deletePurgeRecordSuccessCallback : res" + JSON.stringify(res));
}

function deletePurgeRecordErrorCallback(err) {
    printMessage("deletePurgeRecordErrorCallback : err" + JSON.stringify(err));
}




