/*** Author  : RI - CTS - 363601 ****/


/** Application - Pre app init call **/
function onPreAppInit(){
  	getCurrentDevLocale();
  	setApplicationBehaviors();  
  	initializeGlobals(); 
}

/** Application - Post app init call **/
function onPostAppInit() {
  	printMessage("Inside - onPostAppInit");

  	//Invokes FFI function 'appInstalledOrNot'
	//var returnedValue = Applaunch.appInstalledOrNot("com.orgname.PropertyCare");
  	//printMessage("Inside - onPostAppInit - FFI call return: " + returnedValue);
}

/** Sync initialization - Success callback  **/
function success_initSyncModule(response){
	printMessage("Inside - success_initSyncModule - gblStartupFormIdentifier :" + gblStartupFormIdentifier);
  	printMessage("Inside - success_initSyncModule - gblIsLaunchedAsStandAloneApp :" + gblIsLaunchedAsStandAloneApp);
  	gblIsSyncInitialized				= true;
  	if(gblIsLaunchedAsStandAloneApp === true){
  		var isEmpAlreayLoggedIn			= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
            cancelSyncScheduler();
            cancelPurgeDBScheduler();
            var loggedEmpProfile		= getEmpProfielFromStore();
          	printMessage("Inside - success_initSyncModule loggedEmpProfile:: "+loggedEmpProfile);
          	gblBasicInfo.email			= loggedEmpProfile.email;
            //setGblUserObject(loggedEmpProfile); 
          
    gblEmployeeObject					= new Employee();
  	gblEmployeeObject.email				= loggedEmpProfile.email;
  	gblEmployeeObject.firstName			= loggedEmpProfile.firstName;
  	gblEmployeeObject.lastName			= loggedEmpProfile.lastName;
  	gblEmployeeObject.countryCode		= loggedEmpProfile.countryCode;
  	gblEmployeeObject.businessCode		= loggedEmpProfile.businessCode;
  	gblEmployeeObject.branchNumber		= loggedEmpProfile.branchNumber;
   
    // Update gblBasicInfo  
    gblBasicInfo.email					= loggedEmpProfile.email;
	gblBasicInfo.businessCode			= loggedEmpProfile.businessCode;
	gblBasicInfo.countryCode            = loggedEmpProfile.countryCode;
    
  	if (!isEmpty(loggedEmpProfile.employeeCode)){
      	gblEmployeeObject.setEmployeeCode(loggedEmpProfile.employeeCode);
    }
          
            updateWelcomeLabel();
            printMessage("Inside - success_initSyncModule - loggedEmpProfile : " + JSON.stringify(loggedEmpProfile));
          	getLeadsFromLocalTbl(success_getLeadsForDisplaying, error_getLeadsForDisplaying, true);
        } else {
          	//LAA41
          	getDeviceConfiguredEmails();
        }
	} else {
        if (gblStartupFormIdentifier === LAConstants.FORM_LEADS) {
            dismissLoading();
        } else if (gblStartupFormIdentifier === LAConstants.FORM_ALERTS) {
            dismissLoading();
        } else if(gblStartupFormIdentifier === LAConstants.FORM_LEAD_MANAGEMENT) {
            getLeadsFromLocalTbl(success_getLeadsForDisplaying, error_getLeadsForDisplaying, true);
        }else{
            printMessage("Inside - success_initSyncModule - Not a stand alone app - gblStartupFormIdentifier : " + gblStartupFormIdentifier);
        }
 	}
  //	startPurgeDBScheduler();     
  	startSyncScheduler();
}

/** Sync initialization - Failure callback  **/
function failure_initSyncModule(response){
	printMessage("Inside  - failure_initSyncModule");
  	if (gblStartupFormIdentifier === LAConstants.FORM_LEADS) {
      	// Nothing to do here
    } else if (gblStartupFormIdentifier === LAConstants.FORM_ALERTS) {
      	// Nothing to do here
    } else {
      	// Nothing to do here
    }
  	dismissLoading();
}

/** Initializing global variables **/
function initializeGlobals(){
    gblLangCode			= {};
    gblIsLaunchedAsStandAloneApp	= true;
  	gblIsSyncInProgress				= false;
  	gblShowCustomProgress			= false;
  	gblSyncCallInProgress			= "";
  
  	gblScopeMappings				= { "LALeadHistoryScope"	: "Lead History",
                                   		"LALeadScope"		: "Leads",
                                   		"LAAlertScope"		: "Alert"
                                  	  };
  
  	gblBasicInfo					= null;
  
  	gblSharedInfo					= null;
  	gblCurrentPageIndex				= 0;
  	gblIsProgressPopupOpen			= false;
  	gblProgressIndicatorFlag		= false;
  	gblLeadsNotSynced				= [];
  	gblIsLeadsNotSyncedPresent		= false;
  	gblStartupFormIdentifier		= "frmLeadManagement";
  	gblLeadsCollectionForScheduler	= [];
  	gblAlertsCollectionForScheduler	= [];
  	glbSegData 						= [];
  	glbSegNoData 					= [];
  	gblIsSyncInitialized			= false;
  	gblShowAllLeadsFromHistoryTbl	= false;
  	gblIsFilterApplied				= false;
  	gblAppliedFilters				= ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
  	gblPrevFormId					= null;
  
  	gblUploadTicket					= {};
  	gblVars							= [];
  	gblIsIntServiceInProgress		= false;
    gblEmployeeObject				= null;  
  	gblLeadsDataInsertedInDB		= false;
  	gblLeadsAddressIsEmpty			= true;
  	gblFocusTextBox					= null;
  	gblConfiguredEmails				= "";
  	glbflagTocheckIcabSync 			= true;
    
    gblCheckSelectedIcab            ="";
    gblSourceChange                 =false;
  
    gblLeadError 					= false;
    gblAlertError					= false;
  
    gblSyncError                     =[];
    gblIsLeadsErrorSyncedPresent     =false;
    glbInsertCounter                 =0;
    glbInsertlength                  =0;
  	glbServiceName 			         = "";
    glbIsPullToRefresh			   	 = false;
    glbLanguageOnly                  =false;
  	gblErrorNumber 					 =0;
  	gblServerCodes 				 	 = ["1000", "1011","1012", "1013","1014", "1015","1016"];
  	
  	gblLeadsESBErrorCollectionForScheduler  = [];
	gblAlertsESBErrorCollectionForScheduler = [];
  	gblLeadHistoryErrorData 				= [];
 	gblErrorObj								= [];
	gblServiceName 							= "";
  	gblIsCheckESBError 						= false;
  	gblIsAutoSendESBError 					= false;
  
     gblLeadsObject 							= {};
  	gblAlertObject 							= {};
    setLanguagesCode();
     
}

/** To set application behaviors **/
function setApplicationBehaviors() {
  	printMessage("Inside setApplicationBehaviors");
 	var inputParamTable 						= {}; 
    inputParamTable.defaultIndicatorColor		= "#ffe6e6";
  	inputParamTable.hidedefaultloadingindicator	= true;
    kony.application.setApplicationBehaviors(inputParamTable); 
  	var callbackParamTable 						= {}; 
    callbackParamTable.onbackground				= ActionApplicationWentBackground;
    callbackParamTable.onforeground				= ActionApplicationBecomesForeground;
  	kony.application.setApplicationCallbacks(callbackParamTable);
}

/** Callback - App running in foreground **/
function onAppBecomesForeground(){
  	printMessage("Inside -  onAppBecomesForeground");
  	
}

/** Callback - App running in background **/
function onAppWentBackground(){
  	printMessage("Inside -  onAppWentBackground");
  	//kony.application.exit();  	
}

/** Device back button action **/
function onDeviceBackButtonClick(){
	var frmObj								= kony.application.getCurrentForm();
  	
 /* 	if (frmObj.id == LAConstants.FORM_ALERTS || frmObj.id == LAConstants.FORM_LEAD_MANAGEMENT) {
      	gblPrevFormId						= null;
      	Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
    } else if (frmObj.id == LAConstants.FORM_LEADS && !isEmpty(gblPrevFormId) && gblPrevFormId == LAConstants.FORM_LEAD_MANAGEMENT){
      	gblPrevFormId						= null;
      	showLeadsManagement();
    } else {
      	gblPrevFormId						= null;
      	Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, ""); 
    } */
  
  	if (frmObj.id == LAConstants.FORM_LEAD_MANAGEMENT) {
      
          if(gblIsLaunchedAsStandAloneApp){
				doExitApplication();
			}else{
              gblPrevFormId						= null;
              Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
            }
      	
    } else if (frmObj.id == LAConstants.FORM_LEADS && !isEmpty(gblPrevFormId) && gblPrevFormId == LAConstants.FORM_LEAD_MANAGEMENT){
      	gblPrevFormId						= null;
      	showLeadsManagementFromLocal();
    } else if (frmObj.id == LAConstants.FORM_ALERTS && !isEmpty(gblPrevFormId) && gblPrevFormId == LAConstants.FORM_LEAD_MANAGEMENT ){
        gblPrevFormId						= null;
      	showLeadsManagementFromLocal();
      
    }else {
      	gblPrevFormId						= null;
      	Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, ""); 
    }
  
  
  
  
}

/** To fetch current device locale  **/
function getCurrentDevLocale(){
  	var selLanCode			= kony.store.getItem(LAConstants.SEL_LAN_CODE);
  	if (isEmpty(selLanCode)) {
      	var curDevLocale	= kony.i18n.getCurrentDeviceLocale();
        var language		= curDevLocale.language;
        var country			= curDevLocale.country;
        var devLocale		= (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : LAConstants.DEF_LOCALE;
        if (kony.i18n.isLocaleSupportedByDevice(devLocale) && kony.i18n.isResourceBundlePresent(devLocale)){
            changeDeviceLocale(devLocale);
        } else {
            changeDeviceLocale(LAConstants.DEF_LOCALE);
        }
    } else {
      	changeDeviceLocale(selLanCode);
    }
}

/** To change current device locale  **/
function changeDeviceLocale(selLocale, callBackSuccess, callBackFailure){
  printMessage("changeDeviceLocale "+selLocale); 
  //printMessage("changeDeviceLocale isLocaleSupportedByDevice "+kony.i18n.isLocaleSupportedByDevice(selLocale));
  //printMessage("changeDeviceLocale isResourceBundlePresent "+kony.i18n.isResourceBundlePresent(selLocale));
  printMessage("changeDeviceLocale callBackSuccess "+callBackSuccess); 
  printMessage("changeDeviceLocale callBackFailure "+callBackFailure); 
           
  	if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)){
      	if (callBackSuccess !== undefined && callBackFailure !== undefined) {
          printMessage("changeDeviceLocale if"+selLocale);
          kony.i18n.setCurrentLocaleAsync(selLocale, callBackSuccess, callBackFailure);
          //Update language
          kony.store.setItem(LAConstants.SEL_LAN_CODE, selLocale);
          saveLocaleUpdatedLangCode();
  		  doRefreshi18n();
          
        } else {
          printMessage("changeDeviceLocale else");
          	kony.i18n.setCurrentLocaleAsync(selLocale, success_setLocaleCallback, failure_setLocaleCallback);
        }
    }else{
      
    } 
}

/** Success callback - change device locale  **/
function success_setLocaleCallback(oldlocalename, newlocalename){
	printMessage("Inside -  success_setLocaleCallback"+newlocalename);
  	kony.store.setItem(LAConstants.SEL_LAN_CODE, newlocalename);
    saveLocaleUpdatedLangCode();
  	//doRefreshi18n();
}

/** Failure callback - change device locale  **/
function failure_setLocaleCallback(errCode, errMsg){
	printMessage("Inside -  failure_setLocaleCallback");
  	dismissLoading();
}

/** To handle form when textfield changes **/
/** To handle form when textfield changes **/
function onTextChangeOnInputField(widgetRef){
  	printMessage("Inside -  onTextboxEndEditing "+widgetRef.id);
  	var frmObj										= kony.application.getCurrentForm();
  	if (frmObj.id === LAConstants.FORM_LEADS || frmObj.id === LAConstants.FORM_ALERTS) {
      	if (!isEmpty(frmObj[widgetRef.id])) {
          	var inputText							= frmObj[widgetRef.id].text;
          	var placeHolderLabelName				= "PH" + widgetRef.id;
          	if (isEmpty(inputText)) {
          		frmObj[placeHolderLabelName].setVisibility(true);
            } else {
                frmObj[placeHolderLabelName].setVisibility(false);
            }
        }
      
      //Change Skin 
      if(widgetRef.id === "txtContactEmail"){
        printMessage("Inside -  validation "+frmObj[widgetRef.id].skin);
         printMessage("Inside -  before  "+frmObj.txtContactEmail.padding);
          printMessage("Inside -  before "+frmObj.txtContactEmail.left);
			
          frmObj.txtContactEmail.padding = [1.4,0,0,0];
          frmObj.txtContactEmail.skin ="txtNormalSkin";
          printMessage("Inside -  after "+frmObj.txtContactEmail.padding);
          printMessage("Inside -  after "+frmObj.txtContactEmail.left);
        }
    }
}


/** Progress popup - show **/
function showProgressPopup(titleKey, messageStr, progressKey){
    PopupProgress.lblMessage.text	= getI18nString(messageStr);
  	PopupProgress.lblProgress.text	= getI18nString(progressKey);
  	if (gblIsProgressPopupOpen === false) {
      	gblIsProgressPopupOpen		= true;
      	PopupProgress.lblTitle.text	= getI18nString(titleKey);
      	PopupProgress.show();
      	kony.timer.schedule("ToggleProgressImageTimer", toggleProgressImage, 0.5, true);
    } 
}

/** Progress popup - to toggle progress indicator image **/
function toggleProgressImage(){
  	PopupProgress.imgLoading.src	= (gblProgressIndicatorFlag === true) ? "loadingone.png" : "loadingtwo.png" ; 
  	gblProgressIndicatorFlag		= !gblProgressIndicatorFlag;
}

/** Progress popup - update message **/
function updateMessageInProgressPopup(messageStr){
  	 PopupProgress.lblMessage.text	= getI18nString(messageStr);
}

/** Progress popup - update progress **/
function updateProgressInProgressPopup(progressKey){
  	 PopupProgress.lblProgress.text	= getI18nString(progressKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	gblIsProgressPopupOpen			= false;
  	kony.timer.cancel("ToggleProgressImageTimer"); 
  	PopupProgress.dismiss();
}

/** Popup for displaying warning amd error and messages **/
function showMessagePopupValidation(msgKey){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= getI18nString(msgKey);
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
    PopupMessage.show();  
}

function showAlertMessagePopupValidation(msgKey){
  	PopupAlertMsg.lblTitle.text		= getI18nString("common.app.title");
  	PopupAlertMsg.lblMessage.text	= getI18nString(msgKey);
    PopupAlertMsg.btnOk.text		= getI18nString("common.label.ok");
    PopupAlertMsg.show();  
}


/** Popup for displaying messages with callback handler **/
function showMessagePopupWithCallback(msgKey){
  	PopupMessageWithCallback.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessageWithCallback.lblMessage.text	= getI18nString(msgKey);
    PopupMessageWithCallback.btnOk.text			= getI18nString("common.label.ok");
    PopupMessageWithCallback.show();  
}

/** MessageWithCallback Popup - Dismiss **/
function dismissMessaWithCallbackgePopup(){
  	PopupMessageWithCallback.dismiss(); 
  	var frmObj									= kony.application.getCurrentForm();
  	if (frmObj.id === LAConstants.FORM_LEADS) {
		showLeadsManagement();
    } else if (frmObj.id === LAConstants.FORM_ALERTS) {
       if(gblIsLaunchedAsStandAloneApp){
         showLeadsManagement();
       }else{
      	Applaunch.LaunchApp(LAConstants.PARENT_APP_PACKAGE_NAME, "");
         }
    }
}

/** Void method call **/
function voidCall(){
  	printMessage("Inside -  voidCall");
}

/** To set global employee object **/
function setGblUserObject(employeeInfo){
  
   	printMessage("setGblUserObject--->"+JSON.stringify(employeeInfo));
  	gblEmployeeObject					= new Employee();
  	gblEmployeeObject.email				= (isEmpty(employeeInfo.employeeWorkEmail) || employeeInfo.employeeWorkEmail == "null") ? gblBasicInfo.email : employeeInfo.employeeWorkEmail;
  	gblEmployeeObject.firstName			= (!isEmpty(employeeInfo.employeeName1)) ? employeeInfo.employeeName1 : gblEmployeeObject.firstName;
  	gblEmployeeObject.lastName			= (!isEmpty(employeeInfo.employeeNameLast)) ? employeeInfo.employeeNameLast : gblEmployeeObject.lastName;
  	gblEmployeeObject.countryCode		= (!isEmpty(employeeInfo.countryCode)) ? employeeInfo.countryCode : gblEmployeeObject.countryCode;
  	gblEmployeeObject.businessCode		= (!isEmpty(employeeInfo.businessCode)) ? employeeInfo.businessCode : gblEmployeeObject.businessCode;
  	gblEmployeeObject.branchNumber		= (!isEmpty(employeeInfo.employeeBranchNumber)) ? employeeInfo.employeeBranchNumber : gblEmployeeObject.branchNumber;
  	if (!isEmpty(employeeInfo.employeeCode)){
      	gblEmployeeObject.setEmployeeCode(employeeInfo.employeeCode);
    }
  
  onInitialSyncDone();
}

/** Class definition - User/Employee **/
function Employee(){
  	this.empId						= 0;
  	this.employeeCode				= "0";
	this.firstName					= "";
	this.lastName					= "";
	this.email						= "";
  	this.countryCode				= "UK";
  	this.businessCode				= "R";
  	this.branchNumber				= "";  	
    
  	this.setEmployeeCode			= function(idVal){
      	this.employeeCode			= idVal;
    };
  
  	this.setEmployeeId				= function(idVal){
      	this.empId					= idVal;
    }; 	
  
  	this.getEmployeeName			= function(){
      	/*var empName					= "";
      	empName						+= (!isEmpty(this.firstName)) ? this.firstName : "";
      	empName						+= " ";
      	empName						+= (!isEmpty(this.lastName)) ? this.lastName : "";
      	return empName;*/
      	return this.email;
    };
}


function resetOnChangeSourceOrEmail	() {

  	    gblBasicInfo					= {};
        gblSharedInfo 					= {}; 
}

function isUploadServiceError(outputparams) {
   // kony.print("Inside isUploadServiceError--->>>");
     kony.print("Inside isUploadServiceError :");
    var isError = false;
    try {
       var jsonString = JSON.stringify(outputparams);
         printMessage("isUploadServiceError jsonString"+jsonString );
      
        if (jsonString !== null && jsonString !== "") {
           isError = jsonString.indexOf('isError\\\": true') > -1;
      
        }
    } catch (err) {
        printMessage("Error in isUploadServiceError function");
    }
    return isError;
}

