/*** Author  : RI - CTS - 363601 ****/


/** Show lead management form **/
function showLeadsManagement() {
  	printMessage("Inside - showLeadsManagement");
  	showLoading("common.message.1001");
  	frmLeadManagement.lblNoRecords.setVisibility(false);
  	frmLeadManagement.txtNotesValBg.setEnabled(false);
  
  	frmLeadManagement.txtNotesValBg.isReadOnly = true;
    gblShowAllLeadsFromHistoryTbl			= true;
    glbSegData								= [];
    glbSegNoData 							= [];
    frmLeadManagement.segLeads.setData([]);
    getLeadsFromLocalTbl(success_getLeadsForDisplaying, error_getLeadsForDisplaying, true);
}

/** Show lead management form for create lead and cancel**/
function showLeadsManagementFromLocal() {
  	printMessage("Inside - showLeadsManagement");
  	showLoading("common.message.1001");
  	frmLeadManagement.lblNoRecords.setVisibility(false);
  	frmLeadManagement.txtNotesValBg.setEnabled(false);
    frmLeadManagement.txtNotesValBg.isReadOnly = true;
    gblShowAllLeadsFromHistoryTbl			= true;
    glbSegData								= [];
  	glbSegNoData							= [];
  	
    frmLeadManagement.segLeads.setData([]);
    getLeadsFromLocalTbl(success_getLeadsForDisplaying_local, error_getLeadHistory, true);
}

/** Preshow - Form - frmLeadManagement **/
function onPreShowLeadManagement(){
  	printMessage("Inside - onPreShowLeadManagement");
  	showLoading("common.message.1001");
  	gblStartupFormIdentifier			= "frmLeadManagement";
  	if (gblIsSyncInitialized === false) {
      	frmLeadManagement.segLeads.setData([]);
      	initSyncModule();
    }
}

/** Postshow - Form - frmLeadManagement **/
function onPostShowLeadManagement(){
  
    frmLeadManagement.flxBurgerMenu.lblUserName.text =gblBasicInfo.email ;
    //alert('onPostShowLeadManagement'+gblBasicInfo.email);
  	printMessage("Inside - onPostShowLeadManagement");
  	refreshSettingsUI();
  	hideLeadFilters();
  	hideLeadDetails(); 
  	frmLeadManagement.flxFormHeader.lblHeaderTitle.text		= getI18nString("common.label.leadManagement");
  	frmLeadManagement.btnLoadMore.text			= getI18nString("common.label.loadMore");
  	frmLeadManagement.btnCreateLead.text		= getI18nString("common.label.createLead");  
  	frmLeadManagement.lblNoRecords.text			= getI18nString("common.label.noRecords");  
  	//update Subheader titles
  	frmLeadManagement.lblDateHdr.text			= getI18nString("common.label.date");  
  	frmLeadManagement.lblStatusHdr.text			= getI18nString("common.label.status");  
  	frmLeadManagement.lblCustomerHdr.text		= getI18nString("common.label.customer");  
  
  	printMessage(" frmLeadManagementlblDateHdr.text :: "+frmLeadManagement.lblDateHdr.text);
  
  	//Leads Details
  	frmLeadManagement.lblLeadDetailsHeader.text	= getI18nString("common.label.leadDetail");  
  	frmLeadManagement.lblLeadDate.text			= getI18nString("common.label.date")+":";  
  	frmLeadManagement.lblLeadStatus.text		= getI18nString("common.label.status")+":";  
  	frmLeadManagement.lblLeadCustomer.text		= getI18nString("common.label.customer")+":";  
  	frmLeadManagement.lblLeadTicketNumber.text	= getI18nString("common.label.ticketNumber");  
  
  	frmLeadManagement.lblFilterPopupHeader.text	= getI18nString("common.label.filterByStatus");  
  	frmLeadManagement.btnClearAll.text			= getI18nString("common.label.clearAll");  
  	frmLeadManagement.btnSelectAll.text			= getI18nString("common.label.selectAll");  
  	frmLeadManagement.btnFilterGo.text			= getI18nString("common.label.go");  
  	  
  
  	var filterList								= [];
  	//filterList.push(["OPEN", getI18nString("common.label.open")]);
    filterList.push(["NEW", getI18nString("common.label.new")]);
  	filterList.push(["PENDING", getI18nString("common.label.pending")]);
  	filterList.push(["CONVERTED", getI18nString("common.label.converted")]);
    filterList.push(["REJECTED",getI18nString("common.label.rejected")]);
  	frmLeadManagement.chkFilters.masterData		= filterList;
  	frmLeadManagement.chkFilters.selectedKeys	= [];
  
   //LAA42
    frmLeadManagement.flxBurgerMenu.flexSettingBtn4Container.setVisibility(false);
    //flexSettingBtn4Container
  	//frmLeadManagement.lblAppVersion.text		= getI18nString("common.label.version") +" " + LAConstants.APP_VERSION;
  	//setHumburgerDeviceDatetime(frmLeadManagement.lblAppVersion,frmLeadManagement.lblLastTransferred);
  	setMasterBurgerMenuVisibity();
}

/** Success getLeads - for displaying created leads in Leads management screen along with other history **/
function success_getLeadsForDisplaying(result){
  	printMessage("Inside  - success_getLeadsForDisplaying: " + JSON.stringify(result));
  	if (result.length > 0) {
      	gblIsLeadsNotSyncedPresent				= true;
      	gblLeadsNotSynced						= result;
    } else {
      	gblIsLeadsNotSyncedPresent				= false;
      	gblLeadsNotSynced						= [];
    }
 /* 	if (isNetworkAvailable() === true) {
      	startSyncLeadHistory();
    } else {
      	getLeadHistoryFromLocalTbl();
    }*/
    
    getLeadsErrorFromLocalTbl(success_getLeadsForDisplaying_localerror_main,error_getLeadsForDisplaying_localerror_main);
}


//getting response only from local

/** Success getLeads - for displaying created leads in Leads management screen only from local **/
function success_getLeadsForDisplaying_local(result){
  	printMessage("Inside  - success_getLeadsForDisplaying: " + JSON.stringify(result));
  	if (result.length > 0) {
      	gblIsLeadsNotSyncedPresent				= true;
      	gblLeadsNotSynced						= result;
    } else {
      	gblIsLeadsNotSyncedPresent				= false;
      	gblLeadsNotSynced						= [];
    }
     //modifying here to get from the error table
      getLeadsErrorFromLocalTbl(success_getLeadsForDisplaying_localerror,error_getLeadsForDisplaying_localerror);
      //	getLeadHistoryFromLocalTbl();
}

//getting data frm local error table
function success_getLeadsForDisplaying_localerror_main(result){
  	printMessage("Inside  - success_getLeadsForDisplaying: local error " + JSON.stringify(result));
  	if (result.length > 0) {
      	gblIsLeadsErrorSyncedPresent				= true;
      	gblLeadsErrorSynced						= result;
    } else {
      	gblIsLeadsErrorSyncedPresent				= false;
      	gblLeadsErrorSynced							= [];
    }
  	if (isNetworkAvailable() === true) {
      	startSyncLeadHistory();
    } else {
      	getLeadHistoryFromLocalTbl();
    }
}
function error_getLeadsForDisplaying_localerror_main(result){
  	printMessage("Inside  - error_getLeadsForDisplaying_localerror: local error " + JSON.stringify(result));
  
  		gblIsLeadsErrorSyncedPresent				= false;
  	////getLeadHistoryCountFromLocalTbl();
  	if (isNetworkAvailable() === true) {
      	startSyncLeadHistory();
    } else {
      	getLeadHistoryFromLocalTbl();
    }
}




//getting data frm local error table
function success_getLeadsForDisplaying_localerror(result){
  	printMessage("Inside  - success_getLeadsForDisplaying: local error " + JSON.stringify(result));
  	if (result.length > 0) {
      	gblIsLeadsErrorSyncedPresent				= true;
      	gblLeadsErrorSynced						= result;
    } else {
      	gblIsLeadsErrorSyncedPresent				= false;
      	gblLeadsErrorSynced							= [];
    }
  	getLeadHistoryFromLocalTbl();
}
function error_getLeadsForDisplaying_localerror(result){
  	printMessage("Inside  - error_getLeadsForDisplaying_localerror: local error " + JSON.stringify(result));
  
  	getLeadHistoryFromLocalTbl();
}

/** Error getLeads - for displaying created leads in Leads management screen along with other history **/
function error_getLeadsForDisplaying(result){
  	printMessage("Inside  - error_getLeadsForDisplaying");
  	gblIsLeadsNotSyncedPresent				= false;
  	////getLeadHistoryCountFromLocalTbl();
  	if (isNetworkAvailable() === true) {
      	startSyncLeadHistory();
    } else {
      	getLeadHistoryFromLocalTbl();
    }
}

/** Success callback - SQL query execution - get lead history  **/
function success_getLeadHistory(result){
  	printMessage("Inside - success_getLeadHistory");
  	printMessage("Inside - success_getLeadHistory - gblIsLeadsNotSyncedPresent: " + gblIsLeadsNotSyncedPresent);
  	printMessage("Inside - success_getLeadHistory - gblLeadsNotSynced : " + JSON.stringify(gblLeadsNotSynced));
  	printMessage("Inside - success_getLeadHistory - result : " + JSON.stringify(result));
  	printMessage("Inside - success_getLeadHistory - result : " + JSON.stringify(result));
  	glbSegNoData  						=[];
  	var oddOrEvenIndex					= 0;
  	var rowObject						= null;
  	var existingRowsCount				= 0;
  	var rowRecord						= {};
  
	if (!isEmpty(glbSegData) && glbSegData.length > 0) {
      	existingRowsCount				= glbSegData.length;
      	oddOrEvenIndex					= existingRowsCount;
    }
  	
  
  	if (gblIsLeadsNotSyncedPresent === true && gblLeadsNotSynced.length > 0){
      	// Load leads which are not yet synced with backend
        for (var j = 0; j < gblLeadsNotSynced.length; j++) {
            rowRecord					= gblLeadsNotSynced[j];
            rowObject					= {};
           	rowObject.lblDate			= getFormartedDate(rowRecord.createdDate);
            rowObject.lblStatus			= "";
            rowObject.lblCustomer		= rowRecord.propertyName;
            rowObject.ticketNumber		= rowRecord.ticketNumber;
            rowObject.notes				= rowRecord.ticketNotes;  
            // Added for LA77, LAA142
            rowObject.prospectStatusCode = "01";
            if (oddOrEvenIndex%2 === 0) {
              rowObject.template		= flxSegRowContainer;
            } else {
              rowObject.template		= flxSegRowAltContainer;
            }
            glbSegData.push(rowObject);          	
            oddOrEvenIndex++;
        }
      	gblIsLeadsNotSyncedPresent		= false;
    } 
  
   /** Loading Lead Error */


   printMessage("Inside - gblIsLeadsErrorSyncedPresent"+gblIsLeadsErrorSyncedPresent);
  if(gblIsLeadsErrorSyncedPresent && gblLeadsErrorSynced.length>0 ){
     printMessage("Inside - gblIsLeadsErrorSyncedPresent"+gblIsLeadsErrorSyncedPresent);
         for (var k = 0; k < gblLeadsErrorSynced.length; k++) {
            rowRecord					= gblLeadsErrorSynced[k];
            rowObject					= {};
            rowObject.lblDate			= getFormartedDate(rowRecord.createdDate);
            rowObject.lblStatus			= "";
            rowObject.lblCustomer		= rowRecord.propertyName;
            rowObject.ticketNumber		= rowRecord.ticketNumber;
            rowObject.notes				= rowRecord.ticketNotes;  
            rowObject.template		= flxSegRowFailContainer;
           
            // Added for LA77, LAA142
            rowObject.prospectStatusCode = "01";
            glbSegData.push(rowObject);          	
           // oddOrEvenIndex++;
        }
      	gblIsLeadsErrorSyncedPresent		= false;
    } 


  	// Load lead history
  	if (result.length > 0) {
      	// Load leads from history table 
      	for (var i = 0; i < result.length; i++) {
          	rowRecord					= result[i];
          	rowObject					= {};
          	//rowObject.lblDate			= rowRecord.createdDate;
          	rowObject.lblDate			= getFormartedDate(rowRecord.createdDate);
        	rowObject.lblCustomer		= rowRecord.propertyName;
        	rowObject.ticketNumber		= rowRecord.ticketNumber;
            rowObject.notes				= rowRecord.ticketNotes;  
            //LAA 142
            rowObject.lblStatus			=rowRecord.prospectStatusDescription;
            rowObject.prospectStatusCode=rowRecord.prospectStatusCode;
          	if (oddOrEvenIndex%2 === 0) {
              	rowObject.template		= flxSegRowContainer;
            } else {
              	rowObject.template		= flxSegRowAltContainer;
            }
          	glbSegData.push(rowObject);
          	oddOrEvenIndex++;
        }
    } else if (glbSegData.length === 0) {
      	frmLeadManagement.segLeads.setData([]);
      	frmLeadManagement.segLeads.setVisibility(true);
        //Disable label
     	//frmLeadManagement.lblNoRecords.setVisibility(true);
        frmLeadManagement.lblNoRecords.setVisibility(false);
    }


  

  
  	printMessage("Inside - glbSegData"+JSON.stringify(glbSegData));
  	if (glbSegData.length > 0) {
      	frmLeadManagement.segLeads.setData(glbSegData);
        frmLeadManagement.segLeads.setVisibility(true);
      	frmLeadManagement.lblNoRecords.setVisibility(false);
        if (existingRowsCount > 0) {
            frmLeadManagement.segLeads.selectedRowIndex	= [0, (existingRowsCount - 1)];
        }
    }else{
      // PullToRefresh work - set nodata msg in the segment
      rowObject					= {};
      rowObject.ticketNumber		= getI18nString("common.message.noDataFound");
      rowObject.lblDate				= "";
      rowObject.lblStatus			= getI18nString("common.message.noDataFound");
      rowObject.lblCustomer			= "";
      //rowObject.imgInfo			= "info.png";
      rowObject.notes				= "";  
      rowObject.template			= flxSegNoData;

      glbSegNoData.push(rowObject);
      frmLeadManagement.segLeads.setData(glbSegNoData);
    }
        
  	
  	// The following codes are added for handling leadManagement form loading in the following scenario
  	// Create a lead -> Success alert message -> Click Ok -> Redirect to leadManagement screen
  	var frmObj							= kony.application.getCurrentForm();
  	if (frmObj.id !== LAConstants.FORM_LEAD_MANAGEMENT) {
      	frmLeadManagement.show();
    }
  
  	dismissLoading(); 
  	//LAA50
  if(glbIsPullToRefresh ===true){
    //Checnking any alerterrors 
    getAlertErrorsTableDataFromLocalDB();
  }
  glbIsPullToRefresh = false;
}

/** Error callback - SQL query execution - get lead history **/
function error_getLeadHistory(error) {
  	printMessage("Inside - error_getLeadHistory");
  	frmLeadManagement.segLeads.setData([]);
  	//frmLeadManagement.lblNoRecords.setVisibility(true);
    //Disable label
  	frmLeadManagement.lblNoRecords.setVisibility(false);
    dismissLoading();
}

/** Show lead filters popup for filtering lead list **/
function showLeadFilters() {
  //	if (!isEmpty(gblAppliedFilters)) {
    frmLeadManagement.chkFilters.selectedKeys	= gblAppliedFilters; 	      
   // }
  	frmLeadManagement.flxFiltersOverlay.setVisibility(true);
}

/** Hide lead filters popup **/
function hideLeadFilters() {
  	frmLeadManagement.flxFiltersOverlay.setVisibility(false);
}

/** Hide lead details popup **/
function hideLeadDetails() {
  	frmLeadManagement.flxLeadDetailsOverlay.setVisibility(false);
}

/** Show lead details popup **/
function showLeadDetails(sectionIndex, rowIndex){
    var leadObject									= frmLeadManagement.segLeads.selectedRowItems[0];
  	printMessage("leadObject: " + JSON.stringify(leadObject));
  
  	printMessage("leadObject: ticketNumber " + leadObject.ticketNumber);
  	printMessage("leadObject: ticketNumber " + (leadObject.ticketNumber === getI18nString("common.message.noDataFound")));
  if(leadObject.ticketNumber !== getI18nString("common.message.noDataFound")){
    frmLeadManagement.lblLeadDateVal.text			= leadObject.lblDate;
    frmLeadManagement.lblLeadStatusVal.text			= leadObject.lblStatus;
    frmLeadManagement.lblLeadCustomerVal.text		= leadObject.lblCustomer;
    frmLeadManagement.txtNotesVal.text				= leadObject.notes;
    frmLeadManagement.flxLeadDetailsOverlay.setVisibility(true);

    if(isEmpty(leadObject.ticketNumber) === false){
      var tNumber = ""+leadObject.ticketNumber;
      printMessage("showLeadDetails tNumber:: "+tNumber);
      frmLeadManagement.lblLeadTicketNumberVal.text	= tNumber;
    }else{
      frmLeadManagement.lblLeadTicketNumberVal.text	= "";
    }
  }
}

/** To format next date range set for loading lead history **/
function getDateRangeForNextPage() {
  	printMessage("Inside getDateRangeForNextPage"); 
  	printMessage("Inside getDateRangeForNextPage - gblCurrentPageIndex: " + gblCurrentPageIndex);
  	var dateRanges				= {};	
  	dateRanges.from				= "";
  	dateRanges.to				= "";
  	if (gblCurrentPageIndex >= 0 && ((gblCurrentPageIndex * LAConstants.PAGINATION_DAYS_RANGE) <= LAConstants.PAGINATION_DAYS_MAX_RANGE)) {
        var currentDateOne		= new Date();    
      	var currentDateTwo		= new Date();
      	printMessage("Inside getDateRangeForNextPage - currentDateOne.getDate(): " + currentDateOne.getDate());
      	printMessage("Inside getDateRangeForNextPage - currentDateTwo.getDate(): " + currentDateTwo.getDate());
        //var fromDateTimestamp	= currentDateOne.setDate(currentDateOne.getDate() - (gblCurrentPageIndex * LAConstants.PAGINATION_DAYS_RANGE));
        //var toDateTimestamp	= currentDateTwo.setDate(currentDateTwo.getDate() - ((gblCurrentPageIndex + 1) * LAConstants.PAGINATION_DAYS_RANGE));
      	var toDateTimestamp		= currentDateOne.setDate(currentDateOne.getDate() - (gblCurrentPageIndex * LAConstants.PAGINATION_DAYS_RANGE));
        var fromDateTimestamp	= currentDateTwo.setDate(currentDateTwo.getDate() - ((gblCurrentPageIndex + 1) * LAConstants.PAGINATION_DAYS_RANGE));
        printMessage("Inside getDateRangeForNextPage - fromDateTimestamp: " + fromDateTimestamp);
      	printMessage("Inside getDateRangeForNextPage - toDateTimestamp: " + toDateTimestamp);
      	var fromDate			= new Date(fromDateTimestamp);	
        var toDate				= new Date(toDateTimestamp);
        dateRanges.from			= getDateTimeInFormat(fromDate, LAConstants.LEAD_DATE_FORMAT);
        dateRanges.to			= getDateTimeInFormat(toDate, LAConstants.LEAD_DATE_FORMAT);
        printMessage("Inside getDateRangeForNextPage - dateRanges: " + JSON.stringify(dateRanges));
    }
  	return dateRanges;
}

/** To format previous date range set for loading lead history **/
function getDateRangeForPreviousPage() {
  	printMessage("Inside getDateRangeForPreviousPage"); 
  	printMessage("Inside getDateRangeForPreviousPage - gblCurrentPageIndex: " + gblCurrentPageIndex);
  	var dateRanges				= {};	
  	dateRanges.from				= "";
  	dateRanges.to				= "";
  	if (gblCurrentPageIndex >= 0 && ((gblCurrentPageIndex * LAConstants.PAGINATION_DAYS_RANGE) <= LAConstants.PAGINATION_DAYS_MAX_RANGE)) {
      	printMessage("Inside getDateRangeForPreviousPage - gblCurrentPageIndex: " + gblCurrentPageIndex);
      	var prevPageIndex		= gblCurrentPageIndex - 1;
        var currentDateOne		= new Date(); 
      	var currentDateTwo		= new Date();
      	printMessage("Inside getDateRangeForPreviousPage - currentDateOne.getDate(): " + currentDateOne.getDate());
      	printMessage("Inside getDateRangeForPreviousPage - currentDateTwo.getDate(): " + currentDateTwo.getDate());
        //var fromDateTimestamp	= currentDateOne.setDate(currentDateOne.getDate() - (prevPageIndex * LAConstants.PAGINATION_DAYS_RANGE));
        //var toDateTimestamp	= currentDateTwo.setDate(currentDateTwo.getDate() - ((prevPageIndex + 1) * LAConstants.PAGINATION_DAYS_RANGE));
      
      	var toDateTimestamp		= currentDateOne.setDate(currentDateOne.getDate() - (prevPageIndex * LAConstants.PAGINATION_DAYS_RANGE));
      	var fromDateTimestamp	= currentDateTwo.setDate(currentDateTwo.getDate() - ((prevPageIndex + 1) * LAConstants.PAGINATION_DAYS_RANGE));
      
        printMessage("Inside getDateRangeForPreviousPage - fromDateTimestamp: " + fromDateTimestamp);
      	printMessage("Inside getDateRangeForPreviousPage - toDateTimestamp: " + toDateTimestamp);
      	var fromDate			= new Date(fromDateTimestamp);	
        var toDate				= new Date(toDateTimestamp);
        dateRanges.from			= getDateTimeInFormat(fromDate, LAConstants.LEAD_DATE_FORMAT);
        dateRanges.to			= getDateTimeInFormat(toDate, LAConstants.LEAD_DATE_FORMAT);
        printMessage("Inside getDateRangeForNextPage - dateRanges: " + JSON.stringify(dateRanges));
    }
  	return dateRanges;
}

/** Show more lead history **/
function showMoreLeadHistory() {
  //alert("Load More");
  //showLoading("common.message.1001");
  showLoading("common.message.2001");
  //gblCurrentPageIndex =1;
  //gblCurrentPageIndex 			= ++gblCurrentPageIndex;
  printMessage("showMoreLeadHistory::: before gblCurrentPageIndex "+gblCurrentPageIndex);
  if(gblCurrentPageIndex === 0){
    gblCurrentPageIndex =1;
  }
  printMessage("showMoreLeadHistory::: after gblCurrentPageIndex "+gblCurrentPageIndex);
  gblAppliedFilters				= ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
  gblIsFilterApplied 			= false;
  frmLeadManagement.imageFilledFilter.setVisibility(false);
  frmLeadManagement.imgFilledFilter.setVisibility(true);
  //getLeadHistoryCountFromLocalTbl();
  if (isNetworkAvailable() === true) {
    startSyncLeadHistory();
  } else {
    //Display No Internet Alert
    showMessagePopup("common.message.2002", ActionPopupMessageOk);
    // UnComment
    getLeadHistoryFromLocalTbl();
  }
}

/** Filter - Go button handler **/
function onClickLeadFilterGoBtn() {

   printMessage("Inside - Filter Go button"+JSON.stringify(glbSegData));
  
  showLoading("common.message.2001");
  glbSegNoData  						= [];
  var selFil = frmLeadManagement.chkFilters.selectedKeyValues;
  var selKeys =  frmLeadManagement.chkFilters.selectedKeys;
  printMessage("Inside - Filter Go button length "+selFil);
  printMessage("Inside - Filter Go button"+JSON.stringify(selFil));
  printMessage("Inside - Filter length isResultNotNull "+isResultNotNull(selFil));
  
  var datalist = glbSegData;
  var filterList = [];
  if(isResultNotNull(selFil) === false)
  {
    /** Here we are not going to show any results on screen as all filters are empty **/
    printMessage("Going into if condition");  
    isFilterApplied = true;
    frmLeadManagement.segLeads.removeAll();
   // frmLeadManagement.segLeads.setData(glbSegData);
    gblAppliedFilters = [];
    
  }
  else{
       printMessage("Going into else condition"+JSON.stringify(datalist));
     	printMessage("Going into else selFil"+JSON.stringify(selFil));
    isFilterApplied = true;
    if(isResultNotNull(selFil)) {
    	frmLeadManagement.segLeads.removeAll();
    }
    printMessage("Inside - Filter selected fil"+selFil);
    printMessage("Inside - Filter length"+JSON.stringify(selFil));
    printMessage("Inside - Filter length isResultNotNull "+isResultNotNull(selFil));
    var selctedKey =[];
    if(isResultNotNull(selFil) && selFil.length >0 && selFil.length <4){
    for(var k =0 ; k<selFil.length ;k++ ){
      if(kony.string.equalsIgnoreCase(selFil[k][1], getI18nString("common.label.new"))){
         selctedKey.push("01");
       }
     else if(kony.string.equalsIgnoreCase(selFil[k][1], getI18nString("common.label.pending"))){
        selctedKey.push("06");
        selctedKey.push("07");
        selctedKey.push("08");
        
       }
     else if(kony.string.equalsIgnoreCase(selFil[k][1], getI18nString("common.label.converted"))){
        selctedKey.push("02");
        selctedKey.push("03");
        selctedKey.push("04");
       }
     else if(kony.string.equalsIgnoreCase(selFil[k][1],getI18nString("common.label.rejected"))){
        selctedKey.push("05");
       
       }
    }
    for(var i = 0; i < datalist.length; i++) {
      if(selctedKey.indexOf(datalist[i].prospectStatusCode)>-1){
        filterList.push(datalist[i]);
      }
    }
    }
    
    printMessage("filterList"+JSON.stringify(filterList));
  
    if((isResultNotNull(filterList) && filterList.length > 0)|| (isResultNotNull(selFil) && ( selFil.length <4) )){
      frmLeadManagement.segLeads.setData(filterList);
      gblFilterList = filterList;
      gblAppliedFilters = selKeys;
    }else{
     // alert("No records found");
      if(selFil.length===4){
      isFilterApplied =false;
      frmLeadManagement.segLeads.setData(glbSegData);
      filterList = glbSegData;
      gblAppliedFilters				= ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
      }
      
      else{
        gblAppliedFilters				= [];
      }
    }
    //Update Segment
  //  if(isResultNotNull(glbSegData) && glbSegData.length === 0){
      if(isResultNotNull(filterList) && filterList.length === 0){
        // PullToRefresh work - set nodata msg in the segment
        var rowObject					= {};
        rowObject.ticketNumber		= getI18nString("common.message.noDataFound");
        rowObject.lblDate			= "";
        rowObject.lblStatus			= getI18nString("common.message.noDataFound");
        rowObject.lblCustomer		= "";
        //rowObject.imgInfo			= "info.png";
        rowObject.notes				= "";  
        rowObject.template			= flxSegNoData;
        glbSegNoData.push(rowObject);
        frmLeadManagement.segLeads.setData(glbSegNoData);
      }
   // }
  }
  	frmLeadManagement.flxFiltersOverlay.setVisibility(false);
  
    if(isFilterApplied){
       frmLeadManagement.imageFilledFilter.setVisibility(true);
       frmLeadManagement.imgFilledFilter.setVisibility(false);
       dismissLoading();
    }else{
      frmLeadManagement.imageFilledFilter.setVisibility(false);
      frmLeadManagement.imgFilledFilter.setVisibility(true);
      dismissLoading();
    }
}

/** Filter - Clear All button handler **/
function onClickLeadFilterClearAllBtn() {
  	frmLeadManagement.chkFilters.selectedKeys	= [];
}

/** Filter - Select All button handler **/
function onClickLeadFilterSelectAllBtn() {
  	frmLeadManagement.chkFilters.selectedKeys	= ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
}


/** Load data on lead management screen **/
/*function loadLeadsOnSegment(){
  	var segmentMasterData				= [];
  	frmLeadManagement.segLeads.setData(segmentMasterData);
  	if (gblTestData.length > 0) {
      	var segSessionDataSet			= [];
      	var segRowDataSet				= [];
      	var sessionObject				= {};
      	var rowObject					= null;
        sessionObject.lblDate			= "Date";
        sessionObject.lblStatus			= "Status";
        sessionObject.lblCustomer		= "Customer";
        sessionObject.lblAction			= "";
        segSessionDataSet.push(sessionObject);
      	var rowRecord					= {};
      	for (var i = 0; i < gblTestData.length; i++) {
          	rowRecord					= gblTestData[i];
          	rowObject					= {};
          	rowObject.lblDate			= rowRecord.date;
        	rowObject.lblStatus			= rowRecord.status;
        	rowObject.lblCustomer		= rowRecord.customer;
        	rowObject.imgInfo			= "info.png";
          	rowObject.ticketNumber		= rowRecord.ticketNumber;
            rowObject.notes				= rowRecord.notes;  
          	if (i%2 === 0) {
              	rowObject.template		= flxSegRowContainer;
            } else {
              	rowObject.template		= flxSegRowAltContainer;
            }
          	segRowDataSet.push(rowObject);
        }
      	segSessionDataSet.push(segRowDataSet);
      	segmentMasterData.push(segSessionDataSet);
      	frmLeadManagement.segLeads.setData(segmentMasterData);
    } else {
     	 
    }
}*/

function toRefreshManagementPush() {
  //alert('refresh');
 /* glbSegData								= [];
  frmLeadManagement.segLeads.setData([]);
  gblCurrentPageIndex =0;
  showMoreLeadHistory();*/
  
  
  gblAppliedFilters				= ["NEW", "PENDING", "CONVERTED", "REJECTED" ];
  gblIsFilterApplied 			= false;
  frmLeadManagement.imageFilledFilter.setVisibility(false);
  frmLeadManagement.imgFilledFilter.setVisibility(true);
  
  if (isNetworkAvailable() === true) {

    frmLeadManagement.lblNoRecords.setVisibility(false);
    frmLeadManagement.txtNotesValBg.setEnabled(false);
    frmLeadManagement.txtNotesValBg.isReadOnly = true;
    gblShowAllLeadsFromHistoryTbl			= true;
    glbSegData								= [];
    glbSegNoData							= [];
    frmLeadManagement.segLeads.setData([]);

    glbIsPullToRefresh 			= true;
    gblCurrentPageIndex =0;
    getLeadsFromLocalTbl(success_getLeadsForDisplaying, error_getLeadsForDisplaying, true);
    //LAA50

  } else {
    //Display No Internet Alert
    showMessagePopup("common.message.2002", ActionPopupMessageOk);
    //getLeadHistoryFromLocalTbl();
  }
}

//=================== #82 issue =======================================

function constructLeadsErrorInputForSyncUpload(){

	getLeadsESBErrorsFromLocalTbl(success_getLeadESBErrorsForUpload, error_getLeadsESBErrorsForUpload, false);
}
/** Should there be errors received for the upload syncs (e.g. Create Lead and Create Alert) in the code range of 500 - 599 or error code 429, then there should be an automatic method of retrying the send them. The error codes indicates that the ESB was busy and unable to handle to request at that point, or there was an issue with iCABS being unavailable, or the messages were throttled in the API gateway. When the technician completes an activity that triggers the upload syncs, the app should automatically attempt to retry the sending of the errored upload syncs */

function getLeadsESBErrorsFromLocalTbl(successCallbackMethod, errorCallbackMethod){
  	printMessage(" Inside - getLeadsESBErrorsFromLocalTbl ");
  	var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	/*var sqlStatement		= "SELECT leadId, authorEmailAddress, accountCode, contractCode, propertyCode, propertyName, propertyAddressLine1, propertyAddressLine2, propertyAddressLine3, propertyAddressLine4, propertyAddressLine5, propertyPostcode, propertyContactName, propertyContactPosition, propertyContactTelephone, propertyContactMobile, propertyContactEmail, propertyContactFax, ticketNotes, createdDate, createdTime, leadDeleted, updateDateTime  FROM leads WHERE countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "'";*/
	
	 var sqlStatement		= "SELECT leaderrors.* FROM leaderrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599 +"' OR LAError.errorCode = '429')  AND LAError.leadalertIds   = leaderrors.leadId";

  	
  	printMessage(" Inside - getLeadsESBErrorsFromLocalTbl - sqlStatement: " + sqlStatement);		  
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

function success_getLeadESBErrorsForUpload(result){
  	printMessage("Inside  - success_getLeadESBErrorsForUpload::: "+JSON.stringify(result));
  	if (result.length > 0) {
      	gblLeadsESBErrorCollectionForScheduler		= [];
      	var aRecord							= null;
      	for (var i = 0; i < result.length; i++) {
          	aRecord							= result[i];
          	aRecord.leadDeleted				= (aRecord.leadDeleted == "false") ? false : true;
          	gblLeadsESBErrorCollectionForScheduler.push(aRecord);
        }
    } else {
      	gblLeadsESBErrorCollectionForScheduler	= [];
    }
  	//constructAlertsErrorsInputForSyncUpload();
  	uploadLeadESBErrorRecords();
  
}
/** Error getLeads - for uploading created leads **/
function error_getLeadsESBErrorsForUpload(result){
  	printMessage("Inside  - error_getLeadsESBErrorsForUpload");
  	gblLeadsESBErrorCollectionForScheduler		= [];
  	constructAlertsErrorsInputForSyncUpload();
}

function constructAlertsErrorsInputForSyncUpload(){
  	getAlertsESBErrorsFromLocalTbl(success_getAlertsESBErrorsForUpload, error_getAlertsESBErrorsForUpload);  
}


function getAlertsESBErrorsFromLocalTbl(successCallbackMethod, errorCallbackMethod){
  	printMessage(" Inside - getAlertsESBErrorsFromLocalTbl ");
  	//var sqlStatement		= " SELECT * FROM leaderrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599+"' OR LAError.errorCode = '429')  AND LAError.leadalertIds = leaderrors.leadId";
  	var sqlStatement		= "SELECT alerterrors.* FROM alerterrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599 +"' OR LAError.errorCode = '429')  AND LAError.leadalertIds   = alerterrors.alertId";

	printMessage(" Inside - getAlertsESBErrorsFromLocalTbl - sqlStatement: " + sqlStatement);	
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

function success_getAlertsESBErrorsForUpload(result){
  
  
  	printMessage("Inside  - success_getAlertsESBErrorsForUpload:" + JSON.stringify(result));
  	if (result.length > 0) {
      	gblAlertsESBErrorCollectionForScheduler		= [];
      	var aRecord							= null;
      	for (var i = 0; i < result.length; i++) {
          	aRecord							= result[i];
          	aRecord.alertDeleted			= (aRecord.alertDeleted == "false") ? false : true;
          	gblAlertsESBErrorCollectionForScheduler.push(aRecord);
        }
    } else {
      	gblAlertsESBErrorCollectionForScheduler	= [];
    }
  	////startSyncLeadsAndAlertsUpload();
  	//uploadLeadESBErrorRecords();
  	  uploadAlertESBErrorRecords();
}	

/** Error getAlert - for uploading created alerts **/
function error_getAlertsESBErrorsForUpload(result){
  	printMessage("Inside  - error_getAlertsESBErrorsForUpload: " + JSON.stringify(result));
  	gblAlertsESBErrorCollectionForScheduler		= [];
  	////startSyncLeadsAndAlertsUpload();
  	uploadLeadESBErrorRecords();
}
function uploadLeadESBErrorRecords() {
  	printMessage("Inside uploadLeadESBErrorRecords ");
  	printMessage("Inside uploadLeadESBErrorRecords - gblLeadsESBErrorCollectionForScheduler.length: " + gblLeadsESBErrorCollectionForScheduler.length);
  	gblIsCheckESBError 						= true;
	printMessage("Inside uploadLeadESBErrorRecords - gblIsCheckESBError: " + gblIsCheckESBError);	
  if (!isEmpty(gblLeadsESBErrorCollectionForScheduler) && gblLeadsESBErrorCollectionForScheduler.length > 0) {
      	gblUploadTicket.type			= "LEAD";
      	var params						= {};
        //params.client_id 				= LAConstants.INTEGRATION_CLIENT_ID;
   	    //params.client_secret 			= LAConstants.INTEGRATION_CLIENT_SECRET;
  		params.businessCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  		params.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  		params.email 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  		params.languageCode				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
		//params.lead 					= encodeURI(JSON.stringify(gblLeadsESBErrorCollectionForScheduler));
      	params.lead 					= gblLeadsESBErrorCollectionForScheduler;
      	params.sourceApplication 		= "LAAS";
      if (isNetworkAvailable() === true) {
        var anIntegrationClient			= new invokeIntegrationService(params);   
      }else{
        gblUploadTicket.isLeadServiceFinished 		= true;
		updateIntegrationServiceCallFlag();
      }
    } else {
         printMessage("Inside uploadLeadRecords else");
      	gblUploadTicket.isLeadServiceFinished	= true;
      	gblUploadTicket.succeededLeadsIds		= [];
      	//uploadAlertESBErrorRecords();
      	constructAlertsErrorsInputForSyncUpload();
    }
}
function uploadAlertESBErrorRecords() {
  	printMessage("Inside uploadAlertESBErrorRecords ");
  	printMessage("Inside uploadAlertESBErrorRecords - gblAlertsESBErrorCollectionForScheduler.length: " + gblAlertsESBErrorCollectionForScheduler.length);
  	gblIsCheckESBError 						= false;
	printMessage("Inside uploadAlertESBErrorRecords - gblIsCheckESBError: " + gblIsCheckESBError);
  
  if (!isEmpty(gblAlertsESBErrorCollectionForScheduler) && gblAlertsESBErrorCollectionForScheduler.length > 0) {
      	gblUploadTicket.type			= "ALERT";
      	var params						= {};
      	params.client_id 				= LAConstants.INTEGRATION_CLIENT_ID;
   		params.client_secret 			= LAConstants.INTEGRATION_CLIENT_SECRET;
  		params.businessCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  		params.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  		params.email 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  		params.languageCode				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
		//params.alert 					= encodeURI(JSON.stringify(gblAlertsESBErrorCollectionForScheduler));
      	params.alert 					= gblAlertsESBErrorCollectionForScheduler;
      	params.sourceApplication 		= "LAAS";
      if (isNetworkAvailable() === true) {
        var anIntegrationClient			= new invokeIntegrationService(params);  
      }else{
        gblUploadTicket.isAlertServiceFinished 		= true;
		updateIntegrationServiceCallFlag();
      }
    } else {
      printMessage("Inside uploadAlertRecords else");
      	gblUploadTicket.isAlertServiceFinished	= true;
      	gblUploadTicket.succeededAlertsIds		= [];
      	//initiate15MinSyncCall();
      	updateIntegrationServiceCallFlag();
    }
}


