//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Tue Nov 06 16:41:58 UTC 2018leadhistory*******************
// **********************************Start leadhistory's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}




/************************************************************************************
* Creates new leadhistory
*************************************************************************************/
leadhistory = function(){
	this.accountCode = null;
	this.authorEmailAddress = null;
	this.businessCode = null;
	this.contractCode = null;
	this.countryCode = null;
	this.createdDate = null;
	this.propertyAddressLine1 = null;
	this.propertyAddressLine2 = null;
	this.propertyAddressLine3 = null;
	this.propertyAddressLine4 = null;
	this.propertyAddressLine5 = null;
	this.propertyContactEmail = null;
	this.propertyContactFax = null;
	this.propertyContactMobile = null;
	this.propertyContactName = null;
	this.propertyContactPosition = null;
	this.propertyContactTelephone = null;
	this.propertyName = null;
	this.propertyPostcode = null;
	this.ticketID = null;
	this.ticketNotes = null;
	this.ticketNotesLatest = null;
	this.ticketNumber = null;
	this.ticketStatusCode = null;
	this.ticketStatusDescription = null;
	this.ticketTypeCode = null;
	this.ticketTypeDetailCode = null;
	this.propertyCode = null;
	this.prospectStatusCode = null;
	this.prospectStatusDescription = null;
	this.convertedContractValue = null;
	this.markForUpload = true;
};

leadhistory.prototype = {
	get accountCode(){
		return this._accountCode;
	},
	set accountCode(val){
		this._accountCode = val;
	},
	get authorEmailAddress(){
		return this._authorEmailAddress;
	},
	set authorEmailAddress(val){
		this._authorEmailAddress = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get contractCode(){
		return this._contractCode;
	},
	set contractCode(val){
		this._contractCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get createdDate(){
		return this._createdDate;
	},
	set createdDate(val){
		this._createdDate = val;
	},
	get propertyAddressLine1(){
		return this._propertyAddressLine1;
	},
	set propertyAddressLine1(val){
		this._propertyAddressLine1 = val;
	},
	get propertyAddressLine2(){
		return this._propertyAddressLine2;
	},
	set propertyAddressLine2(val){
		this._propertyAddressLine2 = val;
	},
	get propertyAddressLine3(){
		return this._propertyAddressLine3;
	},
	set propertyAddressLine3(val){
		this._propertyAddressLine3 = val;
	},
	get propertyAddressLine4(){
		return this._propertyAddressLine4;
	},
	set propertyAddressLine4(val){
		this._propertyAddressLine4 = val;
	},
	get propertyAddressLine5(){
		return this._propertyAddressLine5;
	},
	set propertyAddressLine5(val){
		this._propertyAddressLine5 = val;
	},
	get propertyContactEmail(){
		return this._propertyContactEmail;
	},
	set propertyContactEmail(val){
		this._propertyContactEmail = val;
	},
	get propertyContactFax(){
		return this._propertyContactFax;
	},
	set propertyContactFax(val){
		this._propertyContactFax = val;
	},
	get propertyContactMobile(){
		return this._propertyContactMobile;
	},
	set propertyContactMobile(val){
		this._propertyContactMobile = val;
	},
	get propertyContactName(){
		return this._propertyContactName;
	},
	set propertyContactName(val){
		this._propertyContactName = val;
	},
	get propertyContactPosition(){
		return this._propertyContactPosition;
	},
	set propertyContactPosition(val){
		this._propertyContactPosition = val;
	},
	get propertyContactTelephone(){
		return this._propertyContactTelephone;
	},
	set propertyContactTelephone(val){
		this._propertyContactTelephone = val;
	},
	get propertyName(){
		return this._propertyName;
	},
	set propertyName(val){
		this._propertyName = val;
	},
	get propertyPostcode(){
		return this._propertyPostcode;
	},
	set propertyPostcode(val){
		this._propertyPostcode = val;
	},
	get ticketID(){
		return this._ticketID;
	},
	set ticketID(val){
		this._ticketID = val;
	},
	get ticketNotes(){
		return this._ticketNotes;
	},
	set ticketNotes(val){
		this._ticketNotes = val;
	},
	get ticketNotesLatest(){
		return this._ticketNotesLatest;
	},
	set ticketNotesLatest(val){
		this._ticketNotesLatest = val;
	},
	get ticketNumber(){
		return this._ticketNumber;
	},
	set ticketNumber(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute ticketNumber in leadhistory.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._ticketNumber = val;
	},
	get ticketStatusCode(){
		return this._ticketStatusCode;
	},
	set ticketStatusCode(val){
		this._ticketStatusCode = val;
	},
	get ticketStatusDescription(){
		return this._ticketStatusDescription;
	},
	set ticketStatusDescription(val){
		this._ticketStatusDescription = val;
	},
	get ticketTypeCode(){
		return this._ticketTypeCode;
	},
	set ticketTypeCode(val){
		this._ticketTypeCode = val;
	},
	get ticketTypeDetailCode(){
		return this._ticketTypeDetailCode;
	},
	set ticketTypeDetailCode(val){
		this._ticketTypeDetailCode = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get prospectStatusCode(){
		return this._prospectStatusCode;
	},
	set prospectStatusCode(val){
		this._prospectStatusCode = val;
	},
	get prospectStatusDescription(){
		return this._prospectStatusDescription;
	},
	set prospectStatusDescription(val){
		this._prospectStatusDescription = val;
	},
	get convertedContractValue(){
		return this._convertedContractValue;
	},
	set convertedContractValue(val){
		this._convertedContractValue = val;
	},
};

/************************************************************************************
* Retrieves all instances of leadhistory SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "accountCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "authorEmailAddress";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* leadhistory.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
leadhistory.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering leadhistory.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	orderByMap = kony.sync.formOrderByClause("leadhistory",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering leadhistory.getAll->successcallback");
		successcallback(leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of leadhistory present in local database.
*************************************************************************************/
leadhistory.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.getAllCount function");
	leadhistory.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of leadhistory using where clause in the local Database
*************************************************************************************/
leadhistory.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering leadhistory.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of leadhistory in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  leadhistory.prototype.create function");
	var valuestable = this.getValuesTable(true);
	leadhistory.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
leadhistory.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  leadhistory.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "leadhistory.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"leadhistory",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  leadhistory.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = leadhistory.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "ticketNumber=" + valuestable.ticketNumber;
		pks["ticketNumber"] = {key:"ticketNumber",value:valuestable.ticketNumber};
		leadhistory.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of leadhistory in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].accountCode = "accountCode_0";
*		valuesArray[0].authorEmailAddress = "authorEmailAddress_0";
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].contractCode = "contractCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].createdDate = "createdDate_0";
*		valuesArray[0].propertyAddressLine1 = "propertyAddressLine1_0";
*		valuesArray[0].propertyAddressLine2 = "propertyAddressLine2_0";
*		valuesArray[0].propertyAddressLine3 = "propertyAddressLine3_0";
*		valuesArray[0].propertyAddressLine4 = "propertyAddressLine4_0";
*		valuesArray[0].propertyAddressLine5 = "propertyAddressLine5_0";
*		valuesArray[0].propertyContactEmail = "propertyContactEmail_0";
*		valuesArray[0].propertyContactFax = "propertyContactFax_0";
*		valuesArray[0].propertyContactMobile = "propertyContactMobile_0";
*		valuesArray[0].propertyContactName = "propertyContactName_0";
*		valuesArray[0].propertyContactPosition = "propertyContactPosition_0";
*		valuesArray[0].propertyContactTelephone = "propertyContactTelephone_0";
*		valuesArray[0].propertyName = "propertyName_0";
*		valuesArray[0].propertyPostcode = "propertyPostcode_0";
*		valuesArray[0].ticketID = "ticketID_0";
*		valuesArray[0].ticketNotes = "ticketNotes_0";
*		valuesArray[0].ticketNotesLatest = "ticketNotesLatest_0";
*		valuesArray[0].ticketNumber = 0;
*		valuesArray[0].ticketStatusCode = "ticketStatusCode_0";
*		valuesArray[0].ticketStatusDescription = "ticketStatusDescription_0";
*		valuesArray[0].ticketTypeCode = "ticketTypeCode_0";
*		valuesArray[0].ticketTypeDetailCode = "ticketTypeDetailCode_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].prospectStatusCode = "prospectStatusCode_0";
*		valuesArray[0].prospectStatusDescription = "prospectStatusDescription_0";
*		valuesArray[0].convertedContractValue = "convertedContractValue_0";
*		valuesArray[1] = {};
*		valuesArray[1].accountCode = "accountCode_1";
*		valuesArray[1].authorEmailAddress = "authorEmailAddress_1";
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].contractCode = "contractCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].createdDate = "createdDate_1";
*		valuesArray[1].propertyAddressLine1 = "propertyAddressLine1_1";
*		valuesArray[1].propertyAddressLine2 = "propertyAddressLine2_1";
*		valuesArray[1].propertyAddressLine3 = "propertyAddressLine3_1";
*		valuesArray[1].propertyAddressLine4 = "propertyAddressLine4_1";
*		valuesArray[1].propertyAddressLine5 = "propertyAddressLine5_1";
*		valuesArray[1].propertyContactEmail = "propertyContactEmail_1";
*		valuesArray[1].propertyContactFax = "propertyContactFax_1";
*		valuesArray[1].propertyContactMobile = "propertyContactMobile_1";
*		valuesArray[1].propertyContactName = "propertyContactName_1";
*		valuesArray[1].propertyContactPosition = "propertyContactPosition_1";
*		valuesArray[1].propertyContactTelephone = "propertyContactTelephone_1";
*		valuesArray[1].propertyName = "propertyName_1";
*		valuesArray[1].propertyPostcode = "propertyPostcode_1";
*		valuesArray[1].ticketID = "ticketID_1";
*		valuesArray[1].ticketNotes = "ticketNotes_1";
*		valuesArray[1].ticketNotesLatest = "ticketNotesLatest_1";
*		valuesArray[1].ticketNumber = 1;
*		valuesArray[1].ticketStatusCode = "ticketStatusCode_1";
*		valuesArray[1].ticketStatusDescription = "ticketStatusDescription_1";
*		valuesArray[1].ticketTypeCode = "ticketTypeCode_1";
*		valuesArray[1].ticketTypeDetailCode = "ticketTypeDetailCode_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].prospectStatusCode = "prospectStatusCode_1";
*		valuesArray[1].prospectStatusDescription = "prospectStatusDescription_1";
*		valuesArray[1].convertedContractValue = "convertedContractValue_1";
*		valuesArray[2] = {};
*		valuesArray[2].accountCode = "accountCode_2";
*		valuesArray[2].authorEmailAddress = "authorEmailAddress_2";
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].contractCode = "contractCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].createdDate = "createdDate_2";
*		valuesArray[2].propertyAddressLine1 = "propertyAddressLine1_2";
*		valuesArray[2].propertyAddressLine2 = "propertyAddressLine2_2";
*		valuesArray[2].propertyAddressLine3 = "propertyAddressLine3_2";
*		valuesArray[2].propertyAddressLine4 = "propertyAddressLine4_2";
*		valuesArray[2].propertyAddressLine5 = "propertyAddressLine5_2";
*		valuesArray[2].propertyContactEmail = "propertyContactEmail_2";
*		valuesArray[2].propertyContactFax = "propertyContactFax_2";
*		valuesArray[2].propertyContactMobile = "propertyContactMobile_2";
*		valuesArray[2].propertyContactName = "propertyContactName_2";
*		valuesArray[2].propertyContactPosition = "propertyContactPosition_2";
*		valuesArray[2].propertyContactTelephone = "propertyContactTelephone_2";
*		valuesArray[2].propertyName = "propertyName_2";
*		valuesArray[2].propertyPostcode = "propertyPostcode_2";
*		valuesArray[2].ticketID = "ticketID_2";
*		valuesArray[2].ticketNotes = "ticketNotes_2";
*		valuesArray[2].ticketNotesLatest = "ticketNotesLatest_2";
*		valuesArray[2].ticketNumber = 2;
*		valuesArray[2].ticketStatusCode = "ticketStatusCode_2";
*		valuesArray[2].ticketStatusDescription = "ticketStatusDescription_2";
*		valuesArray[2].ticketTypeCode = "ticketTypeCode_2";
*		valuesArray[2].ticketTypeDetailCode = "ticketTypeDetailCode_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].prospectStatusCode = "prospectStatusCode_2";
*		valuesArray[2].prospectStatusDescription = "prospectStatusDescription_2";
*		valuesArray[2].convertedContractValue = "convertedContractValue_2";
*		leadhistory.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
leadhistory.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering leadhistory.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"leadhistory",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "ticketNumber=" + valuestable.ticketNumber;
				pks["ticketNumber"] = {key:"ticketNumber",value:valuestable.ticketNumber};
				var wcs = [];
				if(leadhistory.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  leadhistory.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  leadhistory.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = leadhistory.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates leadhistory using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  leadhistory.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	leadhistory.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
leadhistory.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  leadhistory.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(leadhistory.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"leadhistory",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = leadhistory.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates leadhistory(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering leadhistory.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"leadhistory",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  leadhistory.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, leadhistory.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = leadhistory.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, leadhistory.getPKTable());
	}
};

/************************************************************************************
* Updates leadhistory(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.accountCode = "accountCode_updated0";
*		inputArray[0].changeSet.authorEmailAddress = "authorEmailAddress_updated0";
*		inputArray[0].changeSet.businessCode = "businessCode_updated0";
*		inputArray[0].changeSet.contractCode = "contractCode_updated0";
*		inputArray[0].whereClause = "where ticketNumber = 0";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.accountCode = "accountCode_updated1";
*		inputArray[1].changeSet.authorEmailAddress = "authorEmailAddress_updated1";
*		inputArray[1].changeSet.businessCode = "businessCode_updated1";
*		inputArray[1].changeSet.contractCode = "contractCode_updated1";
*		inputArray[1].whereClause = "where ticketNumber = 1";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.accountCode = "accountCode_updated2";
*		inputArray[2].changeSet.authorEmailAddress = "authorEmailAddress_updated2";
*		inputArray[2].changeSet.businessCode = "businessCode_updated2";
*		inputArray[2].changeSet.contractCode = "contractCode_updated2";
*		inputArray[2].whereClause = "where ticketNumber = 2";
*		leadhistory.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
leadhistory.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering leadhistory.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898cf65f1fb";
	var tbname = "leadhistory";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"leadhistory",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, leadhistory.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  leadhistory.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, leadhistory.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  leadhistory.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = leadhistory.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes leadhistory using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.prototype.deleteByPK function");
	var pks = this.getPKTable();
	leadhistory.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
leadhistory.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering leadhistory.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function leadhistoryTransactionCallback(tx){
		sync.log.trace("Entering leadhistory.deleteByPK->leadhistory_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function leadhistoryErrorCallback(){
		sync.log.error("Entering leadhistory.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function leadhistorySuccessCallback(){
		sync.log.trace("Entering leadhistory.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leadhistory.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, leadhistoryTransactionCallback, leadhistorySuccessCallback, leadhistoryErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes leadhistory(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. leadhistory.remove("where accountCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
leadhistory.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering leadhistory.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function leadhistory_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function leadhistory_removeSuccess(){
		sync.log.trace("Entering leadhistory.remove->leadhistory_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering leadhistory.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering leadhistory.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, leadhistory_removeTransactioncallback, leadhistory_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes leadhistory using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
leadhistory.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	leadhistory.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
leadhistory.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function leadhistoryTransactionCallback(tx){
		sync.log.trace("Entering leadhistory.removeDeviceInstanceByPK -> leadhistoryTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function leadhistoryErrorCallback(){
		sync.log.error("Entering leadhistory.removeDeviceInstanceByPK -> leadhistoryErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function leadhistorySuccessCallback(){
		sync.log.trace("Entering leadhistory.removeDeviceInstanceByPK -> leadhistorySuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leadhistory.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, leadhistoryTransactionCallback, leadhistorySuccessCallback, leadhistoryErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes leadhistory(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
leadhistory.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function leadhistory_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function leadhistory_removeSuccess(){
		sync.log.trace("Entering leadhistory.remove->leadhistory_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering leadhistory.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering leadhistory.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, leadhistory_removeTransactioncallback, leadhistory_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves leadhistory using primary key from the local Database. 
*************************************************************************************/
leadhistory.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	leadhistory.getAllDetailsByPK(pks,successcallback,errorcallback);
};
leadhistory.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var wcs = [];
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering leadhistory.getAllDetailsByPK-> success callback function");
		successcallback(leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves leadhistory(s) using where clause from the local Database. 
* e.g. leadhistory.find("where accountCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
leadhistory.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of leadhistory with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	leadhistory.markForUploadbyPK(pks, successcallback, errorcallback);
};
leadhistory.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(leadhistory.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of leadhistory matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
leadhistory.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering leadhistory.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering leadhistory.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering leadhistory.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of leadhistory pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
leadhistory.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of leadhistory pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
leadhistory.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of leadhistory deferred for upload.
*************************************************************************************/
leadhistory.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, leadhistory.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to leadhistory in local database to last synced state
*************************************************************************************/
leadhistory.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to leadhistory's record with given primary key in local 
* database to last synced state
*************************************************************************************/
leadhistory.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leadhistory.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	leadhistory.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
leadhistory.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var wcs = [];
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leadhistory.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether leadhistory's record  
* with given primary key got deferred in last sync
*************************************************************************************/
leadhistory.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  leadhistory.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	leadhistory.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
leadhistory.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var wcs = [] ;
	var flag;
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether leadhistory's record  
* with given primary key is pending for upload
*************************************************************************************/
leadhistory.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  leadhistory.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	leadhistory.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
leadhistory.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leadhistory.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leadhistory.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leadhistory.getTableName();
	var wcs = [] ;
	var flag;
	if(leadhistory.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leadhistory.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
leadhistory.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering leadhistory.removeCascade function");
	var tbname = leadhistory.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


leadhistory.convertTableToObject = function(res){
	sync.log.trace("Entering leadhistory.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new leadhistory();
			obj.accountCode = res[i].accountCode;
			obj.authorEmailAddress = res[i].authorEmailAddress;
			obj.businessCode = res[i].businessCode;
			obj.contractCode = res[i].contractCode;
			obj.countryCode = res[i].countryCode;
			obj.createdDate = res[i].createdDate;
			obj.propertyAddressLine1 = res[i].propertyAddressLine1;
			obj.propertyAddressLine2 = res[i].propertyAddressLine2;
			obj.propertyAddressLine3 = res[i].propertyAddressLine3;
			obj.propertyAddressLine4 = res[i].propertyAddressLine4;
			obj.propertyAddressLine5 = res[i].propertyAddressLine5;
			obj.propertyContactEmail = res[i].propertyContactEmail;
			obj.propertyContactFax = res[i].propertyContactFax;
			obj.propertyContactMobile = res[i].propertyContactMobile;
			obj.propertyContactName = res[i].propertyContactName;
			obj.propertyContactPosition = res[i].propertyContactPosition;
			obj.propertyContactTelephone = res[i].propertyContactTelephone;
			obj.propertyName = res[i].propertyName;
			obj.propertyPostcode = res[i].propertyPostcode;
			obj.ticketID = res[i].ticketID;
			obj.ticketNotes = res[i].ticketNotes;
			obj.ticketNotesLatest = res[i].ticketNotesLatest;
			obj.ticketNumber = res[i].ticketNumber;
			obj.ticketStatusCode = res[i].ticketStatusCode;
			obj.ticketStatusDescription = res[i].ticketStatusDescription;
			obj.ticketTypeCode = res[i].ticketTypeCode;
			obj.ticketTypeDetailCode = res[i].ticketTypeDetailCode;
			obj.propertyCode = res[i].propertyCode;
			obj.prospectStatusCode = res[i].prospectStatusCode;
			obj.prospectStatusDescription = res[i].prospectStatusDescription;
			obj.convertedContractValue = res[i].convertedContractValue;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

leadhistory.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering leadhistory.filterAttributes function");
	var attributeTable = {};
	attributeTable.accountCode = "accountCode";
	attributeTable.authorEmailAddress = "authorEmailAddress";
	attributeTable.businessCode = "businessCode";
	attributeTable.contractCode = "contractCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.createdDate = "createdDate";
	attributeTable.propertyAddressLine1 = "propertyAddressLine1";
	attributeTable.propertyAddressLine2 = "propertyAddressLine2";
	attributeTable.propertyAddressLine3 = "propertyAddressLine3";
	attributeTable.propertyAddressLine4 = "propertyAddressLine4";
	attributeTable.propertyAddressLine5 = "propertyAddressLine5";
	attributeTable.propertyContactEmail = "propertyContactEmail";
	attributeTable.propertyContactFax = "propertyContactFax";
	attributeTable.propertyContactMobile = "propertyContactMobile";
	attributeTable.propertyContactName = "propertyContactName";
	attributeTable.propertyContactPosition = "propertyContactPosition";
	attributeTable.propertyContactTelephone = "propertyContactTelephone";
	attributeTable.propertyName = "propertyName";
	attributeTable.propertyPostcode = "propertyPostcode";
	attributeTable.ticketID = "ticketID";
	attributeTable.ticketNotes = "ticketNotes";
	attributeTable.ticketNotesLatest = "ticketNotesLatest";
	attributeTable.ticketNumber = "ticketNumber";
	attributeTable.ticketStatusCode = "ticketStatusCode";
	attributeTable.ticketStatusDescription = "ticketStatusDescription";
	attributeTable.ticketTypeCode = "ticketTypeCode";
	attributeTable.ticketTypeDetailCode = "ticketTypeDetailCode";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.prospectStatusCode = "prospectStatusCode";
	attributeTable.prospectStatusDescription = "prospectStatusDescription";
	attributeTable.convertedContractValue = "convertedContractValue";

	var PKTable = {};
	PKTable.ticketNumber = {}
	PKTable.ticketNumber.name = "ticketNumber";
	PKTable.ticketNumber.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject leadhistory. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject leadhistory. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject leadhistory. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

leadhistory.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering leadhistory.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = leadhistory.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

leadhistory.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering leadhistory.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.accountCode = this.accountCode;
	valuesTable.authorEmailAddress = this.authorEmailAddress;
	valuesTable.businessCode = this.businessCode;
	valuesTable.contractCode = this.contractCode;
	valuesTable.countryCode = this.countryCode;
	valuesTable.createdDate = this.createdDate;
	valuesTable.propertyAddressLine1 = this.propertyAddressLine1;
	valuesTable.propertyAddressLine2 = this.propertyAddressLine2;
	valuesTable.propertyAddressLine3 = this.propertyAddressLine3;
	valuesTable.propertyAddressLine4 = this.propertyAddressLine4;
	valuesTable.propertyAddressLine5 = this.propertyAddressLine5;
	valuesTable.propertyContactEmail = this.propertyContactEmail;
	valuesTable.propertyContactFax = this.propertyContactFax;
	valuesTable.propertyContactMobile = this.propertyContactMobile;
	valuesTable.propertyContactName = this.propertyContactName;
	valuesTable.propertyContactPosition = this.propertyContactPosition;
	valuesTable.propertyContactTelephone = this.propertyContactTelephone;
	valuesTable.propertyName = this.propertyName;
	valuesTable.propertyPostcode = this.propertyPostcode;
	valuesTable.ticketID = this.ticketID;
	valuesTable.ticketNotes = this.ticketNotes;
	valuesTable.ticketNotesLatest = this.ticketNotesLatest;
	if(isInsert===true){
		valuesTable.ticketNumber = this.ticketNumber;
	}
	valuesTable.ticketStatusCode = this.ticketStatusCode;
	valuesTable.ticketStatusDescription = this.ticketStatusDescription;
	valuesTable.ticketTypeCode = this.ticketTypeCode;
	valuesTable.ticketTypeDetailCode = this.ticketTypeDetailCode;
	valuesTable.propertyCode = this.propertyCode;
	valuesTable.prospectStatusCode = this.prospectStatusCode;
	valuesTable.prospectStatusDescription = this.prospectStatusDescription;
	valuesTable.convertedContractValue = this.convertedContractValue;
	return valuesTable;
};

leadhistory.prototype.getPKTable = function(){
	sync.log.trace("Entering leadhistory.prototype.getPKTable function");
	var pkTable = {};
	pkTable.ticketNumber = {key:"ticketNumber",value:this.ticketNumber};
	return pkTable;
};

leadhistory.getPKTable = function(){
	sync.log.trace("Entering leadhistory.getPKTable function");
	var pkTable = [];
	pkTable.push("ticketNumber");
	return pkTable;
};

leadhistory.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering leadhistory.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key ticketNumber not specified in  " + opName + "  an item in leadhistory");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("ticketNumber",opName,"leadhistory")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.ticketNumber)){
			if(!kony.sync.isNull(pks.ticketNumber.value)){
				wc.key = "ticketNumber";
				wc.value = pks.ticketNumber.value;
			}
			else{
				wc.key = "ticketNumber";
				wc.value = pks.ticketNumber;
			}
		}else{
			sync.log.error("Primary Key ticketNumber not specified in  " + opName + "  an item in leadhistory");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("ticketNumber",opName,"leadhistory")));
			return false;
		}
	}
	else{
		wc.key = "ticketNumber";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

leadhistory.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering leadhistory.validateNull function");
	if(valuestable.ticketID!==undefined){
		if(kony.sync.isNull(valuestable.ticketID) || kony.sync.isEmptyString(valuestable.ticketID)){
			sync.log.error("Mandatory attribute ticketID is missing for the SyncObject leadhistory.");
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "leadhistory", "ticketID")));
			return false;
		}
	}
	return true;
};

leadhistory.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering leadhistory.validateNullInsert function");
	if(kony.sync.isNull(valuestable.ticketID) || kony.sync.isEmptyString(valuestable.ticketID)){
		sync.log.error("Mandatory attribute ticketID is missing for the SyncObject leadhistory.");
		errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeMandatoryAttribute,kony.sync.getErrorMessage(kony.sync.errorCodeMandatoryAttribute, "leadhistory", "ticketID")));
		return false;
	}
	return true;
};

leadhistory.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering leadhistory.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


leadhistory.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

leadhistory.getTableName = function(){
	return "leadhistory";
};




// **********************************End leadhistory's helper methods************************