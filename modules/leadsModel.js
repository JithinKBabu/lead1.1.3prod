//****************Sync Version:Sync-Dev-8.0.0_v201711101237_r14*******************
// ****************Generated On Tue Nov 06 16:41:58 UTC 2018leads*******************
// **********************************Start leads's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}




/************************************************************************************
* Creates new leads
*************************************************************************************/
leads = function(){
	this.accountCode = null;
	this.authorEmailAddress = null;
	this.businessCode = null;
	this.callLogReference = null;
	this.contractCode = null;
	this.countryCode = null;
	this.createdDate = null;
	this.createdTime = null;
	this.email = null;
	this.errorCode = null;
	this.errorDescription = null;
	this.errorDescriptionResult = null;
	this.errorNumber = null;
	this.isError = null;
	this.languageCode = null;
	this.leadDeleted = null;
	this.leadId = null;
	this.propertyAddressLine1 = null;
	this.propertyAddressLine2 = null;
	this.propertyAddressLine3 = null;
	this.propertyAddressLine4 = null;
	this.propertyAddressLine5 = null;
	this.propertyCode = null;
	this.propertyContactEmail = null;
	this.propertyContactFax = null;
	this.propertyContactMobile = null;
	this.propertyContactName = null;
	this.propertyContactPosition = null;
	this.propertyContactTelephone = null;
	this.propertyName = null;
	this.propertyPostcode = null;
	this.prospectNumber = null;
	this.sourceApplication = null;
	this.ticketID = null;
	this.ticketNotes = null;
	this.TicketNumber = null;
	this.updateDateTime = null;
	this.markForUpload = true;
};

leads.prototype = {
	get accountCode(){
		return this._accountCode;
	},
	set accountCode(val){
		this._accountCode = val;
	},
	get authorEmailAddress(){
		return this._authorEmailAddress;
	},
	set authorEmailAddress(val){
		this._authorEmailAddress = val;
	},
	get businessCode(){
		return this._businessCode;
	},
	set businessCode(val){
		this._businessCode = val;
	},
	get callLogReference(){
		return this._callLogReference;
	},
	set callLogReference(val){
		this._callLogReference = val;
	},
	get contractCode(){
		return this._contractCode;
	},
	set contractCode(val){
		this._contractCode = val;
	},
	get countryCode(){
		return this._countryCode;
	},
	set countryCode(val){
		this._countryCode = val;
	},
	get createdDate(){
		return this._createdDate;
	},
	set createdDate(val){
		this._createdDate = val;
	},
	get createdTime(){
		return this._createdTime;
	},
	set createdTime(val){
		this._createdTime = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get errorCode(){
		return this._errorCode;
	},
	set errorCode(val){
		this._errorCode = val;
	},
	get errorDescription(){
		return this._errorDescription;
	},
	set errorDescription(val){
		this._errorDescription = val;
	},
	get errorDescriptionResult(){
		return this._errorDescriptionResult;
	},
	set errorDescriptionResult(val){
		this._errorDescriptionResult = val;
	},
	get errorNumber(){
		return this._errorNumber;
	},
	set errorNumber(val){
		this._errorNumber = val;
	},
	get isError(){
		return this._isError;
	},
	set isError(val){
		this._isError = val;
	},
	get languageCode(){
		return this._languageCode;
	},
	set languageCode(val){
		this._languageCode = val;
	},
	get leadDeleted(){
		return kony.sync.getBoolean(this._leadDeleted)+"";
	},
	set leadDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute leadDeleted in leads.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._leadDeleted = val;
	},
	get leadId(){
		return this._leadId;
	},
	set leadId(val){
		this._leadId = val;
	},
	get propertyAddressLine1(){
		return this._propertyAddressLine1;
	},
	set propertyAddressLine1(val){
		this._propertyAddressLine1 = val;
	},
	get propertyAddressLine2(){
		return this._propertyAddressLine2;
	},
	set propertyAddressLine2(val){
		this._propertyAddressLine2 = val;
	},
	get propertyAddressLine3(){
		return this._propertyAddressLine3;
	},
	set propertyAddressLine3(val){
		this._propertyAddressLine3 = val;
	},
	get propertyAddressLine4(){
		return this._propertyAddressLine4;
	},
	set propertyAddressLine4(val){
		this._propertyAddressLine4 = val;
	},
	get propertyAddressLine5(){
		return this._propertyAddressLine5;
	},
	set propertyAddressLine5(val){
		this._propertyAddressLine5 = val;
	},
	get propertyCode(){
		return this._propertyCode;
	},
	set propertyCode(val){
		this._propertyCode = val;
	},
	get propertyContactEmail(){
		return this._propertyContactEmail;
	},
	set propertyContactEmail(val){
		this._propertyContactEmail = val;
	},
	get propertyContactFax(){
		return this._propertyContactFax;
	},
	set propertyContactFax(val){
		this._propertyContactFax = val;
	},
	get propertyContactMobile(){
		return this._propertyContactMobile;
	},
	set propertyContactMobile(val){
		this._propertyContactMobile = val;
	},
	get propertyContactName(){
		return this._propertyContactName;
	},
	set propertyContactName(val){
		this._propertyContactName = val;
	},
	get propertyContactPosition(){
		return this._propertyContactPosition;
	},
	set propertyContactPosition(val){
		this._propertyContactPosition = val;
	},
	get propertyContactTelephone(){
		return this._propertyContactTelephone;
	},
	set propertyContactTelephone(val){
		this._propertyContactTelephone = val;
	},
	get propertyName(){
		return this._propertyName;
	},
	set propertyName(val){
		this._propertyName = val;
	},
	get propertyPostcode(){
		return this._propertyPostcode;
	},
	set propertyPostcode(val){
		this._propertyPostcode = val;
	},
	get prospectNumber(){
		return this._prospectNumber;
	},
	set prospectNumber(val){
		this._prospectNumber = val;
	},
	get sourceApplication(){
		return this._sourceApplication;
	},
	set sourceApplication(val){
		this._sourceApplication = val;
	},
	get ticketID(){
		return this._ticketID;
	},
	set ticketID(val){
		this._ticketID = val;
	},
	get ticketNotes(){
		return this._ticketNotes;
	},
	set ticketNotes(val){
		this._ticketNotes = val;
	},
	get TicketNumber(){
		return this._TicketNumber;
	},
	set TicketNumber(val){
		this._TicketNumber = val;
	},
	get updateDateTime(){
		return this._updateDateTime;
	},
	set updateDateTime(val){
		this._updateDateTime = val;
	},
};

/************************************************************************************
* Retrieves all instances of leads SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "accountCode";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "authorEmailAddress";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* leads.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
leads.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering leads.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	orderByMap = kony.sync.formOrderByClause("leads",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering leads.getAll->successcallback");
		successcallback(leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of leads present in local database.
*************************************************************************************/
leads.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering leads.getAllCount function");
	leads.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of leads using where clause in the local Database
*************************************************************************************/
leads.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering leads.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering leads.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of leads in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leads.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  leads.prototype.create function");
	var valuestable = this.getValuesTable(true);
	leads.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
leads.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  leads.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "leads.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"leads",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  leads.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	var pks = [];
	var errMsg = "";
	
	function createSuccesscallback(res){
		if(res==null || res.length==0){
			var relationshipMap={};  
			relationshipMap = leads.getRelationshipMap(relationshipMap,valuestable);
			kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
		}
		else{
			errMsg = "[" + errMsg + "]";
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
		}
	}
	
	if(kony.sync.enableORMValidations){
		errMsg = "leadId=" + valuestable.leadId;
		pks["leadId"] = {key:"leadId",value:valuestable.leadId};
		leads.getAllDetailsByPK(pks,createSuccesscallback,errorcallback)
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of leads in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].accountCode = "accountCode_0";
*		valuesArray[0].authorEmailAddress = "authorEmailAddress_0";
*		valuesArray[0].businessCode = "businessCode_0";
*		valuesArray[0].callLogReference = "callLogReference_0";
*		valuesArray[0].contractCode = "contractCode_0";
*		valuesArray[0].countryCode = "countryCode_0";
*		valuesArray[0].createdDate = "createdDate_0";
*		valuesArray[0].createdTime = "createdTime_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].errorCode = "errorCode_0";
*		valuesArray[0].errorDescription = "errorDescription_0";
*		valuesArray[0].errorDescriptionResult = "errorDescriptionResult_0";
*		valuesArray[0].errorNumber = "errorNumber_0";
*		valuesArray[0].isError = "isError_0";
*		valuesArray[0].languageCode = "languageCode_0";
*		valuesArray[0].leadDeleted = true;
*		valuesArray[0].leadId = "leadId_0";
*		valuesArray[0].propertyAddressLine1 = "propertyAddressLine1_0";
*		valuesArray[0].propertyAddressLine2 = "propertyAddressLine2_0";
*		valuesArray[0].propertyAddressLine3 = "propertyAddressLine3_0";
*		valuesArray[0].propertyAddressLine4 = "propertyAddressLine4_0";
*		valuesArray[0].propertyAddressLine5 = "propertyAddressLine5_0";
*		valuesArray[0].propertyCode = "propertyCode_0";
*		valuesArray[0].propertyContactEmail = "propertyContactEmail_0";
*		valuesArray[0].propertyContactFax = "propertyContactFax_0";
*		valuesArray[0].propertyContactMobile = "propertyContactMobile_0";
*		valuesArray[0].propertyContactName = "propertyContactName_0";
*		valuesArray[0].propertyContactPosition = "propertyContactPosition_0";
*		valuesArray[0].propertyContactTelephone = "propertyContactTelephone_0";
*		valuesArray[0].propertyName = "propertyName_0";
*		valuesArray[0].propertyPostcode = "propertyPostcode_0";
*		valuesArray[0].prospectNumber = "prospectNumber_0";
*		valuesArray[0].sourceApplication = "sourceApplication_0";
*		valuesArray[0].ticketID = "ticketID_0";
*		valuesArray[0].ticketNotes = "ticketNotes_0";
*		valuesArray[0].TicketNumber = "TicketNumber_0";
*		valuesArray[0].updateDateTime = "updateDateTime_0";
*		valuesArray[1] = {};
*		valuesArray[1].accountCode = "accountCode_1";
*		valuesArray[1].authorEmailAddress = "authorEmailAddress_1";
*		valuesArray[1].businessCode = "businessCode_1";
*		valuesArray[1].callLogReference = "callLogReference_1";
*		valuesArray[1].contractCode = "contractCode_1";
*		valuesArray[1].countryCode = "countryCode_1";
*		valuesArray[1].createdDate = "createdDate_1";
*		valuesArray[1].createdTime = "createdTime_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].errorCode = "errorCode_1";
*		valuesArray[1].errorDescription = "errorDescription_1";
*		valuesArray[1].errorDescriptionResult = "errorDescriptionResult_1";
*		valuesArray[1].errorNumber = "errorNumber_1";
*		valuesArray[1].isError = "isError_1";
*		valuesArray[1].languageCode = "languageCode_1";
*		valuesArray[1].leadDeleted = true;
*		valuesArray[1].leadId = "leadId_1";
*		valuesArray[1].propertyAddressLine1 = "propertyAddressLine1_1";
*		valuesArray[1].propertyAddressLine2 = "propertyAddressLine2_1";
*		valuesArray[1].propertyAddressLine3 = "propertyAddressLine3_1";
*		valuesArray[1].propertyAddressLine4 = "propertyAddressLine4_1";
*		valuesArray[1].propertyAddressLine5 = "propertyAddressLine5_1";
*		valuesArray[1].propertyCode = "propertyCode_1";
*		valuesArray[1].propertyContactEmail = "propertyContactEmail_1";
*		valuesArray[1].propertyContactFax = "propertyContactFax_1";
*		valuesArray[1].propertyContactMobile = "propertyContactMobile_1";
*		valuesArray[1].propertyContactName = "propertyContactName_1";
*		valuesArray[1].propertyContactPosition = "propertyContactPosition_1";
*		valuesArray[1].propertyContactTelephone = "propertyContactTelephone_1";
*		valuesArray[1].propertyName = "propertyName_1";
*		valuesArray[1].propertyPostcode = "propertyPostcode_1";
*		valuesArray[1].prospectNumber = "prospectNumber_1";
*		valuesArray[1].sourceApplication = "sourceApplication_1";
*		valuesArray[1].ticketID = "ticketID_1";
*		valuesArray[1].ticketNotes = "ticketNotes_1";
*		valuesArray[1].TicketNumber = "TicketNumber_1";
*		valuesArray[1].updateDateTime = "updateDateTime_1";
*		valuesArray[2] = {};
*		valuesArray[2].accountCode = "accountCode_2";
*		valuesArray[2].authorEmailAddress = "authorEmailAddress_2";
*		valuesArray[2].businessCode = "businessCode_2";
*		valuesArray[2].callLogReference = "callLogReference_2";
*		valuesArray[2].contractCode = "contractCode_2";
*		valuesArray[2].countryCode = "countryCode_2";
*		valuesArray[2].createdDate = "createdDate_2";
*		valuesArray[2].createdTime = "createdTime_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].errorCode = "errorCode_2";
*		valuesArray[2].errorDescription = "errorDescription_2";
*		valuesArray[2].errorDescriptionResult = "errorDescriptionResult_2";
*		valuesArray[2].errorNumber = "errorNumber_2";
*		valuesArray[2].isError = "isError_2";
*		valuesArray[2].languageCode = "languageCode_2";
*		valuesArray[2].leadDeleted = true;
*		valuesArray[2].leadId = "leadId_2";
*		valuesArray[2].propertyAddressLine1 = "propertyAddressLine1_2";
*		valuesArray[2].propertyAddressLine2 = "propertyAddressLine2_2";
*		valuesArray[2].propertyAddressLine3 = "propertyAddressLine3_2";
*		valuesArray[2].propertyAddressLine4 = "propertyAddressLine4_2";
*		valuesArray[2].propertyAddressLine5 = "propertyAddressLine5_2";
*		valuesArray[2].propertyCode = "propertyCode_2";
*		valuesArray[2].propertyContactEmail = "propertyContactEmail_2";
*		valuesArray[2].propertyContactFax = "propertyContactFax_2";
*		valuesArray[2].propertyContactMobile = "propertyContactMobile_2";
*		valuesArray[2].propertyContactName = "propertyContactName_2";
*		valuesArray[2].propertyContactPosition = "propertyContactPosition_2";
*		valuesArray[2].propertyContactTelephone = "propertyContactTelephone_2";
*		valuesArray[2].propertyName = "propertyName_2";
*		valuesArray[2].propertyPostcode = "propertyPostcode_2";
*		valuesArray[2].prospectNumber = "prospectNumber_2";
*		valuesArray[2].sourceApplication = "sourceApplication_2";
*		valuesArray[2].ticketID = "ticketID_2";
*		valuesArray[2].ticketNotes = "ticketNotes_2";
*		valuesArray[2].TicketNumber = "TicketNumber_2";
*		valuesArray[2].updateDateTime = "updateDateTime_2";
*		leads.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
leads.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering leads.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"leads",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var isDuplicateKey = false;
		//checking for duplicate records
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkDuplicatePkCallback, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
		function checkDuplicatePkCallback(tx){
			arrayLength = valuesArray.length;
			for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
				var pks = [];
				errMsg = "leadId=" + valuestable.leadId;
				pks["leadId"] = {key:"leadId",value:valuestable.leadId};
				var wcs = [];
				if(leads.pkCheck(pks,wcs,errorcallback,"searching")===false){
					isError = true;
					return;
				}
				var query = kony.sync.qb_createQuery();
							kony.sync.qb_select(query, null);
							kony.sync.qb_from(query, tbname);
							kony.sync.qb_where(query, wcs);
				var query_compile = kony.sync.qb_compile(query);
				var sql = query_compile[0];
				var params = query_compile[1];
				var resultset = kony.sync.executeSql(tx, sql, params);
				if(resultset===false){
					isError = true;
					return;
				}
				if(resultset.rows.length!=0){
					isError = true;
					errMsg = "[" + errMsg + "]";
					isDuplicateKey = true;
					return;
				}
			}
			if(!isError){
				checkIntegrity(tx);
			}
		}
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  leads.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
			if(isDuplicateKey){
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeDuplicatePrimaryKey,kony.sync.getErrorMessage(kony.sync.errorCodeDuplicatePrimaryKey, tbname, errMsg)));
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  leads.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = leads.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates leads using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leads.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  leads.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	leads.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
leads.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  leads.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(leads.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"leads",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = leads.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates leads(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
leads.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering leads.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"leads",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  leads.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, leads.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = leads.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, leads.getPKTable());
	}
};

/************************************************************************************
* Updates leads(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.accountCode = "accountCode_updated0";
*		inputArray[0].changeSet.authorEmailAddress = "authorEmailAddress_updated0";
*		inputArray[0].changeSet.businessCode = "businessCode_updated0";
*		inputArray[0].changeSet.callLogReference = "callLogReference_updated0";
*		inputArray[0].whereClause = "where leadId = '0'";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.accountCode = "accountCode_updated1";
*		inputArray[1].changeSet.authorEmailAddress = "authorEmailAddress_updated1";
*		inputArray[1].changeSet.businessCode = "businessCode_updated1";
*		inputArray[1].changeSet.callLogReference = "callLogReference_updated1";
*		inputArray[1].whereClause = "where leadId = '1'";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.accountCode = "accountCode_updated2";
*		inputArray[2].changeSet.authorEmailAddress = "authorEmailAddress_updated2";
*		inputArray[2].changeSet.businessCode = "businessCode_updated2";
*		inputArray[2].changeSet.callLogReference = "callLogReference_updated2";
*		inputArray[2].whereClause = "where leadId = '2'";
*		leads.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
leads.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering leads.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898cf65f1fb";
	var tbname = "leads";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"leads",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, leads.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  leads.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, leads.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  leads.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = leads.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes leads using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
leads.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leads.prototype.deleteByPK function");
	var pks = this.getPKTable();
	leads.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
leads.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering leads.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(leads.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function leadsTransactionCallback(tx){
		sync.log.trace("Entering leads.deleteByPK->leads_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function leadsErrorCallback(){
		sync.log.error("Entering leads.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function leadsSuccessCallback(){
		sync.log.trace("Entering leads.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leads.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, leadsTransactionCallback, leadsSuccessCallback, leadsErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes leads(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. leads.remove("where accountCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
leads.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering leads.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;
	var record = "";

	function leads_removeTransactioncallback(tx){
			wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function leads_removeSuccess(){
		sync.log.trace("Entering leads.remove->leads_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering leads.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering leads.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, leads_removeTransactioncallback, leads_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes leads using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
leads.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leads.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	leads.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
leads.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering leads.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(leads.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function leadsTransactionCallback(tx){
		sync.log.trace("Entering leads.removeDeviceInstanceByPK -> leadsTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function leadsErrorCallback(){
		sync.log.error("Entering leads.removeDeviceInstanceByPK -> leadsErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function leadsSuccessCallback(){
		sync.log.trace("Entering leads.removeDeviceInstanceByPK -> leadsSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leads.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, leadsTransactionCallback, leadsSuccessCallback, leadsErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes leads(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
leads.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leads.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function leads_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function leads_removeSuccess(){
		sync.log.trace("Entering leads.remove->leads_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering leads.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering leads.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, leads_removeTransactioncallback, leads_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves leads using primary key from the local Database. 
*************************************************************************************/
leads.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leads.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	leads.getAllDetailsByPK(pks,successcallback,errorcallback);
};
leads.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering leads.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var wcs = [];
	if(leads.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering leads.getAllDetailsByPK-> success callback function");
		successcallback(leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves leads(s) using where clause from the local Database. 
* e.g. leads.find("where accountCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
leads.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leads.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of leads with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
leads.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering leads.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	leads.markForUploadbyPK(pks, successcallback, errorcallback);
};
leads.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leads.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(leads.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of leads matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
leads.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leads.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering leads.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering leads.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering leads.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of leads pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
leads.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering leads.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of leads pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
leads.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering leads.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of leads deferred for upload.
*************************************************************************************/
leads.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering leads.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, leads.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to leads in local database to last synced state
*************************************************************************************/
leads.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering leads.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to leads's record with given primary key in local 
* database to last synced state
*************************************************************************************/
leads.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering leads.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	leads.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
leads.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leads.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var wcs = [];
	if(leads.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering leads.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether leads's record  
* with given primary key got deferred in last sync
*************************************************************************************/
leads.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  leads.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	leads.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
leads.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leads.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var wcs = [] ;
	var flag;
	if(leads.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether leads's record  
* with given primary key is pending for upload
*************************************************************************************/
leads.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  leads.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	leads.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
leads.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering leads.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "leads.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = leads.getTableName();
	var wcs = [] ;
	var flag;
	if(leads.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering leads.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
leads.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering leads.removeCascade function");
	var tbname = leads.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


leads.convertTableToObject = function(res){
	sync.log.trace("Entering leads.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new leads();
			obj.accountCode = res[i].accountCode;
			obj.authorEmailAddress = res[i].authorEmailAddress;
			obj.businessCode = res[i].businessCode;
			obj.callLogReference = res[i].callLogReference;
			obj.contractCode = res[i].contractCode;
			obj.countryCode = res[i].countryCode;
			obj.createdDate = res[i].createdDate;
			obj.createdTime = res[i].createdTime;
			obj.email = res[i].email;
			obj.errorCode = res[i].errorCode;
			obj.errorDescription = res[i].errorDescription;
			obj.errorDescriptionResult = res[i].errorDescriptionResult;
			obj.errorNumber = res[i].errorNumber;
			obj.isError = res[i].isError;
			obj.languageCode = res[i].languageCode;
			obj.leadDeleted = res[i].leadDeleted;
			obj.leadId = res[i].leadId;
			obj.propertyAddressLine1 = res[i].propertyAddressLine1;
			obj.propertyAddressLine2 = res[i].propertyAddressLine2;
			obj.propertyAddressLine3 = res[i].propertyAddressLine3;
			obj.propertyAddressLine4 = res[i].propertyAddressLine4;
			obj.propertyAddressLine5 = res[i].propertyAddressLine5;
			obj.propertyCode = res[i].propertyCode;
			obj.propertyContactEmail = res[i].propertyContactEmail;
			obj.propertyContactFax = res[i].propertyContactFax;
			obj.propertyContactMobile = res[i].propertyContactMobile;
			obj.propertyContactName = res[i].propertyContactName;
			obj.propertyContactPosition = res[i].propertyContactPosition;
			obj.propertyContactTelephone = res[i].propertyContactTelephone;
			obj.propertyName = res[i].propertyName;
			obj.propertyPostcode = res[i].propertyPostcode;
			obj.prospectNumber = res[i].prospectNumber;
			obj.sourceApplication = res[i].sourceApplication;
			obj.ticketID = res[i].ticketID;
			obj.ticketNotes = res[i].ticketNotes;
			obj.TicketNumber = res[i].TicketNumber;
			obj.updateDateTime = res[i].updateDateTime;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

leads.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering leads.filterAttributes function");
	var attributeTable = {};
	attributeTable.accountCode = "accountCode";
	attributeTable.authorEmailAddress = "authorEmailAddress";
	attributeTable.businessCode = "businessCode";
	attributeTable.callLogReference = "callLogReference";
	attributeTable.contractCode = "contractCode";
	attributeTable.countryCode = "countryCode";
	attributeTable.createdDate = "createdDate";
	attributeTable.createdTime = "createdTime";
	attributeTable.email = "email";
	attributeTable.errorCode = "errorCode";
	attributeTable.errorDescription = "errorDescription";
	attributeTable.errorDescriptionResult = "errorDescriptionResult";
	attributeTable.errorNumber = "errorNumber";
	attributeTable.isError = "isError";
	attributeTable.languageCode = "languageCode";
	attributeTable.leadDeleted = "leadDeleted";
	attributeTable.leadId = "leadId";
	attributeTable.propertyAddressLine1 = "propertyAddressLine1";
	attributeTable.propertyAddressLine2 = "propertyAddressLine2";
	attributeTable.propertyAddressLine3 = "propertyAddressLine3";
	attributeTable.propertyAddressLine4 = "propertyAddressLine4";
	attributeTable.propertyAddressLine5 = "propertyAddressLine5";
	attributeTable.propertyCode = "propertyCode";
	attributeTable.propertyContactEmail = "propertyContactEmail";
	attributeTable.propertyContactFax = "propertyContactFax";
	attributeTable.propertyContactMobile = "propertyContactMobile";
	attributeTable.propertyContactName = "propertyContactName";
	attributeTable.propertyContactPosition = "propertyContactPosition";
	attributeTable.propertyContactTelephone = "propertyContactTelephone";
	attributeTable.propertyName = "propertyName";
	attributeTable.propertyPostcode = "propertyPostcode";
	attributeTable.prospectNumber = "prospectNumber";
	attributeTable.sourceApplication = "sourceApplication";
	attributeTable.ticketID = "ticketID";
	attributeTable.ticketNotes = "ticketNotes";
	attributeTable.TicketNumber = "TicketNumber";
	attributeTable.updateDateTime = "updateDateTime";

	var PKTable = {};
	PKTable.leadId = {}
	PKTable.leadId.name = "leadId";
	PKTable.leadId.isAutoGen = false;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject leads. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject leads. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject leads. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

leads.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering leads.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = leads.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

leads.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering leads.prototype.getValuesTable function");
	var valuesTable = {};
	valuesTable.accountCode = this.accountCode;
	valuesTable.authorEmailAddress = this.authorEmailAddress;
	valuesTable.businessCode = this.businessCode;
	valuesTable.callLogReference = this.callLogReference;
	valuesTable.contractCode = this.contractCode;
	valuesTable.countryCode = this.countryCode;
	valuesTable.createdDate = this.createdDate;
	valuesTable.createdTime = this.createdTime;
	valuesTable.email = this.email;
	valuesTable.errorCode = this.errorCode;
	valuesTable.errorDescription = this.errorDescription;
	valuesTable.errorDescriptionResult = this.errorDescriptionResult;
	valuesTable.errorNumber = this.errorNumber;
	valuesTable.isError = this.isError;
	valuesTable.languageCode = this.languageCode;
	valuesTable.leadDeleted = this.leadDeleted;
	if(isInsert===true){
		valuesTable.leadId = this.leadId;
	}
	valuesTable.propertyAddressLine1 = this.propertyAddressLine1;
	valuesTable.propertyAddressLine2 = this.propertyAddressLine2;
	valuesTable.propertyAddressLine3 = this.propertyAddressLine3;
	valuesTable.propertyAddressLine4 = this.propertyAddressLine4;
	valuesTable.propertyAddressLine5 = this.propertyAddressLine5;
	valuesTable.propertyCode = this.propertyCode;
	valuesTable.propertyContactEmail = this.propertyContactEmail;
	valuesTable.propertyContactFax = this.propertyContactFax;
	valuesTable.propertyContactMobile = this.propertyContactMobile;
	valuesTable.propertyContactName = this.propertyContactName;
	valuesTable.propertyContactPosition = this.propertyContactPosition;
	valuesTable.propertyContactTelephone = this.propertyContactTelephone;
	valuesTable.propertyName = this.propertyName;
	valuesTable.propertyPostcode = this.propertyPostcode;
	valuesTable.prospectNumber = this.prospectNumber;
	valuesTable.sourceApplication = this.sourceApplication;
	valuesTable.ticketID = this.ticketID;
	valuesTable.ticketNotes = this.ticketNotes;
	valuesTable.TicketNumber = this.TicketNumber;
	valuesTable.updateDateTime = this.updateDateTime;
	return valuesTable;
};

leads.prototype.getPKTable = function(){
	sync.log.trace("Entering leads.prototype.getPKTable function");
	var pkTable = {};
	pkTable.leadId = {key:"leadId",value:this.leadId};
	return pkTable;
};

leads.getPKTable = function(){
	sync.log.trace("Entering leads.getPKTable function");
	var pkTable = [];
	pkTable.push("leadId");
	return pkTable;
};

leads.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering leads.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key leadId not specified in  " + opName + "  an item in leads");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("leadId",opName,"leads")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.leadId)){
			if(!kony.sync.isNull(pks.leadId.value)){
				wc.key = "leadId";
				wc.value = pks.leadId.value;
			}
			else{
				wc.key = "leadId";
				wc.value = pks.leadId;
			}
		}else{
			sync.log.error("Primary Key leadId not specified in  " + opName + "  an item in leads");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("leadId",opName,"leads")));
			return false;
		}
	}
	else{
		wc.key = "leadId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

leads.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering leads.validateNull function");
	return true;
};

leads.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering leads.validateNullInsert function");
	return true;
};

leads.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering leads.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


leads.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

leads.getTableName = function(){
	return "leads";
};




// **********************************End leads's helper methods************************