/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Popup related methods
***********************************************************************/

/** Popup for displaying warning amd error and messages **/
function showMessagePopup(msgKey, buttonHandlerAction){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= getI18nString(msgKey);
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
  	if (buttonHandlerAction !== null){
      	PopupMessage.btnOk.onClick	= buttonHandlerAction;
    } else {
      	PopupMessage.btnOk.onClick	= ActionPopupMessageOk;
    }
    PopupMessage.show();  
}

/** Message Popup - Dismiss **/
function dismissMessagePopup(){
  //alert(1212);
  	PopupMessage.dismiss(); 
  	setTextBoxFocus(gblFocusTextBox);
}

function dismissAlertMessagePopup(){
  	PopupAlertMsg.dismiss(); 
}

/** Message Popup - Dismiss **/
function dismissEmailErrorPopup(){
  	dismissLoading();
  	PopupChangeEmail.dismiss();
  	exitApplication();
}

/** Confirm popup - show **/
function showConfirmPopup(msgKey, yesButtonHandlerAction, noButtonHandlerAction){
  	PopupConfirmation.lblTitle.text		= getI18nString("common.app.title");
  	PopupConfirmation.lblMessage.text	= getI18nString(msgKey);
    PopupConfirmation.btnNo.text		= getI18nString("common.label.no");
  	PopupConfirmation.btnYes.text		= getI18nString("common.label.yes");
    PopupConfirmation.btnYes.onClick	= yesButtonHandlerAction;
  	if (noButtonHandlerAction !== null){
      	PopupConfirmation.btnNo.onClick	= noButtonHandlerAction;
    } else {
      	PopupConfirmation.btnNo.onClick	= ActionConfirmPopupNo;
    }
    PopupConfirmation.show();  
}

/** Confirm popup - Dismiss **/
function dismissConfirmPopup(){
  	PopupConfirmation.dismiss(); 
}

/** On click message popup Ok button action - This will exit application **/
function onClickMessagePopupOkButton(){
  	showLoading("common.message.1001");
  	dismissMessagePopup();
  	dismissLoading();
  	exitApplication();
}

/** Progress popup - show **/
function showProgressPopup(titleKey, messageStr, progressKey){
    PopupProgress.lblMessage.text	= getI18nString(messageStr);
  	PopupProgress.lblProgress.text	= getI18nString(progressKey);
  	if (gblIsProgressPopupOpen === false) {
      	gblIsProgressPopupOpen		= true;
      	PopupProgress.lblTitle.text	= getI18nString(titleKey);
      	PopupProgress.show();
      	kony.timer.schedule("ToggleProgressImageTimer", toggleProgressImage, 0.5, true);
    } 
}

/** Progress popup - to toggle progress indicator image **/
function toggleProgressImage(){
  	PopupProgress.imgLoading.src	= (gblProgressIndicatorFlag === true) ? "loadingone.png" : "loadingtwo.png" ; 
  	gblProgressIndicatorFlag		= !gblProgressIndicatorFlag;
}

/** Progress popup - update message **/
function updateMessageInProgressPopup(messageStr){
  	 PopupProgress.lblMessage.text	= getI18nString(messageStr);
}

/** Progress popup - update progress **/
function updateProgressInProgressPopup(progressKey){
  	 PopupProgress.lblProgress.text	= getI18nString(progressKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	gblIsProgressPopupOpen			= false;
  	kony.timer.cancel("ToggleProgressImageTimer"); 
  	PopupProgress.dismiss();
}



/** Confirm popup (exit app) - Yes btn action **/
function onClickConfirmPopupYesBtn(){
  	dismissConfirmPopup();
  	exitApplication();
}

/** Confirm popup (exit app) - No btn action **/
function onClickConfirmPopupNoBtn(){
  	dismissConfirmPopup();
}

