/*** Author  : RI - CTS - 363601 ****/

function invokeIntegrationService(params){
  	// Description : success call back for upload service 
    this.uploadSuccessCallBack							= function(response) {
        printMessage("Inside uploadService - uploadSuccessCallBack: "+JSON.stringify(response));
      var uploadedRecord							= null;
      var errorNumber ="";
      var isLastIndex = false;
      
      printMessage("invokeIntegrationService::: gblUploadTicket.type "+gblUploadTicket.type);
      if (!isEmpty(response)){

        if(gblUploadTicket.type === "LEAD"){ 
          if (!isEmpty(response.leadCollection) && response.leadCollection.length > 0) {

            for (var i = 0; i < response.leadCollection.length; i++) {
              errorNumber = response.leadCollection[i].errorNumber;
              printMessage("Inside uploadService - errorNumber: "+(parseInt(errorNumber) ===0));
              //Call
              if(i === response.leadCollection.length -1){
                isLastIndex = true;
              }
              validateErrorNumber(errorNumber,response.leadCollection[i],isLastIndex);
            }
          }else {
            printMessage("Inside uploadService - uploadSuccessCallBack: Successfully uploaded records but response having empty leadCollection");

            // Change logic
            if(!isEmpty(response.errorNumber)){
              errorNumber = response.errorNumber;
              printMessage("Inside uploadService - uploadSuccessCallBack: errorCode 44442 " + errorNumber);
              //validateErrorNumber(errorNumber,response,true);
              gblUploadTicket.isLeadServiceFinished	= true;
              insertErrorRecordsIntoDB(errorNumber);
              printMessage("Inside uploadService response having empty leadCollection ::: gblIsCheckESBError"+gblIsCheckESBError);
              updateIntegrationServiceCallFlag();
              if(gblIsCheckESBError === true){
                 constructAlertsErrorsInputForSyncUpload();
              }else{
                uploadAlertRecords();
              }
           } 
          }
        }else{
          printMessage("Inside uploadService - alert ");

          if (!isEmpty(response.ticketStatus) && response.ticketStatus.length > 0) {
            for (var j = 0; j < response.ticketStatus.length; j++) {
              errorNumber = response.ticketStatus[j].errorNumber;
              printMessage("Inside uploadService - errorNumber "+(parseInt(errorNumber) ===0));
              //call
              if(j === response.ticketStatus.length -1){
                isLastIndex = true;
              }
              validateErrorNumber(errorNumber,response.ticketStatus[j],isLastIndex);
            }
          } else {
            printMessage("Inside uploadService - uploadSuccessCallBack: Successfully uploaded records but response having empty ticketStatus");
            // Change logic
            if(!isEmpty(response.errorNumber)){
              errorNumber = response.errorNumber;
              printMessage("Inside uploadService - uploadSuccessCallBack: errorCode 44443 " + errorNumber);
             // validateErrorNumber(errorNumber,response,true);
              insertErrorRecordsIntoDB(errorNumber);
              //call ESB lead errors
              gblUploadTicket.isAlertServiceFinished	= true;
              updateIntegrationServiceCallFlag();
              printMessage("Inside uploadService response having empty AlertCollection ::: gblIsCheckESBError"+gblIsCheckESBError);
              if(gblIsCheckESBError === true){
                 constructLeadsErrorInputForSyncUpload();
              }
            } 
          }
        }
      }
    };

    // Description : failure call back for upload service 
    this.uploadFailureCallBack	= function(error) {
      printMessage("Inside uploadService - uploadFailureCallBack: " + JSON.stringify(error));
      var errorNumber ="";

      try{
        if(!isEmpty(error)) {
          if(!isEmpty(error.httpStatusCode))
            errorNumber = error.httpStatusCode;
        }  else if(!isEmpty(error.httpresponse)){
          errorNumber=error.httpresponse.responsecode;
        }
        gblErrorNumber = errorNumber;
      }catch(err){
        printMessage("uploadFailureCallBack:::Exception "+JSON.stringify(err));
      }

      if(gblServerCodes.indexOf(gblErrorNumber) === -1){
        //insert into LAError and delete from leads/alerts table
        if (gblUploadTicket.type === "LEAD"){
          gblUploadTicket.isLeadServiceFinished		= true;
          //gblLeadError 					        	= true;
          updateIntegrationServiceCallFlag();
          insertErrorRecordsIntoDB(gblErrorNumber);
          // uploadAlertRecords();
          if(gblIsCheckESBError === true){
            constructAlertsErrorsInputForSyncUpload();
          }else{
            uploadAlertRecords();
          }
        } else {
          gblUploadTicket.isAlertServiceFinished		= true;
          updateIntegrationServiceCallFlag();
          insertErrorRecordsIntoDB(gblErrorNumber);
          if(gblIsCheckESBError === true){
            constructLeadsErrorInputForSyncUpload();
          }
        }
      }else{
        //Retry records
        printMessage("Retry alerts service using ScheduleTask:: ");
      }
      
      //Update updateIntegrationServiceCallFlag
      updateIntegrationServiceCallFlag();
    };

    var integrationClient 			= null;
    var serviceName 				= (gblUploadTicket.type === "LEAD") ? "LALeads" : "LAAlerts";
    var operationName				= (gblUploadTicket.type === "LEAD") ? "CreateLead" : "CreateAlert";
    var headers						= null;

    try {
      printMessage("Inside uploadService - serviceName: " + serviceName);
      printMessage("Inside uploadService - operationName: " + operationName);
      printMessage("Inside uploadService - params: " + JSON.stringify(params));
      integrationClient 			= KNYMobileFabric.getIntegrationService(serviceName);
      integrationClient.invokeOperation(operationName, headers, params, this.uploadSuccessCallBack, this.uploadFailureCallBack);
    } catch(exception){
      printMessage("Inside uploadService - Exception: " + exception.message);
    }
}

/**  Description : To uploade locally created lead records **/
function uploadLeadRecords() {
  	printMessage("Inside uploadLeadRecords - gblLeadsCollectionForScheduler.length: " + gblLeadsCollectionForScheduler.length);
  	gblIsCheckESBError 					= false;
  	gblIsAutoSendESBError				= false;
  	printMessage("Inside uploadLeadRecords - gblIsCheckESBError: " + gblIsCheckESBError);
  	if (!isEmpty(gblLeadsCollectionForScheduler) && gblLeadsCollectionForScheduler.length > 0) {
        gblIsAutoSendESBError 			= true;
      	gblUploadTicket.type			= "LEAD";
      	var params						= {};
        //params.client_id 				= LAConstants.INTEGRATION_CLIENT_ID;
   	    //params.client_secret 			= LAConstants.INTEGRATION_CLIENT_SECRET;
  		params.businessCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  		params.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
      	params.email 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  		params.languageCode				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
		//params.lead 					= encodeURI(JSON.stringify(gblLeadsCollectionForScheduler));
      	params.lead 					= gblLeadsCollectionForScheduler;
      	params.sourceApplication 		= "LAAS";
      if (isNetworkAvailable() === true) {
      	var anIntegrationClient			= new invokeIntegrationService(params);  
      }else{
        gblUploadTicket.isLeadServiceFinished 		= true;
		updateIntegrationServiceCallFlag();
      }
    } else {
         printMessage("Inside uploadLeadRecords else");
      	gblUploadTicket.isLeadServiceFinished	= true;
      	gblUploadTicket.succeededLeadsIds		= [];
      	uploadAlertRecords();
    }
}

/**  Description : To uploade locally created alert records (isCheckESBError )**/
function uploadAlertRecords() {
  printMessage("Inside uploadAlertRecords - gblAlertsCollectionForScheduler.length: " + gblAlertsCollectionForScheduler.length);
  gblIsCheckESBError 					= true;

  printMessage("Inside uploadAlertRecords - gblIsCheckESBError: " + gblIsCheckESBError);
  
  	if (!isEmpty(gblAlertsCollectionForScheduler) && gblAlertsCollectionForScheduler.length > 0) {
      
      	gblUploadTicket.type			= "ALERT";
      	var params						= {};
      	params.client_id 				= LAConstants.INTEGRATION_CLIENT_ID;
   		params.client_secret 			= LAConstants.INTEGRATION_CLIENT_SECRET;
  		params.businessCode 			= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  		params.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  		params.email 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  		params.languageCode				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
		//params.alert 					= encodeURI(JSON.stringify(gblAlertsCollectionForScheduler));
      	params.alert 					= gblAlertsCollectionForScheduler;
      	params.sourceApplication 		= "LAAS";
      if (isNetworkAvailable() === true) {
      	var anIntegrationClient			= new invokeIntegrationService(params);  
      }else{
        gblUploadTicket.isAlertServiceFinished 	= true;
		updateIntegrationServiceCallFlag();
      }
    } else {
        printMessage("Inside uploadAlertRecords else");
      	gblUploadTicket.isAlertServiceFinished	= true;
      	gblUploadTicket.succeededAlertsIds		= [];
      	//initiate15MinSyncCall();
      	updateIntegrationServiceCallFlag();
      if(gblIsAutoSendESBError){
        //Check Auto resync only if lead/alert is created
      	constructLeadsErrorInputForSyncUpload();
      }
    }
}

/**  Description : To update integration service flag **/
function updateIntegrationServiceCallFlag() {
  printMessage("Inside updateIntegrationServiceCallFlag ");
  printMessage("Inside updateIntegrationServiceCallFlag  gblLeadError"+gblLeadError);
  printMessage("Inside updateIntegrationServiceCallFlag - gblUploadTicket.isLeadServiceFinished: " + gblUploadTicket.isLeadServiceFinished);
  printMessage("Inside updateIntegrationServiceCallFlag - gblUploadTicket.isAlertServiceFinished: " + gblUploadTicket.isAlertServiceFinished);
  //	 	if (gblUploadTicket.isLeadServiceFinished === true && !isEmpty(gblLeadsCollectionForScheduler) && gblLeadsCollectionForScheduler.length > 0 && gblLeadError ===false) {
  if (gblUploadTicket.isLeadServiceFinished === true && !isEmpty(gblLeadsCollectionForScheduler) && gblLeadsCollectionForScheduler.length > 0 ) {
    printMessage("Inside updateIntegrationServiceCallFlag gblLeadError"+gblLeadError);
   // deleteLeadsFromLocalTbl(); 
  }

  printMessage("Inside updateIntegrationServiceCallFlag gblAlertError"+gblAlertError);
  //   if (gblUploadTicket.isAlertServiceFinished === true && !isEmpty(gblAlertsCollectionForScheduler) && gblAlertsCollectionForScheduler.length > 0 && gblAlertError ===false) {
  if (gblUploadTicket.isAlertServiceFinished === true && !isEmpty(gblAlertsCollectionForScheduler) && gblAlertsCollectionForScheduler.length > 0 ) {
    printMessage("inside updateIntegrationServiceCallFlag here sending to delete -->>Inside updateIntegrationServiceCallFlag ");	
    //deleteAlertsFromLocalTbl();      
  }
  //LAA57
  //if (gblUploadTicket.isLeadServiceFinished === true && gblUploadTicket.isAlertServiceFinished === true) {
  if (gblUploadTicket.isLeadServiceFinished === true || gblUploadTicket.isAlertServiceFinished === true) {
    var lastUploadSyncDateTime 					= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
    printMessage("Inside updateIntegrationServiceCallFlag lastUploadSyncDateTime"+lastUploadSyncDateTime);	
    //kony.store.setItem(LAConstants.LAST_UPLOAD_SYNC_DATE_TIME, lastUploadSyncDateTime);	
    gblIsIntServiceInProgress					= false;
    printMessage("Inside updateIntegrationServiceCallFlag finally - gblIsIntServiceInProgress: " + gblIsIntServiceInProgress);
  } 

}

function validateErrorNumber(errorNumberValue,responseData,isLastCollectionIndex){
  var uploadedRecord							= null;
  

  printMessage("validateErrorNumber::: gblUploadTicket.type "+gblUploadTicket.type);
  //printMessage("validateErrorNumber lead errorNumber value is  "+parseInt(errorNumberValue) ===0);
  try{
   	   gblErrorNumber = errorNumberValue;
    if(parseInt(errorNumberValue) ===0){

      if(gblUploadTicket.type === "LEAD"){ 
        printMessage("validateErrorNumber lead errorNumber is zero");
        gblLeadError 					        = false;
        printMessage("validateErrorNumber before uploadedRecord "+JSON.stringify(responseData));
        uploadedRecord							= responseData;
        printMessage("validateErrorNumber after uploadedRecord "+JSON.stringify(responseData));
        gblUploadTicket.succeededLeadsIds.push(uploadedRecord.leadId);
        printMessage("validateErrorNumber  uploadedRecord.leadId ");
        //Upload Alerts
        //uploadAlertRecords();
        printMessage("validateErrorNumber Inside uploadService - Leads uploadSuccessCallBack: Successfully uploaded records ");
       
        printMessage("responseData.leadId:: "+responseData.leadId);
        deleteRecordUsingSelectedId(responseData.leadId);
        
         //Calling Upload alert after Success and Failure leads (Add in three condition, if collection length is inxed value  )
        if(isLastCollectionIndex){
             //uploadAlertRecords();
          gblUploadTicket.isLeadServiceFinished	= true;
           updateIntegrationServiceCallFlag();
          if(gblIsCheckESBError === true){
            constructAlertsErrorsInputForSyncUpload();
          }else{
            uploadAlertRecords();
          }
        }
      }else{

        printMessage("validateErrorNumber alert errorNumber is zero");
        gblAlertError 					        = false;
       // gblUploadTicket.isAlertServiceFinished	= true;
        uploadedRecord					= responseData;
        gblUploadTicket.succeededAlertsIds.push(uploadedRecord.alertId);
        printMessage("Inside uploadService - alert uploadSuccessCallBack: Successfully uploaded records ");
        
        printMessage("responseData.alert id:: "+responseData.alertId);
        deleteRecordUsingSelectedId(responseData.alertId);
        if(isLastCollectionIndex){
          gblUploadTicket.isAlertServiceFinished	= true;
          updateIntegrationServiceCallFlag();
          if(gblIsCheckESBError === true){
            constructLeadsErrorInputForSyncUpload();
          }
        }
      }
    }else if(parseInt(errorNumberValue) > 20000){
      if(gblUploadTicket.type === "LEAD"){ 
        printMessage("validateErrorNumber lead errorNumber > zero");
        //errorNumber > 20000
        //gblUploadTicket.isLeadServiceFinished	= true;
        // if its error avaoiding the deletion from local db
        gblLeadError 					        = true;
        //logUploadSupportSyncErrors(errorNumberValue, "Lead", "ServiceError", gblLeadsCollectionForScheduler);
        printMessage("responseData.leadId:: "+responseData.leadId);
        getServiceFailedRecordFromTable(responseData.leadId);
        //Upload Alerts
        //uploadAlertRecords();
        //Calling Upload alert after Success and Failure leads (Add in three condition, if collection length is inxed value  )
        if(isLastCollectionIndex){
            // uploadAlertRecords();
           gblUploadTicket.isLeadServiceFinished	= true;
          updateIntegrationServiceCallFlag();
          if(gblIsCheckESBError === true){
            constructAlertsErrorsInputForSyncUpload();
          }else{
            uploadAlertRecords();
          }
        }
      }else{
        //errorNumber > 20000
        printMessage("validateErrorNumber alert errorNumber > zero");
        //gblUploadTicket.isAlertServiceFinished	= true;
        gblAlertError 					        = true;
        //logUploadSupportSyncErrors(errorNumberValue, "Alert", "ServiceError", gblAlertsCollectionForScheduler);
         printMessage("responseData.alertId:: "+responseData.alertId);
         getServiceFailedRecordFromTable(responseData.alertId);
        if(isLastCollectionIndex){
          gblUploadTicket.isAlertServiceFinished	= true;
          updateIntegrationServiceCallFlag();
          if(gblIsCheckESBError === true){
            constructLeadsErrorInputForSyncUpload();
          }
        }
      }
    }else{
      if(gblUploadTicket.type === "LEAD"){ 
        printMessage("Inside uploadService - lead uploadSuccessCallBack: ESB layer throws an error");
        gblUploadTicket.isLeadServiceFinished	= true;
        gblLeadError 					        = true;
        updateIntegrationServiceCallFlag();
        if(gblServerCodes.indexOf(gblErrorNumber) === -1){
          if (gblUploadTicket.type === "LEAD"){
            insertErrorRecordsIntoDB(gblErrorNumber);
            //uploadAlertRecords();
            if(gblIsCheckESBError === true){
              constructAlertsErrorsInputForSyncUpload();
            }else{
              uploadAlertRecords();
            }
          } 
        }else{
          //Retry records
          printMessage("Retry alerts service using ScheduleTask:: ");
        }
      }else{
        printMessage("Inside uploadService - alert uploadSuccessCallBack: ESB layer throws an error");
        gblUploadTicket.isAlertServiceFinished	= true;
        gblAlertError 					        = true;
        updateIntegrationServiceCallFlag();
        if(gblServerCodes.indexOf(gblErrorNumber) === -1){
          //insert into LAError and delete from leads/alerts table
           insertErrorRecordsIntoDB(gblErrorNumber);
          if(gblIsCheckESBError === true){
            constructLeadsErrorInputForSyncUpload();
          }
           //getServiceFailedRecordFromTable(responseData.alertId);
        }else{
          //Retry records
          printMessage("Retry alerts service using ScheduleTask:: ");
        }
      }
    }
    
    /*//Calling Upload alert after Success and Failure leads (Add in three condition, if collection length is inxed value  )
   	if(isLastCollectionIndex){
      if(gblUploadTicket.type === "LEAD"){ 
         uploadAlertRecords();
      }
    }*/
  }catch(err){
    printMessage("validateErrorNumber err "+JSON.stringify(err));
  }
}
/** Get failure record from leads/alerts table based on selected id  */
function getServiceFailedRecordFromTable(failureleadId){
  
  var sqlQuerySmt = "Select * from leads where leadId = '"+failureleadId+"'";
    if(gblUploadTicket.type !== "LEAD"){ //alerts where alertId
      sqlQuerySmt = "Select * from alerts where alertId = '"+failureleadId+"'";
  }
  printMessage("getServiceFailedRecordFromTable sqlQuerySmt::: "+sqlQuerySmt);
  executeCustomQuery(sqlQuerySmt, null, getFailureRecordSuccesCallback, getFailureRecordErrorCallback);
}
function getFailureRecordSuccesCallback(res) {
  printMessage("getFailureRecordSuccesCallback ::: "+JSON.stringify(res));
  // var failureRecord = [];
  // failureRecord.push(res);
  try{
    if(res.length>0){
      printMessage("getFailureRecordSuccesCallback:: gblErrorNumber "+gblErrorNumber);
      if(gblUploadTicket.type === "LEAD"){ 

        logUploadSupportSyncErrors(gblErrorNumber, "Lead", "ServiceError", res);

        for (var j = 0; j < res.length; j++) {
          printMessage("getFailureRecordSuccesCallback:: res.leadId "+res[j].leadId);
          deleteRecordUsingSelectedId(res[j].leadId);
        }
      }else{
        logUploadSupportSyncErrors(gblErrorNumber, "Alert", "ServiceError", res);
        for (var i = 0; i < res.length; i++) {
          printMessage("getFailureRecordSuccesCallback:: res.alertId "+res[i].alertId);
          deleteRecordUsingSelectedId(res[i].alertId);
        }
      }
    }
  }catch(err){
    printMessage("getFailureRecordSuccesCallback Exception::: "+JSON.stringify(err));
  }
}
function getFailureRecordErrorCallback(res) {
  printMessage("getFailureRecordErrorCallback ::: "+JSON.stringify(res));
}
/** Get failure record from leads table based on leadid  */
function deleteFailedsRecordFromLeadsAlertTable(isLeadRecord){
  
  var sqlQuerySmt 	= "delete from leads";
  if(isLeadRecord === false){ 

    sqlQuerySmt 	= "delete from alerts";
  }
  
  printMessage("deleteFailedsRecordFromLeadsAlertTable sqlQuerySmt::: "+sqlQuerySmt);
  executeCustomQuery(sqlQuerySmt, null, deleteLeadsRecordSuccesCallback, deleteLeadsRecordErrorCallback);
}
function deleteLeadsRecordSuccesCallback(res) {
  printMessage("deleteLeadsRecordSuccesCallback ::: "+JSON.stringify(res));
  printMessage("deleteLeadsRecordSuccesCallback:: gblErrorNumber "+gblErrorNumber);
}

function deleteLeadsRecordErrorCallback(res) {
  printMessage("deleteLeadsRecordErrorCallback ::: "+JSON.stringify(res));
}

/** Inserting error record into LAError and leaderrors/alerterrors table and delete record from leads/alert table */
function insertErrorRecordsIntoDB(errorNumber){

  printMessage("insertErrorRecordsIntoDB::: gblUploadTicket.type "+gblUploadTicket.type);
  gblErrorNumber 	= 	errorNumber;
  if(gblUploadTicket.type === "LEAD"){ 

    printMessage("insertErrorRecordIntoDB :: LEAD "+gblErrorNumber);
    if (!isEmpty(gblLeadsCollectionForScheduler) && gblLeadsCollectionForScheduler.length > 0) {

      printMessage("insertErrorRecordIntoDB:: leads"+ JSON.stringify( gblLeadsCollectionForScheduler));
      logUploadSupportSyncErrors(gblErrorNumber, "Lead", "ServiceError", gblLeadsCollectionForScheduler);
      deleteFailedsRecordFromLeadsAlertTable(true);
    }
    //Checking #82 issue
    /*if (!isEmpty(gblLeadsESBErrorCollectionForScheduler) && gblLeadsESBErrorCollectionForScheduler.length > 0) {

      printMessage("insertErrorRecordIntoDB:: gblLeadsESBErrorCollectionForScheduler "+ JSON.stringify( gblLeadsESBErrorCollectionForScheduler));
      updateRetrySyncErrors("Lead",gblLeadsESBErrorCollectionForScheduler);
    }*/

  }else{
    printMessage("insertErrorRecordIntoDB :: Alert "+gblErrorNumber);
    if (!isEmpty(gblAlertsCollectionForScheduler) && gblAlertsCollectionForScheduler.length > 0) {
      printMessage("insertErrorRecordIntoDB:: Alert "+JSON.stringify( gblAlertsCollectionForScheduler));
      logUploadSupportSyncErrors(gblErrorNumber, "Alert", "ServiceError", gblAlertsCollectionForScheduler);
      deleteFailedsRecordFromLeadsAlertTable(false);
    }
    //Checking #82 issue
    /*if (!isEmpty(gblAlertsESBErrorCollectionForScheduler) && gblAlertsESBErrorCollectionForScheduler.length > 0) {
      printMessage("insertErrorRecordIntoDB:: gblAlertsESBErrorCollectionForScheduler "+JSON.stringify( gblAlertsESBErrorCollectionForScheduler));
      updateRetrySyncErrors("Alert",gblAlertsESBErrorCollectionForScheduler);
    }*/
  }
}

/* Delete lead  from leads based on selected leadId**/
function deleteRecordUsingSelectedId(selectedLeadId){
  
  var sqlQuerySmt = "delete from leads where leadId = '"+selectedLeadId+"'";
 if(gblUploadTicket.type !== "LEAD"){ 
    
   sqlQuerySmt = "delete from alerts where alertId = '"+selectedLeadId+"'";
  }
  
  printMessage("deleteRecordUsingSelectedId sqlQuerySmt::: "+sqlQuerySmt);
  executeCustomQuery(sqlQuerySmt, null, success_commonCallback , error_commonCallback );
}




