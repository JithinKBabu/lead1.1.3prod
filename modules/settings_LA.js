/*** Author  : RI - CTS - 363601 ****/


/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshSettingsUI(){
  	//printMessage("Inside refreshSettingsUI"+kony.application.getCurrentForm());
	var frmObj						= kony.application.getCurrentForm();
	frmObj.flxBurgerMenu.centerX 	= "-50%";  
    if(gblIsLaunchedAsStandAloneApp){
    //frmObj.lblUserName.text = gblBasicInfo.email;
       frmObj.flxBurgerMenu.lblUserName.text = gblEmployeeObject.firstName+" " +gblEmployeeObject.lastName;
    }else{
      
       var isEmpAlreayLoggedIn			= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
        if (!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) {
         
            var loggedEmpProfile		= getEmpProfielFromStore();
          	gblBasicInfo.email			= loggedEmpProfile.email;
            //setGblUserObject(loggedEmpProfile); 
            gblEmployeeObject					= new Employee();
            gblEmployeeObject.email				= loggedEmpProfile.email;
            gblEmployeeObject.firstName			= loggedEmpProfile.firstName;
            gblEmployeeObject.lastName			= loggedEmpProfile.lastName;
        }
    //  printMessage("Inside refreshSettingsUI--->>"+JSON.stringify(gblEmployeeObject));
       if ((!isEmpty(gblEmployeeObject)) && (!isEmpty(gblEmployeeObject.firstName)) && (!isEmpty(gblEmployeeObject.lastName)) ) {
          frmObj.flxBurgerMenu.lblUserName.text = gblEmployeeObject.firstName+" " +gblEmployeeObject.lastName;
       }else{
    	  frmObj.flxBurgerMenu.lblUserName.text =  gblBasicInfo.email;
         }
    }
}

/** Settings Menu (Burger Menu)- Open menu button action **/
function openSettingsMenu(){
  	var frmObj						= kony.application.getCurrentForm();
  	refreshSettingsUI();
  	function openMenu_Callback(){}
	
	frmObj.flxBurgerMenu.animate(
		kony.ui.createAnimation({"100":{"centerX":"50%", "stepConfig":{"timingFunction":kony.anim.LINEAR}}}),
        {"delay":0, "iterationCount":1, "fillMode":kony.anim.FILL_MODE_FORWARDS, "duration":0.25},
      	{"animationEnd" : openMenu_Callback});
  
  	updateBurgetMenui18();
}

/** Settings Menu (Burger Menu)- Close menu button action **/
function closeSettingsMenu(){
	var frmObj						= kony.application.getCurrentForm();	
  	function closeMenu_Callback(){}
	
	frmObj.flxBurgerMenu.animate(
		kony.ui.createAnimation({"100":{"centerX":"-50%", "stepConfig":{"timingFunction":kony.anim.LINEAR}}}),
		{"delay":0, "iterationCount":1, "fillMode":kony.anim.FILL_MODE_FORWARDS, "duration":0.25},
 		{"animationEnd" : closeMenu_Callback});
  
  // frmObj.lblSupportViewSettings.text		= getI18nString("common.label.createAlert");
}

/** Settings Menu (Burger Menu)- Menu button action **/
function onClickMenuBtn(evntIdentifier){
  //printMessage("Inside onClickMenuBtn: " + JSON.stringify(evntIdentifier));
  //printMessage("Inside onClickMenuBtn: evntIdentifier.id" + evntIdentifier.id);
  //showMessagePopup("common.message.notImplemented");
  // showSettingsPopup();
  //LAA35
  closeSettingsMenu();
  // Displaying Waring for changing for Source
  //showConfirmPopup("common.message.1005", ActionConfirmSourceChange, ActionDeclineChangeEmail);
  //getiCabInstancesFromLocalTbl();

  //4445
  //Adding Validation
  var isEmpAlreayLoggedIn				= kony.store.getItem(LAConstants.IS_EMP_LOGGED_IN);
  printMessage("onClickMenuBtn:: isEmpAlreayLoggedIn "+isEmpAlreayLoggedIn);
  printMessage("onClickMenuBtn:: gblIsLaunchedAsStandAloneApp "+gblIsLaunchedAsStandAloneApp);
  if ((!isEmpty(isEmpAlreayLoggedIn) && isEmpAlreayLoggedIn === true) || gblIsLaunchedAsStandAloneApp === true){
    printMessage("User loged in ");
    if(kony.application.getCurrentForm().id === LAConstants.FORM_LEAD_MANAGEMENT){

      getiCabInstancesFromLocalTbl();
    }else if(kony.application.getCurrentForm().id === LAConstants.FORM_ALERTS || kony.application.getCurrentForm().id === LAConstants.FORM_LEADS){
      //Disply Language selection popup 
      displayLanguageSelectionPopup();
    }
  }else{

    if(kony.application.getCurrentForm().id === LAConstants.FORM_LEAD_MANAGEMENT){

      if(isNetworkAvailable()){
        startSyncICabInstances();
      }
    }else {
      //Disply Language selection popup 
      displayLanguageSelectionPopup();
    }
  }
}

function onConfirmSourceChange(){
  setSelectedSourceValue(gblCheckSelectedIcab);
  dismissConfirmPopup();
  gblSourceChange =true;
  onClickLogout();
  glbSegData								= [];
  gblCurrentPageIndex                      =0;
  resetSync();
  //getiCabInstancesFromLocalTbl();
}

/** Settings Menu (Burger Menu)- Logout button action **/
function onClickLogout(){
  
  //gblBasicInfo						= {};
  gblSharedInfo 					= {}; 
  kony.store.setItem(LAConstants.EMP_PROFILE, JSON.stringify(gblSharedInfo));
  kony.store.setItem(LAConstants.IS_EMP_LOGGED_IN, false);
  if(gblSourceChange === false){
    showLoading("common.message.1001");
    doExitApplication();
  }
}


function onClickEmailId(){
  showLoading("common.message.1001");
  closeSettingsMenu();
  getDeviceConfiguredEmails();
}

function doRefreshi18n(){
  
  printMessage("doRefreshi18n:: getCurrentForm "+kony.application.getCurrentForm().id);
  if(kony.application.getCurrentForm().id === LAConstants.FORM_LEADS){
    
    onPostShowLead();
    
  }else if(kony.application.getCurrentForm().id === LAConstants.FORM_ALERTS){
     onPostShowAlert();
    
  }else if(kony.application.getCurrentForm().id === LAConstants.FORM_LEAD_MANAGEMENT){
     onPostShowLeadManagement();
  }
}

function updateBurgetMenui18(){
   var frmObj								= kony.application.getCurrentForm();	
   frmObj.flxBurgerMenu.lblMenuName4.text					= getI18nString("common.label.convertoalert");
   frmObj.flxBurgerMenu.lblMenuName3.text					= getI18nString("common.label.createLead");
   frmObj.flxBurgerMenu.lblMenuName1.text					= getI18nString("common.label.settings");
   frmObj.flxBurgerMenu.lblMenuName2.text					= getI18nString("common.label.supportView");
   frmObj.flxBurgerMenu.lblDeleteAll.text					= getI18nString("common.label.deleteAll");
  // frmObj.lblAbout.text						= getI18nString("common.label.about");
  if(frmObj.id === LAConstants.FORM_ALERTS && gblIsLaunchedAsStandAloneApp === false){
    frmAlert.flxBurgerMenu.flxSettingBtn3Container.setVisibility(true);
    frmAlert.flxBurgerMenu.lblMenuName3.text =getI18nString("common.label.converttolead"); 
  }
}
