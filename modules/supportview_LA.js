function showSupportViewPage(){
  printMessage("supportview  showSupportViewPage isTrue ::");
  getAllSupportErrors();
}

function postShowSupportView(){
  SupportView.lblHeaderTitle.text 		=  getI18nString("common.label.supportView");
  SupportView.lblSyncErrorHdr.text 		=  LAConstants.SYNC_SERVER_HOST_CLOUD;
  SupportView.lblNoRecords.text 		=  getI18nString("common.message.noDataFound");
  
  SupportView.lblDetailsHeader.text		=  getI18nString("common.label.action");
  SupportView.btnResend.text 			=  getI18nString("common.label.resend");
  SupportView.btnDelete.text 			=  getI18nString("common.label.delete");
  
  refreshSettingsUI();
  setMasterBurgerMenuVisibity();
}

function closeSupportView(){
  showLeadsManagementFromLocal();
}

//adding the code for support view
 var ids=0;
	 gblSupportView = {
        SUPPORTVIEW_DATE_TIME_FORMAT: "YYYY-MM-DD HH:mm:ss:SSSS",
        //date time format which showing in support view screen
        // for remove record from support view screen when onclick 'Resend' button
        selIndex: null,
        selItem: null,
        isError: false,
        //client and server side sync error codes
        //which is for ignore to log supportview
        //SYNC_ERROR_CODES: ["7001", "7002", "7003", "7004", "7005", "7006", "7007", "7008", "7009", "7010", "7011", "7012", "7013", "7014", "7015", "7016", "7018", "7019", "7020", "7021", "7022", "7777", "8888", "1000", "1011", "1012", "1013", "1014", "1015", "1016", "1022", "1200", "7025", "1001", "1002", "1003", "1004", "1005", "1006", "1008", "1010", "SY3001E", "SY3002E", "SY3003E", "SY3004E", "SY3005E", "SY0000E"],
        SYNC_ERROR_CODES: ["7001", "7002", "7003", "7004", "7005", "7006", "7007", "7008", "7009", "7010", "7011", "7012", "7013", "7014", "7015", "7016", "7018", "7019", "7020", "7021", "7022", "7777", "8888", "1022", "1200", "7025", "1001", "1002", "1003", "1004", "1005", "1006", "1008", "1010", "SY3001E", "SY3002E", "SY3003E", "SY3004E", "SY3005E", "SY0000E"],
        RETRY_SYNC_ERROR_CODES: ["7001", "7002", "7003", "7004", "7005", "7006", "7007", "7008", "7009", "7010", "7011", "7012", "7013", "7014", "7015", "7016", "7018", "7019", "7020", "7021", "7022", "7777", "8888", "1022", "1200", "7025", "1001", "1002", "1003", "1004", "1005", "1006", "1008", "1010", "SY0000E"],
        //Http server side error codes
        HTTP_ERROR_CODES: ["429", "500", "501", "502", "503", "504", "505", "506", "508", "511", "520", "521", "522", "523", "524"]
    };
gblRetryErrorCodes = [1000, 1011, 1012, 1013, 1014, 1015, 1016];
gblNoOfRetryAttempts = 0;


function checkLeadHistoryAlreadyExistedInDB(failedLHRecords){
  
  var sqlStmt 			= "";
  var sqlStmtDateTime 	= "";
  if(!isEmpty(failedLHRecords) & failedLHRecords.length > 0){
     for(var i = 0; i < failedLHRecords.length; i++){
       gblLeadHistoryErrorData =[];
       gblLeadHistoryErrorData.push(failedLHRecords[i]) ;
       printMessage("checkLeadHistoryAlreadyExistedInDB::failedLHRecords : "+JSON.stringify(failedLHRecords));
       printMessage("checkLeadHistoryAlreadyExistedInDB::gblLeadHistoryErrorData: "+JSON.stringify(gblLeadHistoryErrorData));
       sqlStmt 			= "select * from LAError where errorCode = '"+failedLHRecords[i].errorCode+"'"+"AND serviceName = '"+"LeadHistory"+"'";
       sqlStmtDateTime 	= "select * from LAError where errorCode = '"+failedLHRecords[i].errorCode+"'"+"AND serviceName = '"+"LeadHistory"+"'"; 
       
       executeCustomQuery(sqlStmt, null, successLAErrorLeadHistoryData , faliureLAErrorLeadHistoryData );
       executeCustomQuery(sqlStmtDateTime, null, successLAErrorDateTimeData , faliureLAErrorDateTimeData );
     }
  }
}

function successLAErrorLeadHistoryData(res){
  
  printMessage("inside successLAErrorLeadHistoryData : res" + JSON.stringify(res));
  printMessage("inside successLAErrorLeadHistoryData : gblLeadHistoryErrorData" + JSON.stringify(gblLeadHistoryErrorData));

  printMessage("inside successLAErrorLeadHistoryData : res:: " + res.length);
  if(res.length ===0){
        
   
    //logDownloadSupportSyncErrors(gblLeadHistoryErrorData);
     LAError.createAll(gblLeadHistoryErrorData, insertSyncErrSuccesscallback, insertAllSyncErrErrorcallback, true);

  }else{
    printMessage("inside successLAErrorData: Record already existed in LAError tabel : ");
  }
  dismissLoading();
}
function faliureLAErrorLeadHistoryData(err){
  
  printMessage("inside faliureLAErrorLeadHistoryData ::: "+JSON.stringify(err));
}


function successLAErrorDateTimeData(res){
  
 printMessage("inside successLAErrorDateTimeData : res" + JSON.stringify(res));
 printMessage("inside successLAErrorDateTimeData : res" + JSON.stringify(gblLeadHistoryErrorData));
 	if( res.length >0){
    
      for(var i = 0; i < res.length; i++){
        printMessage("inside successLAErrorDateTimeData : res[i].dateTime" + res[i].dateTime);
        var todaydate 						= getDateTimeFormat(new Date(), gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT); // YYYY-MM-DD HH:mm:ss:SSSS
        var dateDiffInMins 					= getSupportViewDateTimeDiffInMins(todaydate, res[i].dateTime);
        dateDiffInMins 						= parseInt(dateDiffInMins);
        printMessage("inside successLAErrorDateTimeData : todaydate " + todaydate);
        printMessage("inside successLAErrorDateTimeData : dateDiffInMins" + dateDiffInMins);
        printMessage("inside successLAErrorDateTimeData : res[i].dateTime" + res[i].dateTime);
        if(dateDiffInMins >= LAConstants.SUPPORTvIEW_ERROR_LOG_TIMER){

          // logDownloadSupportSyncErrors(gblLeadHistoryErrorData);
          LAError.createAll(gblLeadHistoryErrorData, insertSyncErrSuccesscallback, insertAllSyncErrErrorcallback, true);
        }else{
          printMessage("inside successLAErrorDateTimeData: Record already existed in LAError tabel Time  : ");
        }
      }
  }else{
    printMessage("inside successLAErrorDateTimeData: Record already existed in LAError tabel : ");
  }
  dismissLoading();
}

function insertSyncErrSuccesscallback(res) {
  printMessage("insertSyncErrorsSuccesscallback : res" + JSON.stringify(res));
}

function insertAllSyncErrErrorcallback(res) {
  printMessage("insertAllSyncErrorsErrorcallback : res" + JSON.stringify(res));
}
function faliureLAErrorDateTimeData(err){
  
  printMessage("inside faliureLAErrorDateTimeData ::: "+JSON.stringify(err));
}

function logDownloadSupportSyncErrors(outputparams) {
    //printMessage("*****SYNC LOG SUPPORT VIEW Inside logDownloadSupportSyncErrors - outputparams => " + JSON.stringify(outputparams));
    function getAllErrosSuccesCallback(res) {
        //printMessage("*****SYNC LOG SUPPORT VIEW getAllErrosSuccesCallback : res" + JSON.stringify(res));
        var errorTable = [];
        var syncErrorCodes = gblSupportView.SYNC_ERROR_CODES;
        var httpErrorCodes = gblSupportView.HTTP_ERROR_CODES;
        if (outputparams !== null && outputparams !== "") {
            var scopeErrors = outputparams.errorInfo;
            printMessage("*****SYNC LOG SUPPORT VIEW scopeErrors is::" + scopeErrors);
            var errObj = {};
            var serviceName = "";
            var errorInfo = {};
            var jsonString = "";
            var isSyncErrorExist;
            var errorCode = "";
            var temperrorCode = outputparams.errorCode;
            printMessage("*****SYNC LOG SUPPORT VIEW temperrorCode is :" + temperrorCode);
            if (temperrorCode !== null && temperrorCode !== undefined && temperrorCode == 'SY3001E') {
                gblTempSyncVersionDiff = false;
                if (outputparams !== undefined && outputparams !== null && outputparams !== "" && gblRetryErrorCodes.indexOf(errorCode) == -1) {
                    errObj.errorCode = outputparams.errorCode + "";
                    errObj.errorType = "SyncError";
                    errObj.errorMessage = "";
                    errObj.serviceName = "Version difference";
                    errObj.dateTime = getDateTimeFormat(new Date(), gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT); //YYYY-MM-DD HH:mm:ss:SSSS
                    errObj.businessCode =(!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
                  	errObj.countryCode = (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;	
                    isSyncErrorExist = isServiceErrorExist(res, serviceName, errObj.errorCode, errObj.dateTime);
         			
                  
                  printMessage("errObj.countryCode"+errObj.countryCode);
                  printMessage("errObj.businessCode"+errObj.businessCode);
                  printMessage("errObj.countryCodegblBasicInfo "+JSON.stringify(gblBasicInfo));
                  
                  if (isSyncErrorExist === false) {
                        errorTable.push(errObj);
                    }
                    gblSyncVersionDiff = true;
                    kony.store.setItem("gblSyncVersionDiff", gblSyncVersionDiff);
                }
            } else {
                for (var scopeName in scopeErrors) {
                    errObj = {};
                    serviceName = getSyncServiceName(scopeName);
                    errorCode = scopeErrors[scopeName].errorCode + "";
                    printMessage("*****SYNC LOG SUPPORT VIEW in logDownloadSupportSyncErrors serviceName is :" + serviceName + ", errorCode is :" + errorCode);
                    //if(serviceName != "" && isSyncErrorExist == false){
                    errorInfo = scopeErrors[scopeName].errorInfo;
                    jsonString = JSON.stringify(errorInfo);
                    printMessage("jsonString ::" + jsonString);
                    if (jsonString !== undefined && errorInfo !== null && jsonString != "{}" && gblRetryErrorCodes.indexOf(errorCode) == -1) {
                        var isTrue = jsonString.indexOf('isError\\\": true') > -1;
                        printMessage("isTrue ::" + isTrue);
                        if (isTrue) {
                            printMessage("isTrue ::----->>>if" + isTrue);
                            errObj.errorCode = getDownloadSupportErrorCode(jsonString).trim();
                            errObj.errorType = "ServiceError";
                        } else {
                            printMessage("isTrue ::----->>>else" + isTrue);
                            errObj.errorCode = scopeErrors[scopeName].errorCode + "";
                            errObj.errorType = "SyncError";
                        }
                    }
                    /*else {
                        errObj.errorCode = scopeErrors[scopeName].errorCode + "";
                        errObj.errorType = "SyncError";
                    }*/
                    errObj.errorMessage = "";
                    errObj.serviceName = serviceName;
                    errObj.dateTime = getDateTimeFormat(new Date(), gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT);//YYYY-MM-DD HH:mm:ss:SSSS
                  
                    errObj.businessCode =(!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
                    errObj.countryCode = (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;	
         			printMessage("errObj.countryCode"+errObj.countryCode);
                   printMessage("errObj.businessCode"+errObj.businessCode);
                  printMessage("errObj.countryCodegblBasicInfo "+JSON.stringify(gblBasicInfo));
                  
                    //if error code not exist then push into support view else ignore because we need ignore sync and network errors
                    isSyncErrorExist = isServiceErrorExist(res, serviceName, errObj.errorCode, errObj.dateTime);
                    printMessage("isSyncErrorExist::" + isSyncErrorExist);
                    printMessage("ErrorCode::" + errObj.errorCode);
                    var pastDate = kony.store.getItem("SyncDate");
                    printMessage("*****SYNC LOG SUPPORT VIEW pastDate => " + pastDate);
                    //var isFutDate = isFutureDate(pastDate);
                    var isFutDate = true;
                    printMessage("*****SYNC LOG SUPPORT VIEW isSyncErrorExist => " + isSyncErrorExist);
                    printMessage("*****SYNC LOG SUPPORT VIEW gblNoOfRetryAttempts => " + gblNoOfRetryAttempts);
                    printMessage("*****SYNC LOG SUPPORT VIEW isFutDate => " + isFutDate);
                    printMessage("*****SYNC LOG SUPPORT VIEW isSyncErrorExist IF Condition is => " + (isSyncErrorExist && isFutDate && gblNoOfRetryAttempts == 1));
                    printMessage("*****SYNC LOG SUPPORT VIEW isSyncErrorExist IF Condition is => " + (isSyncErrorExist && isFutDate));
                    //if (isSyncErrorExist && isFutDate && gblNoOfRetryAttempts == 1) {
                  	if (isSyncErrorExist && isFutDate ) {
                        isSyncErrorExist = false;
                    }
                    printMessage("*****SYNC LOG SUPPORT VIEW isSyncErrorExist after IF Condition::" + isSyncErrorExist);
                    if (errObj.errorCode !== undefined && errObj.errorCode != 'undefined' && (gblRetryErrorCodes.indexOf(errorCode) == -1) && isSyncErrorExist === false) {
                        errorTable.push(errObj);
                    } else if (errObj.errorCode !==undefined && errObj.errorCode != 'undefined') {
                        printMessage("*****SYNC LOG SUPPORT VIEW  this not sync error related to version code");
                    } else {
                        printMessage("*****SYNC LOG SUPPORT VIEW  check if sync error related to version code");
                    }
                }
            }
        }
        printMessage("errorTable : " + JSON.stringify(errorTable));
        if (errorTable.length > 0) {
            printMessage("insertion function to error table is called");

            /*function insertSyncErrorsSuccesscallback(res) {
                printMessage("insertSyncErrorsSuccesscallback : res" + JSON.stringify(res));
            }

            function insertAllSyncErrorsErrorcallback(res) {
                printMessage("insertAllSyncErrorsErrorcallback : res" + JSON.stringify(res));
            }*/
           // com.kony.ErrorScope.Errors.createAll(errorTable, insertSyncErrorsSuccesscallback, insertAllSyncErrorsErrorcallback, true);
          
          checkLeadHistoryAlreadyExistedInDB(errorTable);
          //LAError.createAll(errorTable, insertSyncErrorsSuccesscallback, insertAllSyncErrorsErrorcallback, true);
        }
    }

    function getAllErrorsErrorCallback(res) {
        printMessage("getAllErrorsErrorCallback : res" + JSON.stringify(res));
    }
    var stmt = "select * from LAError";
    executeCustomQuery(stmt, null, getAllErrosSuccesCallback, getAllErrorsErrorCallback);
}

function insertSyncErrorsSuccesscallback(res) {
  printMessage("insertSyncErrorsSuccesscallback : res" + JSON.stringify(res));
}

function insertAllSyncErrorsErrorcallback(res) {
  printMessage("insertAllSyncErrorsErrorcallback : res" + JSON.stringify(res));
}

function getSyncServiceName(scopeName) {
    var serviceName = "";
    switch (scopeName) {
    case "LALeadHistoryScope":
        serviceName = "LeadHistory";
        break;
    case "LALeadScope":
        serviceName = "Lead";
        break;
    case "LAAlertScope":
        serviceName = "Alert";
        break;
    default:
        break;
    }
    return serviceName;
}

function getDownloadSupportErrorCode(jsonString) {
    printMessage(" Inside getDownloadSupportErrorCode - jsonString :" + jsonString);
    var errorCode = "";
    try {
        //var jsonArr = jsonString.split("errorCode");
     	 var jsonArr = jsonString.split("errorNumber");
        if (jsonArr !== null && jsonArr.length > 0) {
            //var str = jsonArr[3];
            var str = jsonArr[1];
            kony.print("str :" + str);
            if (str !== null) {
                var strArr = str.split(",");
                kony.print("strArr :" + JSON.stringify(strArr));
                if (strArr !== null && strArr.length > 0) {
                    errorCode = strArr[0].replace(/[^\w\s]/gi, '');
                }
            }
        }
    } catch (error) {
        printMessage("Error while retrieve errorCode :" + JSON.stringify(error));
    }
    return errorCode;
}

/** Checking if service and errorcode exist in 'Error' table */
function isServiceErrorExist(errors, serviceName, errorCode, dateTime) {
    var isExist = false;
    printMessage("in isServiceErrorExist errors is :" + JSON.stringify(errors));
    if (errors !== null && errors.length > 0) {
        for (var i = 0; i < errors.length; i++) {
            printMessage("*****SYNC LOG serviceName is :" + serviceName + " existing servc :" + errors[i].serviceName + " errorcode is :" + errorCode + " existing error :" + errors[i].errorCode + "dateTime is :" + dateTime + " existing dateTime :" + errors[i].dateTime);
            if(dateTime.substring(0, 10) == errors[i].dateTime.substring(0, 10) && serviceName == errors[i].serviceName && errorCode == errors[i].errorCode){
            	isExist = true;
                break;
            } else if (serviceName == errors[i].serviceName && errorCode == errors[i].errorCode) {
                isExist = true;
                break;
            } else if (errorCode == errors[i].errorCode) {
                isExist = true;
                break;
            }
        }
    }
    printMessage("is already exists :" + isExist);
    return isExist;
}

function logUploadSupportSyncErrors(errorCode, serviceName, errorType, obj){

  printMessage(" Inside logUploadSupportSyncErrors errorCode :"+errorCode);
  //alert("errorCode "+errorCode);
  printMessage(" serviceName :"+serviceName);
  printMessage("obj  ::::" + JSON.stringify(obj));
  
  gblErrorObj= [];
  printMessage("logUploadSupportSyncErrors  after obj ");
  gblServiceName = "";
  printMessage("logUploadSupportSyncErrors  after gblServiceName ");
  gblServiceName 	= serviceName;
  printMessage("logUploadSupportSyncErrors  after gblServiceName 1:: obj length "+obj.length);
  gblErrorObj = obj ;
  printMessage("logUploadSupportSyncErrors  after gblServiceName 1::gblErrorObj length "+gblErrorObj.length);
  var sqlStmt 		="";
  for(var k=0; k < obj.length; k++){
		printMessage("logUploadSupportSyncErrors  after gblServiceName in loop");
    if(serviceName=="Lead"){

      printMessage("Lead::: "+obj[k].leadId);
      //sqlStmt 		= "select * from LAError where leadalertIds != '"+obj[a1].leadId+"'"+" AND errorCode = '"+gblErrorNumber+"'";
       sqlStmt 			= "select * from LAError where errorCode = '"+gblErrorNumber+"'"+" AND serviceName = '"+gblServiceName+"'"+" AND leadalertIds = '"+obj[k].leadId+"'";
   	   printMessage("Lead::: sqlStmt "+sqlStmt);
    }else if(serviceName=="Alert"){

      //sqlStmt 		= "select * from LAError where leadalertIds != '"+obj[a1].alertId+"'"+" AND errorCode = '"+gblErrorNumber+"'";
      //sqlStmt 			= "select * from LAError errorCode != '"+gblErrorNumber+"'";
     sqlStmt 			= "select * from LAError where errorCode = '"+gblErrorNumber+"'"+" AND serviceName = '"+gblServiceName+"'"+" AND leadalertIds = '"+obj[k].alertId+"'";
     printMessage("Alert::: sqlStmt "+sqlStmt);
    }else if(serviceName=="LeadHistory"){

      //sqlStmt = "select * from LAError where leadalertIds = '"+obj[a1].selectedLeadId+"'"+" AND errorCode = '"+gblErrorNumber+"'";
    }
    printMessage("logUploadSupportSyncErrors sqlStmt :"+sqlStmt);
    executeCustomQuery(sqlStmt, null, successLAErrorData , faliureLAErrorData );
  }
}

function successLAErrorData(res){
  
  printMessage("inside successLAErrorData : res" + JSON.stringify(res));
  printMessage("inside successLAErrorData : res" + isEmpty(res));
  printMessage("inside successLAErrorData : res" + res.length);
  // if(!isEmpty(res) && res.length ===0){
  
  if(res.length ===0){

    logUploadSupportSyncErrorsIntoDB(gblErrorNumber, gblServiceName, "ServiceError", gblErrorObj);
  }else{
    printMessage("inside successLAErrorData: Record already existed in LAError tabel : ");
  }
  dismissLoading();
}
function faliureLAErrorData(err){
  
  printMessage("inside faliureLAErrorData ::: "+JSON.stringify(err));
}


function logUploadSupportSyncErrorsIntoDB(errorCode, serviceName, errorType, obj){

  printMessage(" Inside logUploadSupportSyncErrorsIntoDB errorCode :"+errorCode);
  //alert("errorCode "+errorCode);
  printMessage(" serviceName :"+serviceName);
  printMessage("logUploadSupportSyncErrorsIntoDB obj  ::::" + JSON.stringify(obj));

  function getAllErrosSuccesCallback1(res){

    if(!isEmpty(res)){
      printMessage("getAllErrosSuccesCallback1 : res" + JSON.stringify(res));

      if(!(isNaN('NaN'))){
        ids =res.maxid;
      }
    }
    printMessage("logUploadSupportSyncErrors: ids-converted" + ids);
    var errObj = {};
    if(errorCode !=='undefined' && isEmpty(errorCode) === false){
      
      errObj.errorCode = errorCode.trim();
    } else{


      errObj.errorCode = "";
    }
    
    errObj.errorType = errorType+"";
    errObj.errorMessage = "";
    errObj.serviceName = serviceName;


    errObj.businessCode=(!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
    errObj.countryCode = (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
    for(var al=0; al < obj.length; al++){
      var leadalertobj =obj[al];
      
      //update dateTime
      printMessage("logUploadSupportSyncErrors obj[al].dateTime::: "+obj[al].updateDateTime);

      var previousFailedDate = getDateTimeFormat(obj[al].updateDateTime, gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT);
      
       errObj.dateTime   = previousFailedDate; 
       printMessage("logUploadSupportSyncErrors obj[al].dateTime::: after "+errObj.dateTime);
      //Update errorNumber
      insertUploadServiceErrors(errObj, serviceName, leadalertobj);
      printMessage("Inside insertUploadServiceErrors Error Obj" + JSON.stringify(errObj));

      if(al== obj.length){
        printMessage(" updateIntegrationServiceCallFlag----->>> :");
        updateIntegrationServiceCallFlag();
      }
    }

    //the below code needs call from method
    //call to upload the flag--->
    function insertSyncErrorsSuccesscallback(res) {
      printMessage("insertSyncErrorsSuccesscallback : res" + JSON.stringify(res));
    }
    function insertSyncErrorsErrorcallback(res) {
      printMessage("Inside insertUploadServiceErrors insertSyncErrorsErrorcallback : res" + JSON.stringify(res));
    }
  }
  function getAllErrorsErrorCallback1(res) {
    kony.print("getAllErrorsErrorCallback1 : res" + JSON.stringify(res));
  }
  var stmt = "SELECT MAX(id) as maxid FROM LAError";
  executeCustomQuery(stmt, null, getAllErrosSuccesCallback1, getAllErrorsErrorCallback1);	
}

function updateRetrySyncErrors(serviceName, obj){

  printMessage(" Inside updateRetrySyncErrors :"+JSON.stringify(obj));
  printMessage(" Inside updateRetrySyncErrors : serviceName "+serviceName);
  var sqlUpdateStatement 	= "";
  var sqlUpdateLAErrStmt 	= "";
  for(var al=0; al < obj.length; al++){
    var leadalertobj = obj[al];
    var mUpdatedTime				= getDateTimeFormat(new Date(), "YYYY-MM-DDTHH:mm:ss.SSS"); //getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");   
    var mUpdatedSupportViewTime		= getDateTimeFormat(new Date(), gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT); //YYYY-MM-DD HH:mm:ss:SSSS

    if (serviceName == "Lead") {
      sqlUpdateStatement	    = "Update leaderrors set updateDateTime = '"+mUpdatedTime+"' where leadId = '" + leadalertobj.leadId+"'";
      sqlUpdateLAErrStmt    	= "Update LAError set dateTime = '"+mUpdatedSupportViewTime+"' where leadalertIds = '" + leadalertobj.leadId+"'";

    }else{
      sqlUpdateStatement	    = "Update alerterrors set updateDateTime = '"+mUpdatedTime+"' where alertId = '" + leadalertobj.alertId+"'";
      sqlUpdateLAErrStmt    	= "Update LAError set dateTime = '"+mUpdatedSupportViewTime+"' where leadalertIds = '" + leadalertobj.alertId+"'";
    }	
    
   
    printMessage(" Inside updateRetrySyncErrors : mUpdatedTime "+mUpdatedTime);
    printMessage(" Inside updateRetrySyncErrors : mUpdatedTime "+mUpdatedSupportViewTime);
	printMessage(" Inside updateRetrySyncErrors : sqlUpdateStatement "+sqlUpdateStatement);
    printMessage(" Inside updateRetrySyncErrors : sqlUpdateStatement "+sqlUpdateLAErrStmt);
    
    executeCustomQuery(sqlUpdateStatement, null, success_commonCallback , error_commonCallback );
    executeCustomQuery(sqlUpdateLAErrStmt, null, success_commonCallback , error_commonCallback );
  }
}

function insertUploadServiceErrors(errorObj, serviceName, obj){
  printMessage("TestInside1 insertUploadServiceErrors - errorObj :"+JSON.stringify(errorObj));
  printMessage(" serviceName :"+serviceName);

  function insertSyncErrorsErrorcallback(res) {
    printMessage("Inside insertUploadServiceErrors insertSyncErrorsErrorcallback : res" + JSON.stringify(res));
  }

  var errorCode="";
  var errorType="";
  var errorMessage="";
  // var serviceName="";
  var dateTime =errorObj.dateTime;
  var leadalertIds='';
  if (isEmpty(errorObj.errorCode)) {
    errorCode ='';
  }else{
    errorCode =errorObj.errorCode;
  }
  if (isEmpty(errorObj.errorType)){
    errorType ='';
  }else{
    errorType = errorObj.errorType;
  }
  if (isEmpty(errorObj.errorMessage)){
    errorMessage ='';
  }else{
    errorMessage=errorObj.errorMessage;
  }

  ids++;

  function createSuccesscallback(res){
    printMessage("createSuccesscallback : res" + JSON.stringify(res));
  }
  function createErrorcallback(res){
    printMessage("createErrorcallback : res" + JSON.stringify(res));
  }
  if(serviceName == "Lead"){
    obj.id = parseInt(ids);
    obj.countryCode = (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
    obj.businessCode = (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
    printMessage("Inside insertUploadServiceErrors before insertUploadServiceErrors obj : " + JSON.stringify(obj));
    leaderrors.create(obj, successcallbackleadsAlertsErrorInsertion, errorcallbackleadsAlertsErrorInsertion, false);
    printMessage("inside lead insert --> " + JSON.stringify(obj));

    var sqlQueryLErrorInsert = "Insert into LAError (id,errorCode,errorType,errorMessage,serviceName,dateTime,leadalertIds,countryCode,businessCode) values ('"+ids+"','"+errorCode+"','"+errorType+"','"+errorMessage+"','"+errorObj.serviceName+"','"+dateTime+"','"+obj.leadId+"','"+errorObj.countryCode+"','"+errorObj.businessCode+"')";
    printMessage("Inside insertUploadServiceErrors insertSyncErrorsErrorcallback : res" + sqlQueryLErrorInsert);
    executeCustomQuery(sqlQueryLErrorInsert, null, successcallbackleadsAlertsErrorInsertion, errorcallbackleadsAlertsErrorInsertion);

  }else if(serviceName == "Alert"){
    obj.id = parseInt(ids);
    printMessage("Inside insertUploadServiceErrors before alert insert obj : " + JSON.stringify(obj));
    alerterrors.create(obj, successcallbackleadsAlertsErrorInsertion, errorcallbackleadsAlertsErrorInsertion, false);

    var sqlQueryAErrorInsert = "Insert into LAError (id,errorCode,errorType,errorMessage,serviceName,dateTime,leadalertIds,countrycode,businesscode) values ('"+ids+"','"+errorCode+"','"+errorType+"','"+errorMessage+"','"+errorObj.serviceName+"','"+dateTime+"','"+obj.alertId+"','"+errorObj.countryCode+"','"+errorObj.businessCode+"')";
    printMessage("Inside insertUploadServiceErrors insertSyncErrorsErrorcallback : res" + sqlQueryAErrorInsert);
    executeCustomQuery(sqlQueryAErrorInsert, null, successcallbackleadsAlertsErrorInsertion, errorcallbackleadsAlertsErrorInsertion);

  } 
}


function getAllSupportErrors() {
  var businessCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  var countryCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;	
  printMessage("supportview  getAllSupportErrors ");
  var sql = "select * from LAError  where  countryCode = '" + countryCode + "' AND businessCode = '"+businessCode+"' order by dateTime desc";
  printMessage(" sql ::" + sql);
  executeCustomQuery(sql, null, getAllSupportErrorsSuccessCallback, getAllSupportErrorsErrorCallback);
}

function getAllSupportErrorsErrorCallback(res) {
     printMessage("getAllSupportErrorsErrorCallback : res" + JSON.stringify(res));
   // dismissLoadingIndicator();
}

function getAllSupportErrorsSuccessCallback(res) {
  printMessage(" supportview getAllSupportErrorsSuccessCallback : res" + JSON.stringify(res));
  var segData = "";
    segData = getSupportViewSegData(res);
    SupportView.segSupportView.setData(segData);
    if (segData !== null && segData.length > 0) {

      SupportView.lblNoRecords.setVisibility(false);

    } else {

      SupportView.lblNoRecords.setVisibility(true);
    }
    showSupportViewForm();
}
function supportDeleteAllErrorLogConfirmHandler(res) {
  printMessage("- Inside supportDeleteAllErrorLogConfirmHandler - res :" + res);
  if (res) {
    showLoading("common.message.1001");
    var sqlQueryLAError 			= "delete from LAError";
    executeCustomQuery(sqlQueryLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);

    dismissConfirmPopup();
    SupportView.flxSupportTransparant.setVisibility(false);
  }
}

function getSupportViewSegData(res) {
     printMessage(" supportview -- Inside getSupportViewSegData --"+JSON.stringify(res));

    var segData = [];
    if (res !== null && res.length > 0) {
        printMessage(" supportview -- Inside getSupportViewSegData --1"+JSON.stringify(res));
        var temp = {};
        var record = {};
        var dateTimeArr = [];
        for (var i = 0; i < res.length; i++) {
            printMessage(" supportview -- Inside getSupportViewSegData --2");
            record = res[i];
            dateTimeArr = record.dateTime.split(" ");

            temp = {};
            temp.lblDate = dateTimeArr[0];
            temp.lblTime = dateTimeArr[1];
            temp.lblErrorCode = record.errorCode + "";
            temp.lblService = record.serviceName;
            temp.errorType = record.errorType + "";
            temp.errorId = record.id;

            segData.push(temp);
        }
    }

    return segData;
}

function showSupportViewForm() {
  printMessage(" supportview- Inside showSupportViewForm --");
  SupportView.show();
}

// onclick of segment to view the selected segment roe

function onClickSupportViewSegment() {
    printMessage("- supportview Inside onClickSupportViewSegment - ");
    SupportView.flxSupportTransparant.setVisibility(true);
    var selIndex = SupportView.segSupportView.selectedIndex;
    var selItem  = SupportView.segSupportView.selectedItems[0];
    var serviceErrorCode = selItem.lblErrorCode;
    if (serviceErrorCode !== null && serviceErrorCode == "SY3001E") {
        SupportView.btnResend.setEnabled(false);
    } else {
        SupportView.btnResend.setEnabled(true);
    }
}

function onClickbtnDelete() {
  
  printMessage("-supportview btnDelete OnClick btnDelete -");
  var selItem = SupportView.segSupportView.selectedItems[0];
  printMessage("selItem :" + JSON.stringify(selItem));
  glbServiceName 	= selItem.lblService;
  printMessage("glbServiceName::: "+glbServiceName);
 // if (glbServiceName == "LeadHistory" || glbServiceName == "Lead" || glbServiceName == "Alert" ) {
  if (glbServiceName == "Lead" || glbServiceName == "Alert" ) {
    showConfirmPopup("common.message.0003", supportErrorLogConfirmHandler, ActionDeclineChangeEmail);
  } else {
    supportErrorLogConfirmHandler(true);
  }
}

function supportErrorLogConfirmHandler(res) {
  printMessage("- Inside supportErrorLogConfirmHandler - res :" + res);
  if (res) {
    showLoading("common.message.1001");
    var selIndex = SupportView.segSupportView.selectedIndex;
    var selItem = SupportView.segSupportView.selectedItems[0];
    printMessage("selIndex :" + selIndex);
    printMessage("selItem :" + JSON.stringify(selItem));
   
    deleteSupportErrorLog();
    dismissConfirmPopup();
    SupportView.flxSupportTransparant.setVisibility(false);
  }
}

function deleteSupportErrorLog() {
  var selIndex = SupportView.segSupportView.selectedIndex;
  var selItem = SupportView.segSupportView.selectedItems[0];
  var sqlLAError 			= "delete from LAError"+" where id = '" + selItem.errorId+"'";
  var sqlLeadAletsError 	= "";
  var sqlLeadAletsUpdate 	= "";
   
  var mUpdatedDate			= getTodaysDateStr();  
  var mUpdatedTime			= getCurrentTimeStr();  

  if (glbServiceName == "Lead") {
    //sqlLeadAletsError 		= "delete from leaderrors "+" where id = '" + selItem.errorId+"'";
    sqlLeadAletsUpdate		= "Update leaderrors set leadDeleted = '"+1+"', createdDate = '"+mUpdatedDate+"', createdTime = '"+mUpdatedTime +"' where id = '" + selItem.errorId+"'";
 	printMessage("inside deleteSupportErrorLog :: "+sqlLeadAletsUpdate);
    executeCustomQuery(sqlLeadAletsUpdate, null, success_commonCallback , error_commonCallback );
    
  }else if(glbServiceName == "Alert"){
    sqlLeadAletsError 		= "delete from alerterrors "+" where id = '" + selItem.errorId+"'";
   // sqlLeadAletsUpdate		= "Update alerterrors set alertDeleted = "+1+", updateDateTime = '"+mUpdatedTime+"' where id = '" + selItem.errorId+"'";
	executeCustomQuery(sqlLeadAletsError, null, success_commonCallback, error_commonCallback);
  }
  printMessage("inside deleteSupportErrorLog ::sqlLAError  "+sqlLAError);
  executeCustomQuery(sqlLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);
}

function deleteSupportErrorLogSuccessCallback(res) {
    printMessage("deleteSupportErrorLogSuccessCallback : res" + JSON.stringify(res));
  	dismissLoading();
  	getAllSupportErrors();
}

function deleteSupportErrorLogErrorCallback(res) {
    printMessage("deleteSupportErrorLogErrorCallback : res" + JSON.stringify(res));
    dismissLoading();
}

function onClickbtnResend() {

  printMessage(" supportview*****SYNC LOG- btnResend OnClick Resend -");
  var sqlInsertionStr = "";
  var selItem = SupportView.segSupportView.selectedItems[0];
  glbServiceName 	= selItem.lblService;
  printMessage("selItem :" + JSON.stringify(selItem));
  printMessage("selItem : error code" + selItem.errorId);

  SupportView.flxSupportTransparant.setVisibility(false);

  var tableName 		= "";
  var sqlStatement 		= "";
  if (glbServiceName == "Lead") {
    tableName = "leaderrors";
    sqlStatement		= "SELECT * FROM "+tableName+" WHERE id = '" + selItem.errorId + "'";  	
    getSelectedLeadsErrorFromLocalTbl(sqlStatement,selItem.errorId,sucessSegSelectedErrorData,faliureSegSelectedErrorData);
  }
  else if(glbServiceName == "Alert"){
    tableName 	= "alerterrors";
    sqlStatement		= "SELECT * FROM "+tableName+" WHERE id = '" + selItem.errorId + "'";  	
    getSelectedLeadsErrorFromLocalTbl(sqlStatement,selItem.errorId,sucessSegSelectedErrorData,faliureSegSelectedErrorData);
  }else if(glbServiceName == "LeadHistory"){
    if (isNetworkAvailable() === true) {
      startSyncLeadHistory();
    }else{
      showMessagePopup("common.message.2002", ActionPopupMessageOk);
    }
  }

 /* var sqlStatement		= "SELECT * FROM "+tableName+" WHERE id = '" + selItem.errorId + "'";  	
  showLoading("common.message.1001");
  getSelectedLeadsErrorFromLocalTbl(sqlStatement,selItem.errorId,sucessSegSelectedErrorData,faliureSegSelectedErrorData);*/
}

function removeHTTPSYNCErrors(){
	var supportViewSegData = frmSupportView.segSupportView.data;
	var newSupportViewSegData = [];
	for(var i = 0; i < supportViewSegData.length; i++){
		var segdata = supportViewSegData[i];
		var errorCode = segdata.lblErrorCode.trim();
		var errorService = segdata.lblService.trim();
		printMessage("supportview errorCode ==>"+errorCode+", errorService ==>"+errorService);
		printMessage("supportview IF Condition ==>"+(gblSupportView.HTTP_ERROR_CODES.indexOf(errorCode) == -1 && (errorService != "Lead" || errorService != "Alert" || errorService != "LeadHistory" )));
		if(gblSupportView.HTTP_ERROR_CODES.indexOf(errorCode) == -1 && 
			(errorService !== "Lead" || errorService !== "Alert" )){
			 newSupportViewSegData.push(segdata);
		}	
	}
	if(newSupportViewSegData.length > 0){
		SupportView.segSupportView.setData(newSupportViewSegData);
      	SupportView.lblNoRecords.setVisibility(false);
	}else{
      SupportView.lblNoRecords.setVisibility(true);
    }	
}

// This method will delete the record from the error table and insert into lead or alert table respectivley
function onClickResendRecord(selItem) {
    printMessage(" supportview - Inside onClickStartDaySupportRecord - ");

    function startDaySuccesCallback(res) {
        printMessage("supportview startDaySuccesCallback  res :" + JSON.stringify(res));
        if (res !== null && res.length > 0) {
            function createStartDaySuccesscallback(res1) {
                printMessage("createStartDaySuccesscallback : res1" + JSON.stringify(res1));
                uploadStartDayFromSupportSyncSession();
            }

            function createStartDayErrorcallback(res1) {
                printMessage("supportview createStartDayErrorcallback : res1" + JSON.stringify(res1));
                //if same record tris to insert again then we will get primary key error
                //even in this case needs to remove from supportview as the record already exist in sync queue
                //deleteSupportViewRecordOffline();												
                dismissLoadingIndicator();
            }

            var startDayObj = res[0];
            printMessage("before insert startday obj : " + JSON.stringify(startDayObj));
          //  com.kony.WorkStartDayScope.WorkStartDay.create(startDayObj, createStartDaySuccesscallback, createStartDayErrorcallback, true);
        }
    }
}


function closeSupportViewPopup(){
  SupportView.flxSupportTransparant.setVisibility(false);
}

function successcallbackleadsAlertsErrorInsertion(){
  printMessage("Inside successcallbackleadsErrorInsertion "+gblLeadError);
  //updateIntegrationServiceCallFlag();
  glbInsertCounter++;
  if(glbInsertlength>glbInsertlength){

    //the below code needs call from method
    //call to upload the flag--->
    printMessage(" updateIntegrationServiceCallFlag----->>> :");
    updateIntegrationServiceCallFlag(); 
  }
}
function errorcallbackleadsAlertsErrorInsertion(){
  printMessage("Inside errorcallbackcallbackleadsErrorInsertion "+gblLeadError);
  glbInsertCounter++;
  if(glbInsertlength>glbInsertlength){

    //the below code needs call from method
    //call to upload the flag--->
    printMessage(" updateIntegrationServiceCallFlag----->>> :");
    updateIntegrationServiceCallFlag(); 
  }
}


function onClickSupportViewDeleteAll(){
  
  printMessage("onClickSupportViewDeleteAll ");
  //Delete all the tabels 
  /*var sqlQueryLAError 			= "delete from LAError;";
  executeCustomQuery(sqlQueryLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);*/
 //getAllSupportErrors();
  getLeadsAlertSupportErrors();
}

function successleadsInsertion(res){
  printMessage("inside successleadsInsertion : res" + JSON.stringify(res));
  var selIndex 	= SupportView.segSupportView.selectedIndex;
  var selItem 	= SupportView.segSupportView.selectedItems[0];
  glbServiceName 	= selItem.lblService;
  var sqlLeadAletsError ="";
  var sqlLAError ="delete from LAError"+" where id = '" + selItem.errorId+"'";
  if (glbServiceName == "Lead") {
    sqlLeadAletsError 	= "delete from leaderrors"+" where id = '" + selItem.errorId+"'";

  }else if(glbServiceName == "Alert"){
    sqlLeadAletsError 	= "delete from alertErrors"+" where id = '" + selItem.errorId+"'";
  }
  executeCustomQuery(sqlLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);
  executeCustomQuery(sqlLeadAletsError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);
}

function faliureleadsInsertion(err){
  
  printMessage("inside faliureleadsInsertion ::: "+JSON.stringify(err));
  dismissLoading();
}

  function sucessSegSelectedErrorData(res){

    printMessage("inside sucessSegSelectedErrorData : res" + JSON.stringify(res));

    if(!isEmpty(res) && res.length >0){
      var resultData = res[0];

      if (glbServiceName == "Lead") {
        leads.create(resultData, successleadsInsertion, faliureleadsInsertion, true);
      }
      else if(glbServiceName == "Alert"){
        alerts.create(resultData, successleadsInsertion, faliureleadsInsertion, true);
      }
    }
    dismissLoading();
  }
function faliureSegSelectedErrorData(err){
  
  printMessage("inside faliureSegSelectedErrorData ::: "+JSON.stringify(err));
}

function getSelectedLeadsErrorFromLocalTbl(sqlStatement,errorId,successCallbackMethod,errorCallbackMethod){
  	printMessage(" Inside - getSelectedLeadsErrorFromLocalTbl ");
  	var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	printMessage(" Inside - getSelectedLeadsErrorFromLocalTbl - sqlStatement: " + sqlStatement);		  
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

//Get Esb Errors

function getLeadsESBErrorsForAutoResend(successCallbackMethod, errorCallbackMethod){
  	printMessage(" Inside - getLeadsESBErrorsForAutoResend ");
  	var email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;	
  	var businessCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	var countryCode 		= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	/*var sqlStatement		= "SELECT leadId, authorEmailAddress, accountCode, contractCode, propertyCode, propertyName, propertyAddressLine1, propertyAddressLine2, propertyAddressLine3, propertyAddressLine4, propertyAddressLine5, propertyPostcode, propertyContactName, propertyContactPosition, propertyContactTelephone, propertyContactMobile, propertyContactEmail, propertyContactFax, ticketNotes, createdDate, createdTime, leadDeleted, updateDateTime  FROM leads WHERE countryCode = '" + countryCode + "' AND businessCode = '" + businessCode + "'";*/
	
	 var sqlStatement		= "SELECT leaderrors.* FROM leaderrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599 +"' OR LAError.errorCode = '429')  AND LAError.leadalertIds   = leaderrors.leadId";

  	
  	printMessage(" Inside - getLeadsESBErrorsForAutoResend - sqlStatement: " + sqlStatement);		  
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

function successGetLeadESBErrorsForAutoUpload(result){
  	printMessage("Inside  - successGetLeadESBErrorsForAutoUpload::: "+JSON.stringify(result));
	for(var i = 0; i < result.length; i++){
		var resultData = result[i];
		//leads.create(resultData, successleadsInsertion, faliureleadsInsertion, true);
      leads.create(resultData, successleadsInsertionData, faliureleadsInsertion, true);
	}
	dismissLoading();
}
/** Error getLeads - for uploading created leads **/
function errorGetLeadsESBErrorsForAutoUpload(result){
  	printMessage("Inside  - errorGetLeadsESBErrorsForAutoUpload");
  	 dismissLoading();
}

function getAlertsESBErrorsForAutoResend(successCallbackMethod, errorCallbackMethod){
  	printMessage(" Inside - getAlertsESBErrorsForAutoResend ");
  	//var sqlStatement		= " SELECT * FROM leaderrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599+"' OR LAError.errorCode = '429')  AND LAError.leadalertIds = leaderrors.leadId";
  	var sqlStatement		= "SELECT alerterrors.* FROM alerterrors INNER JOIN LAError ON (LAError.errorCode between '"+500+"' AND '"+599 +"' OR LAError.errorCode = '429')  AND LAError.leadalertIds   = alerterrors.alertId";

	printMessage(" Inside - getAlertsESBErrorsForAutoResend - sqlStatement: " + sqlStatement);	
  	executeCustomQuery(sqlStatement, null, successCallbackMethod, errorCallbackMethod);  	
}

function successGetAlertsESBErrorsForAutoUpload(result){
  
  	printMessage("Inside  - successGetAlertsESBErrorsForAutoUpload:" + JSON.stringify(result));
  	for(var i = 0; i < result.length; i++){
	// alerts.create(resultData, successleadsInsertion, faliureleadsInsertion, true);
      alerts.create(resultData, successleadsInsertionData, faliureleadsInsertion, true);
	}
}	

/** Error getAlert - for uploading created alerts **/
function errorGetAlertsESBErrorsForAutoUpload(result){
  	printMessage("Inside  - errorGetAlertsESBErrorsForAutoUpload: " + JSON.stringify(result));
  	
}
function successleadsInsertionData(res){
  printMessage("inside successleadsInsertionData : res" + JSON.stringify(res));
  /*var selIndex 	= SupportView.segSupportView.selectedIndex;
  var selItem 	= SupportView.segSupportView.selectedItems[0];*/
  //glbServiceName 	= selItem.lblService;
  var uniqId = "";
  if(!isEmpty(res.leadId)){
  uniqId 			=	res.leadId;
  }else{
   uniqId 			=	res.alertId;
  }
  var sqlLeadAletsError ="";
  var sqlLAError ="delete from LAError"+" where leadalertIds = '" + uniqId+"'";
  if (glbServiceName == "Lead") {
    sqlLeadAletsError 	= "delete from leaderrors"+" where leadId = '" + uniqId+"'";

  }else if(glbServiceName == "Alert"){
    sqlLeadAletsError 	= "delete from alertErrors"+" where alertId = '" + uniqId+"'";
  }
  executeCustomQuery(sqlLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);
  executeCustomQuery(sqlLeadAletsError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);
}

function getLeadsAlertSupportErrors() {
  var businessCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  var countryCode 					= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;	
  printMessage("supportview  getLeadsAlertSupportErrors ");
  sql = "select * from LAError  where  countryCode = '" + countryCode + "' AND businessCode = '"+businessCode+"' AND serviceName = '"+"Lead"+"' OR serviceName = '"+"Alert"+"'";

  printMessage(" sql ::" + sql);
  executeCustomQuery(sql, null, getLeadsAlertsSupportErrorsSuccessCallback, getAllSupportErrorsErrorCallback);
}

function getLeadsAlertsSupportErrorsSuccessCallback(res) {
  printMessage(" supportview getLeadsAlertsSupportErrorsSuccessCallback : res" + JSON.stringify(res));

  if(res.length > 0){
    showConfirmPopup("common.message.0003", supportDeleteAllErrorLogConfirmHandler, ActionDeclineChangeEmail);
  }else{
    supportDeleteAllErrorLogConfirmHandler(true);
  }
}
function supportDeleteAllErrorLogConfirmHandler(res) {
  printMessage("- Inside supportDeleteAllErrorLogConfirmHandler - res :" + res);
  if (res) {
    showLoading("common.message.1001");
    var sqlQueryLAError 			= "delete from LAError";
    executeCustomQuery(sqlQueryLAError, null, deleteSupportErrorLogSuccessCallback, deleteSupportErrorLogErrorCallback);

    dismissConfirmPopup();
    SupportView.flxSupportTransparant.setVisibility(false);
  }
}
