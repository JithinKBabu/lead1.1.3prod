/*** Author  : RI - CTS - 363601 ****/


/** Initialize Sync Module  **/
function initSyncModule(){ 	
  	printMessage("Inside - initSyncModule");
  	try {
      	sync.init(success_initSyncModule, failure_initSyncModule);
    } catch(exception) {
      	printMessage("Exception: " + exception.message);
   	}
}

/** Reset sync **/
function resetSync(){
  	printMessage("Inside - resetSync");
  	sync.reset(onSyncResetSuccessCallback, onSyncResetFailureCallback);
}

/** Sync reset success callback **/
function onSyncResetSuccessCallback(response){
  	printMessage("Inside - onSyncResetSuccessCallback");
    startSyncICabInstances();
}

/** Sync reset failure callback **/
function onSyncResetFailureCallback(response){
  	printMessage("Inside - onSyncResetFailureCallback");
}


/** Sync timer scheduler every 30 seconds - To upload local data **/
function startSyncScheduler(){
	printMessage("Inside startSyncScheduler ");
	var timeInSecs 							= LAConstants.SYNC_SCHEDULER_TIME; /** 30 secs time interval **/
	try {		
		kony.timer.schedule("SyncScheduler", schedulerSyncUploadCall, timeInSecs, true);
	} catch(error) {
		printMessage("startSyncScheduler error: " + JSON.stringify(error));
	}
}

/** To cancel sync timer scheduler **/
function cancelSyncScheduler(){
  	printMessage("Inside cancelSyncScheduler ");
  	try {		
		kony.timer.cancel("SyncScheduler");
	} catch(error) {
		printMessage("cancelSyncScheduler error: " + JSON.stringify(error));
	}
}
/** To cancel sync timer scheduler **/
function cancelPurgeDBScheduler(){
  	printMessage("Inside cancelPurgeDBScheduler ");
  	try {		
		kony.timer.cancel("PurgeDBScheduler");
	} catch(error) {
		printMessage("startPurgeDBScheduler error: " + JSON.stringify(error));
	}
}



/** Data sync - ICabInstances **/
function startSyncICabInstances(){
  	printMessage("Inside - startSyncICabInstances");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= LAConstants.SYNC_ICAB_INSTANCES;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"LAEmployees"	    :{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAiCabInstancesPc"	:{doupload:false, dodownload:true, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadHistoryScope"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAAlertScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE}
                                          };  	
  	initDataSync(syncConfigData, "LAiCabInstancesPc");
}




/** Data sync - SyncEmpService **/
function startSyncEmpService(){
  	printMessage("Inside - startSyncEmpService---->>");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= LAConstants.SYNC_EMP_DATA;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"LAEmployees"	    :{doupload:false, dodownload:true, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAiCabInstancesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadHistoryScope"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAAlertScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE}
                                          };  	
  	initDataSync(syncConfigData, "LAEmployees");
}





/** Data sync - LeadHistory **/
function startSyncLeadHistory(){
  	printMessage("Inside - startSyncLeadHistory");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= LAConstants.SYNC_LEAD_HISTORY;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"LAEmployees"	    :{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAiCabInstancesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadHistoryScope"	:{doupload:false, dodownload:true, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAAlertScope"		:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE}
                                          };  	
  	initDataSync(syncConfigData, "LALeadHistoryScope");
}

/** Added for Lead and Alerts upload **/

function startSyncLeadUpload(){
  	printMessage("Inside - startSyncLeads---->>");
  	gblShowCustomProgress				= true;
  	gblSyncCallInProgress				= LAConstants.SYNC_UPLOAD_LEAD;
  	var syncConfigData					= {};  	
  	syncConfigData.sessiontasks 		= {"LAEmployees"	    :{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAiCabInstancesPc"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadHistoryScope"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadScope"		:{doupload:true, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAAlertScope"		:{doupload:true, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE}
                                          };  	
  	initDataSync(syncConfigData, "LALeadScope");
}


/** Data sync - 15 minutes sync - upload sync **/
function schedulerSyncUploadCall(){
  printMessage(" Inside - schedulerSyncUploadCall ");
  var lastSyncDateTime				= kony.store.getItem(LAConstants.LAST_UPLOAD_SYNC_DATE_TIME);
  printMessage("Inside - schedulerSyncUploadCall lastSyncDateTime:: "+lastSyncDateTime);
  var dateDiffInMins 					= 0;
  if (!isEmpty(lastSyncDateTime)) {
    //Previous formatt
    //var currentDateTime 			= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
    var currentDateTime 			= getDateTimeFormat(new Date(), LAConstants.LEAD_DATE_SYNC_FORMAT);
    printMessage("Last sync DateTime: " + lastSyncDateTime + " current DateTime: " + currentDateTime);
    dateDiffInMins 					= getDateTimeDiffInMins(currentDateTime, lastSyncDateTime);
    dateDiffInMins 					= parseInt(dateDiffInMins);
    printMessage(" Inside - schedulerSyncUploadCall dateDiffInMins: " + dateDiffInMins);
  } else {

    updateLastSyncDate();

  }
  //if (isNetworkAvailable() === true) {
  printMessage(" Inside - schedulerSyncUploadCall - gblIsIntServiceInProgress:gblIsIntServiceInProgress " + gblIsIntServiceInProgress);
  printMessage(" Inside - schedulerSyncUploadCall - gblIsIntServiceInProgress: " + (dateDiffInMins >= LAConstants.SYNC_UPLOAD_SYNC_TIMER));
  if (dateDiffInMins >= LAConstants.SYNC_UPLOAD_SYNC_TIMER && gblIsIntServiceInProgress === false){	
    printMessage(" Inside - schedulerSyncUploadCall dateDiffInMins   " );
    updateLastSyncDate();
    gblIsIntServiceInProgress				= true;
    gblUploadTicket							= {};
    gblUploadTicket.isLeadServiceFinished	= false;
    gblUploadTicket.isAlertServiceFinished	= false;
    gblUploadTicket.succeededLeadsIds		= [];
    gblUploadTicket.succeededAlertsIds		= [];
    gblUploadTicket.type					= "";
    if (isNetworkAvailable() === true) {
      //constructLeadsInputForSyncUpload(); commented to make this as sync upload
      startSyncLeadUpload(); 
      //constructLeadsErrorInputForSyncUpload();
    }else{
      
      printMessage("Inside - schedulerSyncUploadCall Internet is not existed !!! refreshing updateIntegrationServiceCallFlag ");
      gblUploadTicket.isLeadServiceFinished		= true;
      gblUploadTicket.isAlertServiceFinished	= true;
      updateIntegrationServiceCallFlag();

    }
  }else{
    // Refreshing timer, if timer value more than 3 minutes
    if(dateDiffInMins > 3){
      printMessage("Inside - schedulerSyncUploadCall dateDiffInMins value is more than 3 minutes !!! refreshing updateIntegrationServiceCallFlag ");
      gblUploadTicket.isLeadServiceFinished		= true;
      gblUploadTicket.isAlertServiceFinished	= true;
      updateIntegrationServiceCallFlag();
    }
  }
}

/** Data sync - 15 minutes sync - scheduler sync **/
/*function startSyncLeadsAndAlertsUpload() {
  	printMessage(" Invoking 15 mins delta changes - 15 minutes scheduler call");
    gblShowCustomProgress				= false;
    gblSyncCallInProgress				= LAConstants.SYNC_LEAD_AND_ALERT_UPLOADS;
    var syncConfigData					= {};  	
    syncConfigData.sessiontasks 		= {"LALeadScope"		:{doupload:true, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LAAlertScope"		:{doupload:true, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE},
                                           "LALeadHistoryScope"	:{doupload:false, dodownload:false, uploaderrorpolicy:LAConstants.SYNC_ERR_POLICY_CONTINUE}                                           
                                          }; 	
    initDataSync(syncConfigData, "LALeadScope");
}*/

/** Start sync session **/
function initDataSync(configObj, scopeId){
  	printMessage(" Inside - initDataSync ");
  	gblIsSyncInProgress					= true;  	
  	if (gblShowCustomProgress === true) {
      	var scopeNameStr				= gblScopeMappings[scopeId];	
      //Updating loading pop up to loading indicator
      //showLoading("common.message.1001"); 
      	if(gblSyncCallInProgress === LAConstants.SYNC_LEAD_HISTORY){
          showLoading("common.message.2001");
        }else{
          showLoading("common.message.1001");  
        }
      
    } 
  	configObj									= (isEmpty(configObj)) ? {} : configObj;
  	configObj.userid							= LAConstants.SYNC_USER_ID;
	configObj.password							= LAConstants.SYNC_PASSWORD;
	configObj.appid 							= LAConstants.SYNC_APP_ID;
	configObj.serverhost 						= (LAConstants.APP_SERVER_ON === "CLOUD") ? LAConstants.SYNC_SERVER_HOST_CLOUD : LAConstants.SYNC_SERVER_HOST_PREMISES;
    configObj.serverport 						= "";
  	configObj.issecure 							= (LAConstants.APP_SERVER_ON === "CLOUD") ? LAConstants.SYNC_IS_SECURE_CLOUD : LAConstants.SYNC_IS_SECURE_PREMISES;
  	configObj.batchsize 						= LAConstants.SYNC_BATCH_SIZE;
	configObj.chunksize 						= LAConstants.SYNC_CHUNK_SIZE;
  	configObj.networktimeout 					= LAConstants.SYNC_TIMEOUT;
	
  	//sync life cycle callbacks
	configObj.onsyncstart 						= onSyncStartCallback;
	configObj.onscopestart 						= onScopeStartCallback;
	configObj.onscopecerror 					= onScopeErrorCallback;
	configObj.onscopesuccess 					= onScopeSuccessCallback;
	configObj.onuploadstart 					= onUploadStartCallback;
	configObj.onuploadsuccess 					= onUploadSuccessCallback;
  
  	configObj.ondownloadstart 					= onDownloadStartCallback;
	configObj.ondownloadsuccess 				= onDownloadSuccessCallback;
  	configObj.onbatchstored 					= onBatchstoredCallback;
	configObj.onbatchprocessingstart 			= onBatchProcessingStartCallback;
	configObj.onbatchprocessingsuccess 			= onBatchProcessingSuccessCallback;
	configObj.onuploadbatchsuccess 				= onUploadBatchSuccessCallback;
  	configObj.onchunkstart 						= onChunkStartCallback;
	configObj.onchunksuccess 					= onChunkSuccessCallback;
	configObj.onchunkerror 						= onChunkErrorCallback;	
  
  	configObj.onperformupgradesuccess  			= onPerformUpgradeSuccessCallback;
	configObj.onupgradescriptsdownloadstart 	= onUpgradeScriptsDownloadStartCallback;
	configObj.onupgradescriptsdownloadsuccess 	= onUpgradeScriptsDownloadSuccessCallback;
	configObj.onupgradescriptsdownloaderror 	= onUpgradeScriptsDownloadErrorCallback;
	configObj.onupgradescriptsexecutionstart 	= onUpgradeScriptsExecutionStartCallback;
	configObj.onupgradescriptsexecutionsuccess 	= onUpgradeScriptsExecutionSuccessCallback;
	configObj.onupgradescriptsexecutionerror 	= onUpgradeScriptsExecutionErrorCallback;
	configObj.onperformupgradeerror 			= onPerformUpgradeErrorCallback;
	configObj.onperformupgradestart 			= onPerformUpgradeStartCallback;
	configObj.onupgraderequired 				= onUpgradeRequiredCallback;
  
	configObj.onsyncsuccess 					= onSyncSuccessCallback;
	configObj.onsyncerror 						= onSyncErrorCallBack;
  
    if (scopeId == "LALeadScope") {
    	configObj.removeafterupload = {"LALeadScope":["leads"],"LAAlertScope":["alerts"]}; // scope name and table --> for us {"LALeadScope":["leads"]
  	}
  
  	sync.startSession(configObj);
}

/** Sync start callback **/
function onSyncStartCallback(response){
  	printMessage(" Inside - onSyncStartCallback ");
}

/** Sync scope start callback **/
function onScopeStartCallback(response){
  	printMessage(" Inside - onScopeStartCallback "+gblShowCustomProgress);
  	printMessage(" response " + JSON.stringify(response));
  	if (gblShowCustomProgress === true) {
      	var scopeNameStr				= (!isEmpty(response) && !isEmpty(response.currentScope)) ? gblScopeMappings[response.currentScope] : "";
      	if (!isEmpty(scopeNameStr)){
          	updateMessageInProgressPopup(scopeNameStr);
        }
      	updateProgressInProgressPopup("common.message.syncInProgress");
    } 
}

/** Sync scope error callback **/
function onScopeErrorCallback(response){
	printMessage(" Inside - onScopeErrorCallback ");
}

/** Sync scope success callback **/
function onScopeSuccessCallback(response){
  	printMessage(" Inside - onScopeSuccessCallback ");
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncDone");
    } 
}

/** Sync upload start callback **/
function onUploadStartCallback(response){
	printMessage("Inside - onUploadStartCallback ");
 /* 	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncUploading");
    }
  
  	var reqData 									= response.uploadRequest;
    if(isEmpty(reqData.clientcontext)){
		reqData.clientcontext 						= {};
    }      
  
  	printMessage("ReqData " + JSON.stringify(reqData));
  	return reqData;*/
  
   	if (gblShowCustomProgress === true) {
      	//updateProgressInProgressPopup("common.message.syncUploading");
    }
  
  	var reqData 									= response.uploadRequest;
    if(isEmpty(reqData.clientcontext)){
		reqData.clientcontext 						= {};
    }     

  reqData.clientcontext.client_id 					= LAConstants.INTEGRATION_CLIENT_ID;
  reqData.clientcontext.client_secret 				= LAConstants.INTEGRATION_CLIENT_SECRET;
 
  reqData.clientcontext.businessCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  reqData.clientcontext.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;

  reqData.clientcontext.email 						= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  reqData.clientcontext.languageCode				= (!isEmpty(kony.store.getItem(LAConstants.SAVED_LANG_CODE))) ? kony.store.getItem(LAConstants.SAVED_LANG_CODE) : LAConstants.DEF_LANGUAGE_CODE;
  //(!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
  reqData.clientcontext.sourceApplication 			= "LAAS";


  printMessage("Inside - onUploadStartCallback ReqData " + JSON.stringify(reqData));
  return reqData;
  
}

/** Sync upload success callback **/
function onUploadSuccessCallback(response){
  	printMessage("Inside - onUploadSuccessCallback ");
}

/** Sync download start callback **/
function onDownloadStartCallback(response){
  	printMessage("Inside - onDownloadStartCallback ");
  	printMessage("Response " + JSON.stringify(response));
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncDownloading");
    }
  	var reqData 									= response.downloadRequest;
  	if(isEmpty(reqData.clientcontext)){
		reqData.clientcontext 						= {};
    }
   
   printMessage("Inside - onDownloadStartCallback "+gblBasicInfo.businessCode);
   printMessage("Inside - onDownloadStartCallback "+gblBasicInfo.countryCode);
   if(!isEmpty(gblBasicInfo.selICabServerCode)){
      var countryandBusinessCodes= gblBasicInfo.selICabServerCode.split("_");
      gblBasicInfo.countryCode  	=countryandBusinessCodes[0];
      gblBasicInfo.businessCode   	=countryandBusinessCodes[1];
     //Testing 102 issue
      /*gblBasicInfo.countryCode  ="NO";
      gblBasicInfo.businessCode ="K";*/
      printMessage("Inside - onDownloadStartCallback-->> "+gblBasicInfo.businessCode);
      printMessage("Inside - onDownloadStartCallback-->> "+gblBasicInfo.countryCode);
   }
  
  printMessage("Inside - onDownloadStartCallback-->> " + JSON.stringify(gblBasicInfo));
  
  	reqData.clientcontext.client_id 				= LAConstants.INTEGRATION_CLIENT_ID;
    reqData.clientcontext.client_secret 			= LAConstants.INTEGRATION_CLIENT_SECRET;
  	reqData.clientcontext.businessCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.businessCode)) ? gblBasicInfo.businessCode : LAConstants.DEF_BUSINESS_CODE;
  	reqData.clientcontext.countryCode 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.countryCode)) ? gblBasicInfo.countryCode : LAConstants.DEF_COUNTRY_CODE;
  	reqData.clientcontext.languageCode				= (!isEmpty(kony.store.getItem(LAConstants.SAVED_LANG_CODE))) ? kony.store.getItem(LAConstants.SAVED_LANG_CODE) : LAConstants.DEF_LANGUAGE_CODE;
      //(!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.languageCode)) ? gblBasicInfo.languageCode : LAConstants.DEF_LANGUAGE_CODE;
    reqData.clientcontext.sourceApplication 			= "LAAS";
  	printMessage("Inside - onDownloadStartCallback gblIsLaunchedAsStandAloneApp "+gblIsLaunchedAsStandAloneApp);
    printMessage("Inside - onDownloadStartCallback gblBasicInfo.email "+gblBasicInfo.email);
  	if(gblSyncCallInProgress === LAConstants.SYNC_ICAB_INSTANCES){
     
      if(gblIsLaunchedAsStandAloneApp === false){
        	reqData.clientcontext.email 			= gblBasicInfo.email;	
      }else{
        reqData.clientcontext.email 				= gblConfiguredEmails;	
      }
  	}else{
      	reqData.clientcontext.email 				= (!isEmpty(gblBasicInfo) && !isEmpty(gblBasicInfo.email)) ? gblBasicInfo.email : LAConstants.DEF_EMAIL_ADDRESS;
  	}
  
  	if (gblSyncCallInProgress === LAConstants.SYNC_LEAD_HISTORY){
      	var dateRange								= getDateRangeForNextPage();
      	reqData.clientcontext.createdFromDate 		= dateRange.from; //2017-06-20
		reqData.clientcontext.createdToDate 		= dateRange.to;
      	//reqData.clientcontext.employeeCodeView 	= "123456";
      	//reqData.clientcontext.employeeEmailView 	= "another.technician@rentokil-initial.com";
      	reqData.clientcontext.maximumNumberOfRows 	= "100";
      	reqData.clientcontext.startingRowNumber 	= "0";
      	/*reqData.clientcontext.countryCode 			= "k";
  		reqData.clientcontext.languageCode			= "K";*/
    }  
  
  	printMessage("ReqData " + JSON.stringify(reqData));
  	return reqData;
}

/** Sync download success callback **/
function onDownloadSuccessCallback(response){
  	printMessage("Inside - onDownloadSuccessCallback");
  	printMessage("Response" + JSON.stringify(response));
}

/** Sync batch stored callback **/
function onBatchstoredCallback(response){
  	printMessage("Inside - onBatchstoredCallback");
}

/** Sync batch processing start callback **/
function onBatchProcessingStartCallback(response){
  	printMessage("Inside - onBatchProcessingStartCallback");
}

/** Sync batch processing success callback **/
function onBatchProcessingSuccessCallback(response){
  	printMessage("Inside - onBatchProcessingSuccessCallback response "+JSON.stringify(response));
}

/** Sync batch upload success callback **/
function onUploadBatchSuccessCallback(outputparams){
  printMessage("Inside - onUploadBatchSuccessCallback");
  printMessage("Inside - onUploadBatchSuccessCallback");
  kony.print("Inside onuploadbatchsuccessCallback - outputparams::" + JSON.stringify(outputparams));

  if (outputparams !== null || outputparams !== "") {
    var isUploadError = isUploadServiceError(outputparams);
    var failedRowInfo = outputparams.uploadcontext.failedrowinfo;
    printMessage("isUploadError :" + isUploadError);
    printMessage("onUploadBatchSuccessCallback failedRowInfo :" + JSON.stringify(failedRowInfo));
    if (isUploadError) {
      
      rollBackHistoryDeviceDataChanges(failedRowInfo);
    }
  
 }
}

/** Delete failed record from lead and alerts table **/
function rollBackHistoryDeviceDataChanges(failedRowInfo) {
     printMessage("rollBackHistoryDeviceDataChanges :--");
   // kony.print("Inside rollBackDeviceDataChanges - failedRowInfo::" + JSON.stringify(failedRowInfo));
    if (failedRowInfo !== null && failedRowInfo.length > 0) {
        var temp = {};
        var whereClause = "";
      	var pkObj ="";
        printMessage("rollBackHistoryDeviceDataChanges for loop start-");
        for (var i = 0; i < failedRowInfo.length; i++) {
            temp = failedRowInfo[i];
            kony.print("temp.type::" + temp.type);
            if (temp.type == "leads") {
                gblLeadsObject = temp;
                pkObj = temp.key;
                printMessage("rollBackHistoryDeviceDataChanges lead pkObj::" + JSON.stringify(pkObj));
              	whereClause = "select * from leads where leadId = '" + pkObj.leadId + "'";
                printMessage("whereClause:: lead" + whereClause);
                executeCustomQuery(whereClause, null, getUploadLeadErrorRecordsSuccesCallback, getUploadErrorRecordsFailureCallback);
                //leads.prototype.rollbackPendingLocalChangesByPK(pkObj, rollBackSuccesCallback, rollBackErrorCallback);
              	leads.rollbackPendingLocalChangesByPK(pkObj, rollBackSuccesCallback, rollBackErrorCallback);
            } else if(temp.type == "alerts"){
              
              	gblAlertObject = temp;
                pkObj = temp.key;

                printMessage("rollBackHistoryDeviceDataChanges alert pkObj::" + JSON.stringify(pkObj));
              	whereClause = "select * from alerts where alertId = '" + pkObj.alertId + "'";
                printMessage("whereClause alert ::" + whereClause);
                executeCustomQuery(whereClause, null, getUploadAlertErrorRecordsSuccesCallback, getUploadErrorRecordsFailureCallback);
                //alerts.prototype.rollbackPendingLocalChangesByPK(pkObj, rollBackSuccesCallback, rollBackErrorCallback);
                alerts.rollbackPendingLocalChangesByPK(pkObj, rollBackSuccesCallback, rollBackErrorCallback);
            }
        }
    }
}


function rollBackSuccesCallback(res) {
    printMessage("Inside rollBackSuccesCallback - res::" + JSON.stringify(res));

}

function rollBackErrorCallback(res) {
    printMessage("Inside rollBackErrorCallback - res::" + JSON.stringify(res));
}

/**Success Call back method getUploadLeadErrorRecordsSuccesCallback **/
function getUploadLeadErrorRecordsSuccesCallback(res) {
  printMessage("Inside getUploadLeadErrorRecordsSuccesCallback - res::" + JSON.stringify(res));

  var leadsObject 		= gblLeadsObject;
  printMessage("getUploadLeadErrorRecordsSuccesCallback leadsObject ::" + JSON.stringify(leadsObject));
  var jsonString 			= JSON.stringify(leadsObject);
  var errorCode 			= getUploadSupportErrorCode(jsonString);
  gblErrorNumber = errorCode;
  printMessage("getUploadLeadErrorRecordsSuccesCallback errorCode ::" + errorCode);
  if (res !== null && res.length > 0 && gblRetryErrorCodes.indexOf(errorCode) == -1) {
    logUploadSupportSyncErrorsIntoDB(errorCode, "Lead", "ServiceError", res);
  }
}

/**Success Call back method getUploadAlertErrorRecordsSuccesCallback **/
function getUploadAlertErrorRecordsSuccesCallback(res) {
  printMessage("Inside getUploadAlertErrorRecordsSuccesCallback - res::" + JSON.stringify(res));

  var alertObject 		= gblAlertObject;
  printMessage("getUploadAlertErrorRecordsSuccesCallback alertObject::" + JSON.stringify(alertObject));
  var jsonString 			= JSON.stringify(alertObject);
  var errorCode 			= getUploadSupportErrorCode(jsonString);
  gblErrorNumber = errorCode;
  printMessage("getUploadAlertErrorRecordsSuccesCallback gblErrorNumber ::" + gblErrorNumber);
  if (res !== null && res.length > 0 && gblRetryErrorCodes.indexOf(errorCode) == -1) {
    logUploadSupportSyncErrorsIntoDB(errorCode, "Alert", "ServiceError", res);
  }
}

/** Sync chunk start callback **/
function onChunkStartCallback(response){
  	printMessage("Inside - onChunkStartCallback");
}

/** Sync chunk success callback **/
function onChunkSuccessCallback(response){
  	printMessage("Inside - onChunkSuccessCallback");
}

/** Sync chunk error callback **/
function onChunkErrorCallback(response){
  	printMessage("Inside - onChunkErrorCallback");
}

/** Sync success callback **/
function onSyncSuccessCallback(response){
  	printMessage("Inside - onSyncSuccessCallback");
    printMessage("Inside - onSyncSuccessCallback---->>>>"+JSON.stringify(response));
  	//printMessage("Inside - onSyncSuccessCallback---->>>>"+response.isErrorFlag);
  	//printMessage("Inside - onSyncSuccessCallback----errorInfo >>>>"+response.errorInfo);
  	if (gblShowCustomProgress === true) {
      	updateProgressInProgressPopup("common.message.syncFinished");
    }
  
  	var today 						= new Date();
	var dateStr 					= today.toLocaleDateString();
	var timeStr 					= today.toLocaleTimeString();
  	var syncDateStr					= dateStr + " " + timeStr;
  	kony.store.setItem(LAConstants.DISPLAY_SYNC_DATE_TIME, syncDateStr); /** To display in settings screen **/
  	gblIsSyncInProgress				= false;
  
   if (gblShowCustomProgress === true) {
    //changes icab instance and emp service
    printMessage("Inside - onSyncSuccessCallback gblSyncCallInProgress"+gblSyncCallInProgress);
    if (gblSyncCallInProgress === LAConstants.SYNC_ICAB_INSTANCES){
      printMessage("Inside - onSyncSuccessCallback SYNC_ICAB_INSTANCES ");
      showLoading("common.message.1001");
      //Updating last sync date time
      updateLastSyncDate();
      dismissProgressMessage();
      getiCabInstancesFromLocalTbl();
    } else if (gblSyncCallInProgress === LAConstants.SYNC_EMP_DATA){
      printMessage("Inside - onSyncSuccessCallback SYNC_EMP_DATA ");
      lastSyncDate 			= getDateTimeFormat(new Date(), "YYYY-MM-DD");
      kony.store.setItem(LAConstants.LAST_SYNC_DATE, lastSyncDate);

      lastSyncDateTime 		= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
      kony.store.setItem(LAConstants.LAST_SYNC_DATE_TIME, lastSyncDateTime);
      //Updating last sync date time
      updateLastSyncDate();
      getEmployeeFromLocalTbl();
    } 

    else if (gblSyncCallInProgress === LAConstants.SYNC_LEAD_HISTORY){
      printMessage("Inside - onSyncSuccessCallback SYNC_LEAD_HISTORY ");
      showLoading("common.message.1001");
      dismissProgressMessage();
      //Updating last sync date time
      updateLastSyncDate();
      getLeadHistoryFromLocalTbl();
    } else if (gblSyncCallInProgress === LAConstants.SYNC_UPLOAD_LEAD){

      printMessage("Inside - onSyncSuccessCallback SYNC_UPLOAD_LEAD ");
      // showLoading("common.message.1001");
      dismissProgressMessage();
      dismissLoading();
      gblUploadTicket.isLeadServiceFinished		= true;
      updateIntegrationServiceCallFlag();
      
    }
  } else {
    dismissLoading();
  }
}

/** Sync error callback **/
function onSyncErrorCallBack(response){
  	printMessage("Inside - onSyncErrorCallBack Response ::" + JSON.stringify(response));
  	gblIsSyncInProgress			= false;
  if (gblShowCustomProgress === true) {

    if (gblSyncCallInProgress === LAConstants.SYNC_ICAB_INSTANCES){
      printMessage("Inside - onSyncFailureCallback SYNC_ICAB_INSTANCES ");
      showLoading("common.message.1001");
      dismissProgressMessage();
      showMessagePopup("common.message.0001", ActionOnMessagePopupOkBtn);
    }else if (gblSyncCallInProgress === LAConstants.SYNC_EMP_DATA){
      printMessage("Inside - onSyncErrorCallBackSYNC_EMP_DATA ");
      //handling the if the employee service is failed
      if (!isEmpty(response) && !isEmpty(response.errorNumber) || parseInt(response.errorNumber) === 20004) {  
        showLoading("common.message.1001");
        dismissProgressMessage();
        kony.store.setItem(LAConstants.SELECTED_SOURCE_VALUE, "");
        clearStoreData();
        showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      }else{
        dismissProgressMessage();
        kony.store.setItem(LAConstants.SELECTED_SOURCE_VALUE, "");
        clearStoreData();
        showMessagePopup("common.message.0001", ActionOnMessagePopupOkBtn);
        
      }
    }else  if (gblSyncCallInProgress === LAConstants.SYNC_LEAD_HISTORY){
      printMessage("Inside - onSyncerrorCallback calling the method to create entry-->>5");
      showMessagePopupValidation("common.message.2003");
      logDownloadSupportSyncErrors(response);
      getLeadHistoryFromLocalTbl();
    
    }else if (gblSyncCallInProgress === LAConstants.SYNC_UPLOAD_LEAD){
      printMessage("Inside - onSyncerrorCallback SYNC_UPLOAD_LEAD ");
      gblUploadTicket.isLeadServiceFinished		= true;
      updateIntegrationServiceCallFlag();
      dismissLoading();
    }else if (gblSyncCallInProgress === LAConstants.SYNC_UPLOAD_ALERT){
      
      printMessage("Inside - onSyncerrorCallback SYNC_UPLOAD_ALERT");
      gblUploadTicket.isLeadServiceFinished		= true;
      updateIntegrationServiceCallFlag();
      dismissLoading();
    }
  }
}

/** Stop sync session **/
function stopSyncSession(){
  	sync.stopSession(onSyncStopSessionCallback);
}

/** Sync stop session callback **/
function onSyncStopSessionCallback(response){
  	gblIsSyncInProgress		= false;
  	dismissProgressMessage();
  	dismissLoading();
}

/** Sync upgrade success callback **/
function onPerformUpgradeSuccessCallback(response){
  
}

/** Sync upgrade scripts download callback **/
function onUpgradeScriptsDownloadStartCallback(response){
  
}

/** Sync upgrade scripts download success callback **/
function onUpgradeScriptsDownloadSuccessCallback(response){
  
}

/** Sync upgrade scripts download error callback **/
function onUpgradeScriptsDownloadErrorCallback(response){
  
}

/** Sync upgrade scripts execution start callback **/
function onUpgradeScriptsExecutionStartCallback(response){
  
}

/** Sync upgrade scripts execution success callback **/
function onUpgradeScriptsExecutionSuccessCallback(response){
  
}

/** Sync upgrade scripts execution error callback **/
function onUpgradeScriptsExecutionErrorCallback(response){
  
}

/** Sync perform upgrade error callback **/
function onPerformUpgradeErrorCallback(response){
  
}

/** Sync perform upgrade start callback **/
function onPerformUpgradeStartCallback(response){
  
}

/** Sync upgrade required callback **/
function onUpgradeRequiredCallback(response){
  
}


/** Log errors in Supportview Logic****/
/*function getUploadLeadErrorRecordsSuccesCallback(res) {
    printMessage("Inside getUploadLeadErrorRecordsSuccesCallback - res::" + JSON.stringify(res));
	
    var leadsObject 		= gblLeadsObject;
    printMessage("getUploadLeadErrorRecordsSuccesCallback leadsObject ::" + JSON.stringify(leadsObject));
    var jsonString 			= JSON.stringify(leadsObject);
    var errorCode 			= getUploadSupportErrorCode(jsonString);
  	gblErrorNumber = errorCode;
    printMessage("getUploadLeadErrorRecordsSuccesCallback errorCode ::" + errorCode);
    if (res !== null && res.length > 0 && gblRetryErrorCodes.indexOf(errorCode) == -1) {
        //var startDayInfo = res[0];
     
       //printMessage("getUploadLeadErrorRecordsSuccesCallback startDayInfo ::" + JSON.stringify(startDayInfo));
        //logUploadSupportSyncErrors(errorCode, "Lead", "ServiceError", res);
      logUploadSupportSyncErrorsIntoDB(errorCode, "Lead", "ServiceError", res);
    }
}*/

/*function getUploadAlertErrorRecordsSuccesCallback(res) {
    printMessage("Inside getUploadAlertErrorRecordsSuccesCallback - res::" + JSON.stringify(res));
	
    var alertObject 		= gblAlertObject;
    printMessage("getUploadAlertErrorRecordsSuccesCallback alertObject::" + JSON.stringify(alertObject));
    var jsonString 			= JSON.stringify(alertObject);
    var errorCode 			= getUploadSupportErrorCode(jsonString);
  	gblErrorNumber = errorCode;
   printMessage("getUploadAlertErrorRecordsSuccesCallback gblErrorNumber ::" + gblErrorNumber);
    if (res !== null && res.length > 0 && gblRetryErrorCodes.indexOf(errorCode) == -1) {
        //var startDayInfo = res[0];
        //logUploadSupportSyncErrors(errorCode, "Alert", "ServiceError", res);
        logUploadSupportSyncErrorsIntoDB(errorCode, "Alert", "ServiceError", res);
    }
}*/

function getUploadErrorRecordsFailureCallback(res) {
    kony.print("Inside getUploadErrorRecordsFailureCallback - res::" + JSON.stringify(res));
}


