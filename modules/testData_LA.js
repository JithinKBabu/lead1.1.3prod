/*** Author  : RI - CTS - 363601 ****/


/** Test Data **/
var gblTestData			= [];
gblTestData.push({"date":"05/07/2016", "status":"Closed", "customer":"Tesco", "ticketNumber":"12345", "notes":"Test notes. Sample data. Please ignore this."});
gblTestData.push({"date":"06/07/2016", "status":"Closed", "customer":"Sainsburry", "ticketNumber":"12345", "notes":"Please ignore this sample data. Please ignore this sample data. Please ignore this sample data."});
gblTestData.push({"date":"06/07/2016", "status":"Open", "customer":"Park Street", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"08/07/2016", "status":"Open", "customer":"Morrisons", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"15/07/2016", "status":"Closed", "customer":"Asda", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"20/07/2016", "status":"Cancelled", "customer":"Tesco", "ticketNumber":"12345", "notes":"Please ignore this sample data."});
gblTestData.push({"date":"01/08/2016", "status":"Open", "customer":"Asda", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"03/08/2016", "status":"Open", "customer":"Park Street", "ticketNumber":"12345", "notes":"Please ignore this sample data."});
gblTestData.push({"date":"11/09/2016", "status":"Cancelled", "customer":"Tesco", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"05/10/2016", "status":"Open", "customer":"Sainsburry", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"06/10/2016", "status":"Open", "customer":"Tesco", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"01/11/2016", "status":"Open", "customer":"Sainsburry", "ticketNumber":"12345", "notes":"Please ignore this sample data."});
gblTestData.push({"date":"05/11/2016", "status":"Open", "customer":"Asda", "ticketNumber":"12345", "notes":"Test notes. Sample data. Please ignore this."});
gblTestData.push({"date":"12/11/2016", "status":"Open", "customer":"Park Street", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"28/11/2016", "status":"Cancelled", "customer":"Tesco", "ticketNumber":"12345", "notes":"Test notes. Sample data."});
gblTestData.push({"date":"05/12/2016", "status":"Open", "customer":"Park Street", "ticketNumber":"12345", "notes":"Please ignore this test data."});