/*** Author  : RI - CTS - 363601 ****/


/** To show loading indicator **/
function showLoading(messageKey){
	var lodingText 		= getI18nString(messageKey);
	try {
		kony.application.showLoadingScreen(loadingSkin, lodingText,"center", true,true, null);
	} catch(e) {
		printMessage("Error while showing loading indicator: " + e);
	}	
}

/** To dismiss loading indicator **/
function dismissLoading(){
	kony.application.dismissLoadingScreen();
}

/** To get i18n localized string for given key **/
function getI18nString(key){
	return key ? kony.i18n.getLocalizedString(key) : "";
}

/** To remove whitespaces  **/
function trim(inputString){
	var outputString 	= "";
	if(inputString !== null){
		outputString 	= kony.string.trim(inputString);
	}
	return outputString;
}

/** To check a string is valid or not  **/
function isEmpty(val){
	return (val === undefined || val === null || val === '' || val.length <= 0 || (typeof(val) === "object" && Object.keys(val).length === 0)) ? true : false;
}

/** To make given string to uppercase  **/
function toUpperCase(inputString){
	var outputVal = "";
	if(!isEmpty(inputString)){
		outputVal = inputString.toUpperCase();
	}
	return outputVal;
}

/** To make given string to uppercase  **/
function toLowerCase(inputString){
	var outputVal = "";
	if(!isEmpty(inputString)){
		outputVal = inputString.toLowerCase();
	}
	return outputVal;
}

/** To add a leading zero on given string  **/
function appendLeadingZero(data) {
	return (parseInt(data) < 10) ? '0' + data : data;
}

/** To validate string length limit  **/
function isValidCharsCountInString(textStr, charLimit){
	var boolResult			= true;
	if (!isEmpty(textStr)){
		if (textStr.length > charLimit){
			boolResult		= false;
		}
	}
	return boolResult;
}

/** To get Current locale  **/
function getCurrentLocale(){
	var currentLocale = "";
	try {
		currentLocale = kony.i18n.getCurrentLocale();		
	} catch(i18nError) {
		printMessage("Exception While getting currentLocale  : "+i18nError );
	}	
	return currentLocale;
}

/** To check network availability  **/
function isNetworkAvailable(){	
	return kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY);
}

/** To exit application  **/
function exitApplication(){
	kony.application.exit();
}

/** To print a message on console  **/
function printMessage(message){
  	var finalMsg				= "*** LEADS & ALERTS *** :: " + message;
	kony.print(finalMsg);
	
}

/** To get a matching record from given array of elements  **/
function getMatchingItemFromArray(recordsArray, matchingKey, matchingValue){
	var matchingItem			= null;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === matchingValue){
				matchingItem	= aRecord;
				break;
			}
		}
	}
	
	return matchingItem;
}

/** To find index of a record from an array  **/
function getItemIndexFromArray(recordsArray, record){
	var index 	= (!isEmpty(recordsArray) && !isEmpty(record)) ? recordsArray.indexOf(record) : -1;
	return index;
}

/** To remove a record from an array  **/
function removeItemFromArray(recordsArray, record){
	var index	= getItemIndexFromArray(recordsArray, record);
	if(index !== -1) {
		recordsArray.splice(index, 1);
	}
}

/** To add a record in an array  **/
function addItemToArray(recordsArray, record, index){
	recordsArray.splice(index, 0, record);
}

/** To search a record in given array  **/
function isRecordExist(record, recordsArray, matchingKey, matchingKey2){
	var result					= false;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === record[matchingKey] || (!isEmpty(matchingKey2) && aRecord[matchingKey2] === record[matchingKey2])){
				result			= true;
				break;
			}
		}
	}
	
	return result;
}

/** To get todays date string for comparison **/
function getTodaysDateStr(){
  	var today	= new Date();
  	var yy		= today.getFullYear();
  	var mm		= today.getMonth();
  	var dd		= today.getDate();
  	mm			= mm + 1;
  	mm			= (mm <= 9) ? ("0" + mm.toString()) : mm.toString();
  	dd			= (dd <= 9) ? ("0" + dd.toString()) : dd.toString(); 
  	yy			= yy.toString();
  	var dateStr	= yy + "-" + mm + "-" + dd;
  	return dateStr;
}

/** To get current time string (hh:mm) **/
function getCurrentTimeStr(){
  	var today	= new Date();
  	var hh		= today.getHours();
  	var mm		= today.getMinutes();
  	hh			= (hh <= 9) ? ("0" + hh.toString()) : hh.toString(); 
  	mm			= (mm <= 9) ? ("0" + mm.toString()) : mm.toString(); 
  	var timeStr	= hh + ":" + mm;
  	return timeStr;
}

/** To get current time string (hh:mm) **/
function getPurgeCurrentDateTimeFormat(){
  
   var currentDate 	= 	getTodaysDateStr();
   var timeStr 		=	getCurrentTimeStr();
  
  var requitedFrmt 	=  getTodaysDateStr()+" "+getCurrentTimeStr();
  return requitedFrmt;
}

/** To identify matched record from an array  **/
function getMatchedRecordFromArray(recordsArray, matchingKey, matchingValue){
	var resultRecord			= null;
	if (!isEmpty(recordsArray) && recordsArray.length > 0){
		var arrayLength			= recordsArray.length;
		var aRecord				= null;
		for (var i = 0; i < arrayLength; i++){
			aRecord				= recordsArray[i];
			
			if (aRecord[matchingKey] === matchingValue){
				resultRecord	= aRecord;
				break;
			}
		}
	}
	return resultRecord;
}

/** To add space in the beiging of given string **/
function prefixWhiteSpace(strText, spaceCount){
	spaceCount		= (!isEmpty(spaceCount)) ? spaceCount : 3;
  	var spaceStr 	= "";
  	for (var i = 0; i < spaceCount; i++) {
      	spaceStr	+= "&nbsp;";
    }
  	var newStr		= spaceStr + strText;
  	return newStr;
}

function sendMail(toMail){
    printMessage("Inside sendMail ::::"+toMail);
    var torecipients 		= [toMail];
    var ccrecipients 		= [];
    var bccrecipients 		= [];
    var subject 			= "Reg :";
    var messagebody 		= "Hi Team,";
    var ismessagebodyhtml 	= false;
    kony.phone.openEmail(torecipients, ccrecipients, bccrecipients, subject, messagebody, ismessagebodyhtml);
}

/*****************************************************************
  Purpose : To date format(Ex: 12/23/2013,21 August,2013)
  Inputs  : Need date as object or string (Ex : new Date(yyyy,mm,dd,hh,mm,ss) or "02/04/2013 12:30:30")
             format can give as input      
  Ex:   
  
  MM/DD/YYYY    - 12/23/2013,
  DD/MM/YYYY    - 23/12/2013,
  Do MMMM, YYYY - 21st August, 2013 
  dddd Do, MMMM – Thursday, 3rd March
  Do MMMM, YYYY – 9th April, 2015
  hh:mm a     -   04:30 pm              
******************************************************************/
function getDateTimeFormat(date, format) {      
    var dateFormat 		= "";
    try {
        if(moment(date).isValid()){  
           	dateFormat 	= moment(date).format(format);
        }
        printMessage("&&&&&&&&& dateFormat is:" + dateFormat);
    } catch(error){
       	printMessage("moment date format error :" + error);
    }
    return dateFormat;
}

//Formats given datetime in given format
function getDateTimeInFormat(dateTime, fmt){
    var strDate1 	= dateTime.getDate();
    strDate1 		= (strDate1 < 10) ? ("0" + strDate1) : strDate1;
    var strMonth1 	= dateTime.getMonth() + 1;
    strMonth1 		= (strMonth1 < 10) ? ("0" + strMonth1) : strMonth1;
    var strYear1 	= dateTime.getFullYear();
    var startDate 	= strYear1+"-"+strMonth1+"-"+strDate1;    
    var H 			= dateTime.getHours();
    var HH 			= (H <= 9)? '0' + H : H;    
    var m 			= dateTime.getMinutes();
    var mm 			= (m <= 9)? '0' + m : m;        
    var s 			= dateTime.getSeconds();
    var ss 			= (s <= 9)? '0' + s : s;      
    var startTime 	= HH+":"+mm+":"+ss;
        
    switch (fmt) {
      	case "yyyy-MM-dd":
        	reqFmt 		= strYear1+"-"+strMonth1+"-"+strDate1;
        	break;
      	case "dd-MM-yyyy":
        	reqFmt 		= strDate1+"-"+strMonth1+"-"+strYear1;
        	break;
      	case "HH:mm:ss":
        	reqFmt 		= HH+":"+mm+":"+ss;
        	break;
     	case "yyyy-MM-dd HH:mm:ss":
        	reqFmt 		= strYear1+"-"+strMonth1+"-"+strDate1+" "+HH+":"+mm+":"+ss;
        	break;
        case "dd-MM-yyyy HH:mm:ss":
        	reqFmt 		= strDate1+"-"+strMonth1+"-"+strYear1+" "+HH+":"+mm+":"+ss;
        	break;
    }
    return reqFmt;
}

/** Validate email address **/
function isValidEmail(emailId){
  	var isValidEmailFlag		= false;
  	if (!isEmpty(emailId)) {
      	var emailAddress		= emailId.trim();      
      	var emailRegExp			= /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //var emailRegExp			= /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
      isValidEmailFlag			= emailRegExp.test(emailAddress);
      	
        if (isValidEmailFlag === true){
          	var emailAddressSplits	= emailAddress.split("@");
          	if (emailAddress.length > 254) {
          		// Validation Custom Rule 1 => Max length not exceeds 254 characters
				isValidEmailFlag		= false;
        	} else if(emailAddressSplits[0].length > 64 || emailAddressSplits[1].length > 253) {
          		// Validation Rule 2 => Local Part Max length not exceeds 64 characters and Domain Part Max length not exceeds 253 characters
				isValidEmailFlag		= false;
        	}
        }
    }
  
  	return isValidEmailFlag;
}

/** Validating for Numeric and spaces*/
function isNumericSpaceRegValidation(inputPhoneNumber){
  
  var numaricSpaceRegExp			= /^(?=.*\d)[\d ]+$/;
  var isValidNumaricSpace			= numaricSpaceRegExp.test(inputPhoneNumber);
  
  return isValidNumaricSpace;
}

function generateUUID() {
    //// return uuid of form xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx
    var uuid = '';
    var i;
    for (i = 0; i < 32; i += 1) {
      switch (i) {
      case 8:
      case 20:
        uuid += '-';
        uuid += (Math.random() * 16 | 0).toString(16);
        break;
      case 12:
        uuid += '-';
        uuid += '4';
        break;
      case 16:
        uuid += '-';
        uuid += (Math.random() * 4 | 8).toString(16);
        break;
      default:
        uuid += (Math.random() * 16 | 0).toString(16);
      }
    }
    kony.print("printing uuid"+uuid);
    return uuid;
   
  }

function getDateTimeDiffInMins(startDate, endDate){
	var mins 		= 0;
	try {
		var ms 		= moment(startDate,"DD/MM/YYYY HH:mm:ss").diff(moment(endDate,"DD/MM/YYYY HH:mm:ss"));
		var d 		= moment.duration(ms);
		var s 		= Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss")+"";
		
		var tempArr = s.split(":");
		mins 		= tempArr[1];
	} catch(error) {
		printMessage("Error while getDateTimeDiffInHours :"+JSON.stringify(error));
	}
	return mins;
}

function getSupportViewDateTimeDiffInMins(startDate, endDate){
	var mins 		= 0;
    var minutes 	= 0;
	try {
      
       //YYYY-MM-DD HH:mm:ss:SSSS
		var ms 		= moment(startDate,"YYYY-MM-DD HH:mm:ss:SSSS").diff(moment(endDate,"YYYY-MM-DD HH:mm:ss:SSSS"));
		var d 		= moment.duration(ms);
		var s 		= Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss")+"";
      
      
     	var seconds = (ms / 1000).toFixed(0);
        minutes 	= Math.floor(seconds / 60);
      
       printMessage("getSupportViewDateTimeDiffInMins::: milli seconds value "+ms);
       printMessage("getSupportViewDateTimeDiffInMins::: minutes value "+minutes);
		
       printMessage("getSupportViewDateTimeDiffInMins::: s value "+JSON.stringify(s));
		var tempArr = s.split(":");
       printMessage("getSupportViewDateTimeDiffInMins::: tempArr value "+JSON.stringify(tempArr));
		mins 		= tempArr[1];
	} catch(error) {
		printMessage("Error while getDateTimeDiffInHours :"+JSON.stringify(error));
	}
	return minutes;
}

function isResultNotNull(resultTable){
	if (resultTable !== null && resultTable !== undefined && resultTable !== ""){
		return true;
	}else{
		return false;
	}
}

/**
* Adding focus to TextBox for Leads Page validation
*/
function setTextBoxFocus(textBox){
  
  try {
    if(textBox !== null){
      textBox.setFocus(true);
    }	
  } catch(e) {
    printMessage("Error while setTextBoxFocus: " + e);
  }	
}


function uniqueCriteriaVA(arrayData) {
	  return arrayData.filter(function(elem, pos) {      
      return arrayData.indexOf(elem) == pos;
      });
}
/**
 * this is to get emailSegData which need to show popup
 */
function getEmailSegData(emailList) {
    var emailSegData = []; 
  var emailRowData = null;
  	printMessage("getEmailSegData:: ");
  //var segmentRowData ={};
    if (emailList !== null && emailList.length > 0) {
      
        var length = emailList.length;
        for (var i = 0; i < length; i++) {
          emailRowData = {};
          printMessage("getEmailSegData::i value "+emailList[i]);
          emailRowData.lblTitle 	= emailList[i];
          /* segmentRowData= {

             lblItem: {
               "text": emailList[i]
             },
             imgRadioBtnIcon: {
               "isVisible":"false"
             }
           };*/
            emailSegData.push(emailRowData);
        }
    }
    return emailSegData;
}

/** Convert input data format yyyy-mm-dd to dd/mm/yy
	Ex: Input Date : 2016-10-30 Ouput Date : 30/10/2016
**/
 
  function getFormartedDate(inputDate){
     var dateValue = inputDate.split("-");
     var yearStr = dateValue[0];
     dateValue[0] = yearStr.substring(2);
     var formatedData = dateValue.reverse().join("/");
     return formatedData;
}


/**
*Clearing All Store data and Globals Data
**/
function clearStoreData(){
  
  kony.store.setItem(LAConstants.IS_EMP_LOGGED_IN, false);
  var emptyEmpObj						= {};
  var empProfileStr						= JSON.stringify(emptyEmpObj);
  kony.store.setItem(LAConstants.EMP_PROFILE, empProfileStr);
  
  resetOnChangeSourceOrEmail();
}


function updateLastSyncDate(){

  //Updating last sync date time
  var lastUploadSyncDateTime 					= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
  printMessage("Inside updateLastSyncDate lastUploadSyncDateTime"+lastUploadSyncDateTime);	
  kony.store.setItem(LAConstants.LAST_UPLOAD_SYNC_DATE_TIME, lastUploadSyncDateTime);	
}

function setHumburgerDeviceDatetime()
{
  var strFormat = getDeviceDateFormat();
  //var strFormat					= kony.store.getItem(LAConstants.LAST_UPLOAD_SYNC_DATE_TIME);
  printMessage("Inside setHumburgerDeviceDatetime strFormat"+strFormat);

  var frmObj								= kony.application.getCurrentForm();
  frmObj.flxBurgerMenu.lblLastTransferred.text			= getI18nString("common.label.lastDateTransferred")+"  "+strFormat;
  //frmObj.lblAppVersion.text					= getI18nString("common.label.version") +" " + LAConstants.APP_VERSION;
  frmObj.flxBurgerMenu.lblAppVersion.text					= getI18nString("common.label.version") +" " + appConfig.appVersion;
}

function getDeviceDateFormat() {
  var strDateinDeviceFormat = "";
  try {
    var lastSyncDateTime				= kony.store.getItem(LAConstants.LAST_UPLOAD_SYNC_DATE_TIME);
    printMessage("lastSyncDateTime:: "+lastSyncDateTime);
      if (!isEmpty(lastSyncDateTime)){
    var objDeviceDateFormat 			= new DeviceDateTimeFormat.getDeviceDateTimeFormat();
    printMessage("getDeviceDateFormat objDeviceDateFormat:: "+objDeviceDateFormat);
    strDateinDeviceFormat 				= objDeviceDateFormat.getDateinDeviceDateTimeFormat(lastSyncDateTime,"  "); //FFI call - To get device date format of device
 	printMessage("getDeviceDateFormat strDateinDeviceFormat:: "+strDateinDeviceFormat);
   }else{
      lastSyncDateTime ="";
    }
  } catch (error) {
    alert(" Error while execute getDeviceDatFormat FFI :::" + error);
  }
  return strDateinDeviceFormat;
}

function setMasterBurgerMenuVisibity(){
  
  var frmObj						= kony.application.getCurrentForm();
  if(kony.application.getCurrentForm().id === LAConstants.FORM_SUPPORTVIEW){
		printMessage("If condition:: ");
    frmObj.flxBurgerMenu.flxBtmContainer.setVisibility(false);
    frmObj.flxBurgerMenu.flxMasterSupportView.setVisibility(true);
    frmObj.flxFormHeader.imgStartSupportTime.setVisibility(false);
  }else{
    printMessage("else condition:: ");
    frmObj.flxBurgerMenu.flxMasterSupportView.setVisibility(false);
    frmObj.flxBurgerMenu.flxBtmContainer.setVisibility(true);
    frmObj.flxFormHeader.imgStartSupportTime.setVisibility(false);
  }

  setHumburgerDeviceDatetime();
}



function isFutureDate(pastDate){
	var isFuture = false;
	try{
		var date = getDateTimeFormat(new Date(pastDate),"YYYY-MM-DD")
		//moment(pastDate+"");
		var now = getDateTimeFormat(new Date(),"YYYY-MM-DD")
		kony.print("now :"+now);
		if (now < date) {
		   // date is past
		    isFuture = false;
		}else if(now > date){
		   // date is future
		    isFuture = true;
		}
	}catch(error){
		kony.print("Error while getting future date function");
	}
	return isFuture;
}

function setInternationalizationCode(){
  
  gblBasicInfo.languages			= [];
  
  gblBasicInfo.languages.push({"code":"en_GB", "name":"English"});
  gblBasicInfo.languages.push({"code":"in_ID", "name":"Indonesian"});
  gblBasicInfo.languages.push({"code":"zh_CN", "name":"Simplified Chinese"});
  gblBasicInfo.languages.push({"code":"zh_TW", "name":"Traditional Chinese"});
  gblBasicInfo.languages.push({"code":"th_TH", "name":"Thai"});
  
  gblBasicInfo.languages.push({"code":"es_ES", "name":"Spanish"});
  gblBasicInfo.languages.push({"code":"ko_KR", "name":"Korean"});
  gblBasicInfo.languages.push({"code":"it_IT", "name":"Italian"});
  gblBasicInfo.languages.push({"code":"pt_BR", "name":"Portuguese"});
  gblBasicInfo.languages.push({"code":"pl_PL", "name":"Polish"});
  
  gblBasicInfo.languages.push({"code":"fi_FI", "name":"Finnish"});
  gblBasicInfo.languages.push({"code":"nb_NO", "name":"Norwegian"});
  gblBasicInfo.languages.push({"code":"fr_FR", "name":"French"});
}



function setLanguagesCode(){
  printMessage("setLanguagesCode");
  
  gblLangCode.lang			= [];
  
  gblLangCode.lang.push({"code":"en_GB", "name":"ENG"});
  gblLangCode.lang.push({"code":"in_ID", "name":"IND"});
  gblLangCode.lang.push({"code":"zh_CN", "name":"SCN"});
  gblLangCode.lang.push({"code":"zh_TW", "name":"TCN"});
  gblLangCode.lang.push({"code":"th_TH", "name":"THA"});
  
  gblLangCode.lang.push({"code":"es_ES", "name":"ES"});
  gblLangCode.lang.push({"code":"ko_KR", "name":"KOR"});
  gblLangCode.lang.push({"code":"it_IT", "name":"ITA"});
  gblLangCode.lang.push({"code":"pt_BR", "name":"POR"});
  gblLangCode.lang.push({"code":"pl_PL", "name":"POL"});
  
  gblLangCode.lang.push({"code":"fi_FI", "name":"FIN"});
  gblLangCode.lang.push({"code":"nb_NO", "name":"NOR"});
  gblLangCode.lang.push({"code":"fr_FR", "name":"FRA"});
}



function startPurgeDBScheduler(){
	printMessage("Inside startPurgeDBScheduler ");
	var timeInSecs 							= LAConstants.PURGE_DB_DATE_SCHEDULE_TIME; 
	try {		
		kony.timer.schedule("PurgeDBScheduler", checkPurgeDBTime, timeInSecs, true);
	} catch(error) {
		printMessage("startPurgeDBScheduler error: " + JSON.stringify(error));
	}
}

/** The application should purge stale data on the client database which is older than 7 days. This activity requires an independent is trigger run once a day. Irrespective of whether the device is online or offline the stale data is purged from the client database. 
Before purging, there should be a check to ensure that there is no Lead or Alert error on the support view associated to the Lead or alert before deletion. This should ensure that the technician is able to indicate when he is happy for those local records are no longer required and can therefore be purged.*/ 

function checkPurgeDBTime(){
  
  var dateDiffInMins 				= 0;
  var lastDeletedPurgeDBTimeValue 	= kony.store.getItem(LAConstants.PURGE_DB_DATE_TIME);

  if (!isEmpty(lastDeletedPurgeDBTimeValue)) {
    printMessage("Inside - checkPurgeDBTime saved lastDeletedPurgeDBTime:: "+lastDeletedPurgeDBTimeValue);
   // var currentDateTime 				= getDateTimeFormat(new Date(), "DD/MM/YYYY HH:mm:ss");
    var currentDateTime 				= getDateTimeFormat(new Date(), gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT); //YYYY-MM-DD HH:mm:ss:SSSS
    printMessage("Inside - checkPurgeDBTime lastDeletedPurgeDBTime: " + lastDeletedPurgeDBTimeValue + " current DateTime: " + currentDateTime);

    //dateDiffInMins 					= getDateTimeDiffInMins(currentDateTime, lastDeletedPurgeDBTimeValue);
    dateDiffInMins 						= getSupportViewDateTimeDiffInMins(currentDateTime, lastDeletedPurgeDBTimeValue);
    dateDiffInMins 						= parseInt(dateDiffInMins);

  }else{

    updatePurgeTime();
  }

  printMessage("Inside - checkPurgeDBTime lastDeletedPurgeDBTime: dateDiffInMins " + dateDiffInMins);

  //1 day
  if (dateDiffInMins >= LAConstants.PURGE_DB_TIMER){ 
    printMessage("Inside - Purge different time is refreshed " + dateDiffInMins);
    updatePurgeTime();
    //Delete leads/alerts from lead/alerts tabel, if lead/alerts are existed in SupportView( or in LAError tabel)
    getLeadsErrorFromLocalDB();
  }
}

function updatePurgeTime(){
    //var lastDeletedPurgeDBTime 		= getDateTimeFormat(new Date(),"YYYY-MM-DDTHH:mm:ss.SSS");
    var lastDeletedPurgeDBTime 		= getDateTimeFormat(new Date(),gblSupportView.SUPPORTVIEW_DATE_TIME_FORMAT); //YYYY-MM-DD HH:mm:ss:SSSS
    printMessage("Inside - checkPurgeDBTime first time lastDeletedPurgeDBTime:: "+lastDeletedPurgeDBTime);
    kony.store.setItem(LAConstants.PURGE_DB_DATE_TIME, lastDeletedPurgeDBTime);
}


function getUploadSupportErrorCode(jsonString) {
  var errorCode = "";
  try {
    printMessage("getUploadSupportErrorCode jsonString::: "+jsonString);
    var jsonArr = jsonString.split("errorNumber");
    //var jsonArr = jsonString.split("errorCode");
    printMessage("getUploadSupportErrorCode jsonArr::: "+JSON.stringify(jsonArr));
    if (jsonArr !== null && jsonArr.length > 0) {
      var str = jsonArr[1];
      kony.print("getUploadSupportErrorCode str :" + str);
      if (str !== null) {
        var strArr = str.split(",");
        kony.print("getUploadSupportErrorCode strArr :" + JSON.stringify(strArr));
        if (strArr !== null && strArr.length > 0) {
          errorCode = strArr[0].replace(/[^\w\s]/gi, '');
        }
      }
      if(isEmpty(errorCode) === false){

        return errorCode.trim();
      }
    }
  } catch (error) {
    kony.print("Error while retrieve errorCode :" + JSON.stringify(error));
  }
  return errorCode;
}




function saveLocaleUpdatedLangCode(){
  printMessage("saveLocaleUpdatedLangCode");
  var getLangKey = kony.store.getItem(LAConstants.SEL_LAN_CODE);
  printMessage("saveLocaleUpdatedLangCode getLangKey:: "+getLangKey);
  for (var m = 0; m < gblLangCode.lang.length; m++) {
 	printMessage("saveLocaleUpdatedLangCode gblLangCode.lang[m].name:: "+"m value:: "+m+" ::: "+gblLangCode.lang[m].name);
    if(getLangKey == gblLangCode.lang[m].code){
      
      kony.store.setItem(LAConstants.SAVED_LANG_CODE,gblLangCode.lang[m].name);
    }
  }
}
